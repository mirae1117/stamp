<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>그루터기 오는길</title>
<link rel="stylesheet" type="text/css" href="/resources/include/css/company/companyMap.css" />
</head>
<body>
	<jsp:include page="../mainpage/header.jsp" />
	<div id="nav">
		<jsp:include page="introAside.jsp" />
	</div>
	<div id="content">
		<table style="width: 536px">
			<tr>
				<td style="border: 1px solid #cecece;"><a
					href="http://map.naver.com/?searchCoord=83dd1b6859b459f0a924f9a5bda9cdf91516b3585102ae1ed151e68e576d390f&query=66%2B4656Y64ql66Cl6rCc67Cc6rWQ7Jyh7JuQ&menu=location&tab=1&lng=604bcb7f6ead89bc62178e0bce1e5884&__fromRestorer=true&mapMode=0&mpx=37.5621545%2C127.0350794%3AZ13%3A0.0049876%2C0.0036263&lat=2b7af5da2e8e77311dc31d768569d1fb&dlevel=12&enc=b64"
					target="_blank"><img
						src="http://prt.map.naver.com/mashupmap/print?key=p1476346417003_-2100717857"
						width="534" height="418" alt="지도 크게 보기" title="지도 크게 보기"
						border="0" style="vertical-align: top;" /></a></td>
			</tr>
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td height="30" bgcolor="#f9f9f9" align="left"
								style="padding-left: 9px; border-left: 1px solid #cecece; border-bottom: 1px solid #cecece;">
								<span style="font-family: tahoma; font-size: 11px; color: #666;">2016.10.13</span>&nbsp;<span
								style="font-size: 11px; color: #e5e5e5;">|</span>&nbsp;<a
								style="font-family: dotum, sans-serif; font-size: 11px; color: #666; text-decoration: none; letter-spacing: -1px;"
								href="http://map.naver.com/?searchCoord=83dd1b6859b459f0a924f9a5bda9cdf91516b3585102ae1ed151e68e576d390f&query=66%2B4656Y64ql66Cl6rCc67Cc6rWQ7Jyh7JuQ&menu=location&tab=1&lng=604bcb7f6ead89bc62178e0bce1e5884&__fromRestorer=true&mapMode=0&mpx=37.5621545%2C127.0350794%3AZ13%3A0.0049876%2C0.0036263&lat=2b7af5da2e8e77311dc31d768569d1fb&dlevel=12&enc=b64"
								target="_blank">지도 크게 보기</a>
							</td>
							<td width="98" bgcolor="#f9f9f9" align="right"
								style="text-align: right; padding-right: 9px; border-right: 1px solid #cecece; border-bottom: 1px solid #cecece;">
								<span style="float: right;"><span
									style="font-size: 9px; font-family: Verdana, sans-serif; color: #444;">&copy;&nbsp;</span>&nbsp;<a
									style="font-family: tahoma; font-size: 9px; font-weight: bold; color: #2db400; text-decoration: none;"
									href="http://www.nhncorp.com" target="_blank">NAVER Corp.</a></span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<jsp:include page="../mainpage/footer.jsp" />
</body>
</html>

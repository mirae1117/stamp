<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<link rel="stylesheet" type="text/css" href="/resources/include/css/company/introAside.css" />
<aside class="mypageAside">
	<h3 class="mypage_title">회사소개</h3>
	<ul class="mypage_lnb">
		<li>
			<ul>
				<li><a href="/main/companyIntro.do">그루터기 소개</a></li>
				<li><a href="/main/companyMap.do">그루터기 오는길</a></li>
			</ul>
		</li>

	</ul>
</aside>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>그루터기 소개</title>

<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<link rel="stylesheet" type="text/css" href="/resources/include/css/company/companyIntro.css" />
</head>
<body>
	<div id="warp">
		<jsp:include page="../mainpage/header.jsp" />
		<jsp:include page="introAside.jsp" />

		<section id="intro" class="myPageContent">
			<div id="content">
				<h1>그루터기는....</h1>
				<p>
					미취학아동부터 일반인까지 이용할 수 있는 문제를 풀어주는 사이트입니다.<br> 자신이 풀기 어려운 문제들을
					사진을 찍어서 올리거나 글을 써서 올리면<br> 다른회원들이 올린 문제를 보고 풀고 문제에 대해서 설명을 해주는<br>
					사이트이면서 모든 사람들한테 무료로 문제풀이를 해주는 사이트입니다.
				</p>
			</div>
		</section>
		<jsp:include page="../mainpage/footer.jsp" />
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style type="text/css">
.form-group{
}
.form-control{
}
.form-group .form-control{
width: auto;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		 $(window).resize(function() {
			 if (window.innerWidth >= 992) {
				$("#log_id").attr("size", "10");
				$("#log_pw").attr("size", "10");
			}else {
				$("#log_id").attr("size", "50");
				$("#log_pw").attr("size", "50");
			}
		});
		
		/*로그인 버튼 클릭시 처리 이벤트*/
		$("#loginbtn").click(function() {
			 
				$("#loginForm").attr({
					"method" : "POST",
					"action" : "/login/loginPro.do"
				});
				$("#loginForm").submit();
		}); /*회원가입 버튼 클릭시 처리 이벤트*/
		$("#joinbtn").click(function() {
			location.href = "/join/joinAge.do";
		}); /*아이디/비밀번호 찾기 버튼 클릭시 처리 이벤트*/
		$("#findIdPasswordBtn").click(function() {
			window.open("/join/findIdPassword.do", "", "width=600,height=400");
		});
	});
</script>
	<form id="loginForm" name="loginForm" class="navbar-form navbar-right">
		<div class="form-group">
        	<input id="log_id" name="log_id" type="text" class="form-control" size="10" placeholder="ID"/>
        </div>
		<div class="form-group">
        	<input id="log_pw" name="log_pw" type="password"  class="form-control" size="10" placeholder="비밀번호" />
        </div>
        <button type="button" id="loginbtn" name="loginbtn" class="btn btn-success">로그인</button>
        <button type="button" id="joinbtn" name="joinbtn" class="btn btn-info">회원가입</button>
        <button type="button" id="findIdPasswordBtn" name="findIdPasswordBtn" class="btn btn-danger">Find ID/PW</button>
	</form>

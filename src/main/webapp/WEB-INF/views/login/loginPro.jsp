<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style type="text/css">
.colorPaleVioletRed {
	color: PaleVioletRed;
}

label {
	margin-right: 20px;
}
.colorRed{
color: red;
}
</style>
<script type="text/javascript">
	//로그아웃 버튼 클릭시
	$(document).ready(function() {
		$("#logoutbtn").click(function() {
			location.href = "/login/logout.do";
		});
		$("#myPage").click(function() {
			location.href = "/mypage/info.do";
		});
		$("#adminPage").click(function() {
			location.href = "/admin/adminPage.do";
		});
	});
</script>
	<input type="hidden" id="log_id" name="log_id" value="${log_id}" />
	<form id="loginForm" name="loginForm" class="navbar-form navbar-right">
		<div class="form-group">
        	<label class="colorPaleVioletRed">${log_id}님 / ${memberPoints}P</label>
        </div>
		<div class="form-group">
        	<c:if test="${message_count != '0'}">
	        	<a href="/message/messageBox/getBox.do">
	        		<img alt="미확인 메세지" src="/resources/include/img/messageIcon.png" width="20px"> <font class="colorRed">${message_count}개</font>
				</a>
			</c:if> 
		</div>
        <input type="button" id="logoutbtn" name="logoutbtn" class="btn btn-danger" value="로그아웃" />
		<input type="button" id="myPage" name="myPage" class="btn btn-info" value="마이페이지" />
		<c:if test="${log_state == '관리자'}">
			<input type="button" class="btn btn-warning" id="adminPage" name="adminPage" value="관리자" />
		</c:if>
	</form>








<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String log_id = "";
	try {
		log_id = (String) session.getAttribute("log_id");
		if (log_id == null || log_id.equals("")) {
%>
<jsp:include page="/login/loginForm.do"></jsp:include>
<%
	} else {
%>
<jsp:include page="/login/loginPro.do"></jsp:include>
<%
	}
	} catch (Exception e) {
		e.printStackTrace();
	}
%>	

<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script>
		var result = '${msg}';
		if (result != null && result != '') {
			alert(result);
		}
	</script>
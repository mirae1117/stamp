
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>footer</title>
<style type="text/css">
footer {
	background-color: #222222;
	color: #D5D5D5;
	padding-bottom: 0px;
}
</style>
</head>
<body>
	<footer>
		<div class="footer1">
			<h1>
				(주)그루터기 &nbsp;&nbsp; 대표자 : 스탬프 &nbsp;&nbsp; 주소 : 서울시 성동구 왕십리 신방빌딩
				&nbsp;&nbsp; 사업자 등록번호 : 123-45-67890<br /> 광고문의 : <a
					href="mailto:stamp@naver.com?subject=Feedback"> stamp@naver.com</a>
				&nbsp;&nbsp; 대표전화 : 02-123-4567 &nbsp;&nbsp; FAX : 02-123-5678
			</h1>
		</div>
		<div class="footer2">
			<h2 class="footer2h2">
				그루터기<br> 개인정보취급방침 | 회사소개 | 찾아오시는길
			</h2>
		</div>
	</footer>
</body>
</html>

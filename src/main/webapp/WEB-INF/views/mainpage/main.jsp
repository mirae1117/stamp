<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>그루터기</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>

<script type="text/javascript" src="/resources/include/js/common.js"></script>
<link rel="stylesheet"
	href="/resources/include/css/ideal-image-slider.css">
<link rel="stylesheet"
	href="/resources/include/css/themes/default/default.css">
<style>
#mainPage {
	padding-top: 50px;
}

.slider1 {
	margin-left: 0px;
	margin-right: 30px;
	margin-bottom: 30px;
}

.slider {
	padding: 0;
	margin: 0;
	width: 100%;
}

.ba {
	margin: auto;
}
</style>
</head>

<body>
	<jsp:include page="header.jsp" />
	<section id="mainPage">
		<div class="slider1">
			<div id="slider">

				<img src="/resources/include/img/1.jpg" height="100px"> <img
					data-src="/resources/include/img/2.jpg" height="100px"> <img
					data-src="/resources/include/img/3.jpg" height="100px"> <img
					data-src="/resources/include/img/4.jpg" height="100px">
			</div>
		</div>
		<!-- Include slider JS file -->
		<script src="/resources/include/js/ideal-image-slider.js"></script>
		<!-- Create your slider -->
		<script>
			new IdealImageSlider.Slider('#slider');
		</script>
		<div class="table-responsive">
			<table class="ba">
				<tr>
					<td><a href="#"><img src="/resources/include/img/01.jpg"
							width="250" height="250" /></a></td>
					<td><a href="#"><img src="/resources/include/img/01.jpg"
							width="250" height="250" /></a></td>
					<td><a href="#"><img src="/resources/include/img/01.jpg"
							width="250" height="250" /></a></td>
					<td><a href="#"><img src="/resources/include/img/01.jpg"
							width="250" height="250" /></a></td>
				</tr>
			</table>
		</div>
	</section>

	<jsp:include page="footer.jsp" />
</body>
</html>
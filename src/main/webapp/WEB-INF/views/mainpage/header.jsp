<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="/resources/include/js/bootstrap.min.js"></script>
	<link href="/resources/include/css/bootstrap.css" rel="stylesheet">
<style type="text/css">
#logo {
	padding : 0;
	margin : 0;
	width: 60px;
	height: 30px;
	cursor: pointer;
}

.loginForm li{
display: inline;
}
.loginForm ul{
display: inline;
}
footer {
	float: left;
	width: 100%;
	height: 180px;
	text-align: center;
	background-color: white;
	width: 100%;
}

footer .footer1 h1 {
	font-size: 20px;
}

footer .footer1 h1 a {
	text-decoration: none;
	color: black;
}

footer .footer1 h1 a:hover {
	color: gray;
}

footer .footer1 h2 {
	font-size: 15px;
}

#mainPage {
	clear: left;
}

.ba ul li {
	display: inline;
}

.ba img {
	margin-left: 15;
	margin-right: 30;
}

.footer1, .footer1 h1 {
	margin-bottom: 0px;
	padding-bottom: 0px;
}

.footer2, .footer2h2 {
	margin: 0 auto;
	padding-top: 5;
}
</style>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#loginbtn").click(function() {
			location.href = "/login/login.do";
		});
		$("#joinbtn").click(function() {
			location.href = "/join/joinAge.do";
		});
		$("#logo").click(function() {
			location.href = "/main/main.do";
		})
	});
</script>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="/main/main.do"><img src="/resources/include/img/logo.png" id="logo"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
			<li><a class="active" href="/main/companyIntro.do">회사소개</a></li>
			<li><a href="/qboard/boardList.do">문제풀이</a></li>
			<li><a href="/notice/noticeList.do">공지사항</a></li>
          </ul>
          <jsp:include page="../login/login.jsp"></jsp:include>
        </div>
      </div>
    </nav>

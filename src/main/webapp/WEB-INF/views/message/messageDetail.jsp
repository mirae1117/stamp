<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>글상세 보기</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<link rel="stylesheet" type="text/css"
	href="/resources/include/css/message.css" />
<script type="text/javascript">
	$(document).ready(function() {
	});
	function messageDelete(boxType) {
		if (confirm("쪽지를 삭제하시겠습니까?") == true) {
			$("#f_data").attr({
				"method" : "POST",
				"action" : "/message/messageDelete.do?boxType=" + boxType
			});
			$("#f_data").submit();
		} else {
			alert("취소하셨습니다.");
		}
	}
	function goList(boxType) {
		location.href = "/message/messageBox/" + boxType + ".do";
	}
</script>
<style type="text/css">
.info_title{
height:32px;
margin:50px 0px 20px 55px;
border-bottom: 1px solid #ededee;
font-size: 25px;
line-height: 30px;
line-height: 1.2em;
font-weight: 600;
padding: 0 0 40px;
}
.info_title2{
margin: 10px 10px 10px 60px;
padding:0px 20px 0 10px;
  background: url(/resources/include/img/bl_h3.gif) no-repeat left;
  font-size: 15px;
  font-weight: bold;
}
</style>
</head>
<body>
	<jsp:include page="../mainpage/header.jsp" />
	<div id="container">
	<jsp:include page="../mypage/myAside.jsp" />
	<section class="myPageContent">
	<h2 class="info_title">쪽지함</h2>
		<h4 class="info_title2">쪽지보기</h4>
		<div id="boardTit">
		</div>
		<input type="hidden" id = "boxType"  name = "boxType" value ="${param.boxType}"/>
		<form name="f_data" id="f_data" method="POST">
			<input type="hidden" name="msg_no" id="msg_no" value="${detail.msg_no}" />
			<input type="hidden" name="m_sender" id="m_sender" value="${detail.m_sender}" />
			<input type="hidden" name="m_receiver" id="m_receiver" value="${detail.m_receiver}" />
		</form>
		<%-- ===============  상세 정보 보여주기 시작 ============ --%>
		<c:if test="${detail == null}">
	다른사람의 쪽지는 볼 수 없습니다.
	</c:if>
		<c:if test="${detail != null}">
			<div></div>
			<div id="boardDetail">
				<table class="table table-bordered">
					<tbody>
						<tr>
							<td class="ac">발신인</td>
							<td>${detail.m_senderName}(<a href="javascript:otherUserInfo('${detail.m_sender}')">${detail.m_sender}</a>)</td>
							<td class="ac">작성일</td>
							<td>${detail.msg_sendtime}</td>
						</tr>
						<tr>
							<td class="ac">수신인</td>
							<td>${detail.m_receiverName}(<a href="javascript:otherUserInfo('${detail.m_receiver}')">${detail.m_receiver}</a>)</td>
							<td class="ac">확인일</td>
							<td>${detail.msg_receivetime}<c:if test="${detail.msg_receivetime == null}">
								확인 안함
							</c:if>
							</td>
						</tr>
						<tr>
							<td class="ac">제목</td>
							<td colspan="3">${detail.msg_title}</td>
						</tr>
						<tr class="ctr">
							<td class="ac">내용</td>
							<td colspan="3">${detail.msg_content}</td>
						</tr>
					</tbody>
				</table>
				<button onclick="messageDelete('${param.boxType}')">삭제하기</button>
				<button onclick="goList('${param.boxType}')">목록으로</button>
			</div>
		</c:if>
	</section>
	</div>
	<%-- =============== 상세 정보 보여주기 종료 ============ --%>
</body>
</html>
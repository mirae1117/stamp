<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>
<c:if test="${boxType == 'sendBox'}">보낸 편지함</c:if>
<c:if test="${boxType == 'getBox'}">받은 편지함</c:if>
</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$(document).on("click", "#receiverId", function() {
			var receiver = $("#m_receiver").val();
			window.open("/mypage/otherInfo/" + receiver + ".do", "", "width=750,height=600");
		});	
		$( window ).resize(function() {
			$(".myPageContent").css("width",($(window).width()-$(".mypageAside").width()-50));
			});
		$(".myPageContent").css("width",($(window).width()-$(".mypageAside").width()-50));
	});
	/* 상세 페이지 이동 함수 */
	function goDetail(msg_no,boxtype) {
		location.href = "/message/messageDetail.do?msg_no=" + msg_no + "&boxType=" + boxtype;
	}
	function msgWriter(receiver,boxtype) {
		location.href = "/message/messageWrite/" + receiver + ".do?boxType=" + boxtype;
	}
</script>
<style type="text/css">
.info_title{
height:32px;
margin:50px 0px 20px 55px;
border-bottom: 1px solid #ededee;
font-size: 25px;
line-height: 30px;
line-height: 1.2em;
font-weight: 600;
padding: 0 0 40px;
}
.info_title2{
margin: 10px 10px 10px 60px;
padding:0px 20px 0 10px;
  background: url(/resources/include/img/bl_h3.gif) no-repeat left;
  font-size: 15px;
  font-weight: bold;
}
#messageTable{
margin-left: 50px;
margin-right: 50px;
}
</style>
</head>
<body>
	<jsp:include page="../mainpage/header.jsp" />
	<div id="container">
	<jsp:include page="../mypage/myAside.jsp" />
	<section class = "myPageContent">
	<h2 class="info_title">쪽지함</h2>
	<c:if test="${boxType == 'sendBox'}"><h4 class="info_title2">수신</h4></c:if>
	<c:if test="${boxType == 'getBox'}"><h4 class="info_title2">발신</h4></c:if>
		
		<div id="messageTable" class="table-responsive">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>쪽지 제목</th>
				<th>발송일</th>
				<%-- 받은편지함일 경우 발신자 수신편지함일경우 수신자 --%>
				<th>
					<c:if test="${boxType == 'sendBox'}">수신자</c:if>
					<c:if test="${boxType == 'getBox'}">발신자</c:if></th>
				<%-- 수신일이 null일 경우 보지않음 출력 --%>
				<th>수신일</th>
			</tr>
		</thead>
		<tbody>
			<!-- 데이터 출력 -->
			<c:choose>
				<c:when test="${not empty messageList}">
					<c:forEach var="message" items="${messageList}" varStatus="status">
						<tr align="center">
							<td align="left"><a
								href="javascript:goDetail(${message.msg_no},'${boxType}')">${message.msg_title}</a></td>
							<td>${message.msg_sendtime}</td>
							<td>
								<c:if test="${boxType == 'sendBox'}">${message.m_receiverName}(<a href="javascript:otherUserInfo('${message.m_receiver}')">${message.m_receiver}</a>)
								<input type="button" value="쪽지보내기" onclick="msgWriter('${message.m_receiver}','${boxType}')" /></c:if>
								<c:if test="${boxType == 'getBox'}">${message.m_senderName}(<a href="javascript:otherUserInfo('${message.m_sender}')">${message.m_sender}</a>)
								<input type="button" value="쪽지보내기" onclick="msgWriter('${message.m_sender}','${boxType}')" />
								</c:if>
							</td>
							<td>${message.msg_receivetime}
							<c:if test="${message.msg_receivetime == null}">
								확인 안함
							</c:if>
							</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr>
						<td colspan="5" align="center">등록된 쪽지가 존재하지 않습니다.</td>
					</tr>
				</c:otherwise>
			</c:choose>
		</tbody>
	</table>
	</div>
	</section>
	</div>
	<%-- =================== 리스트 종료 ================== --%>
	
	<jsp:include page="../mainpage/footer.jsp" />
</body>
</html>
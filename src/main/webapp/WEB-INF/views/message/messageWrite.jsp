<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>글쓰기 화면</title>
<link rel="stylesheet" type="text/css"
	href="/resources/include/css/message.css" />
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/* 저장 버튼 클릭 시 처리 이벤트*/
		$("#msgInsert").click(function() {
			//입력값 체크
			if (!chkSubmit($('#msg_title'), "제목을"))
				return;
			else if (!chkSubmit($('#msg_title'), "작성할 내용을"))
				return;
			else {
				$("#msg_writeForm").attr("method", "POST");
				$("#msg_writeForm").attr("action", "/message/messageInsert.do");
				$("#msg_writeForm").submit();
			}
		});
		/* 닫기 버튼 클릭 시 처리 이벤트 */
		$("#close").click(function() {
			close();
		});
	});
</script>
<style type="text/css">
#container{
position:relative;
margin:0;
padding-left : 20px;
}
</style>
</head>
<body>
<div id="container">
	<div id="boardTit">
		<h3>쪽지보내기</h3>
	</div>
	<form id="msg_writeForm" name="msg_writeForm">
		<table id="boardWrite" class="table table-bordered">
			<tr>
				<td>수신자</td>
				<td>${receiver_name}(<a href="javascript:otherUserInfo('${receiver}')">${receiver}</a>)<input type="hidden" name="m_receiver" id="m_receiver" value="${receiver}">
				<input type="hidden" name="m_sender" id="m_sender" value="${log_id}">
				</td>
			</tr>
			<tr>
				<td>글제목</td>
				<td><input type="text" class="form-control" name="msg_title" id="msg_title"></td>
			</tr>
			<tr>
				<td>내용</td>
				<td height="200"><textarea name="msg_content" class="form-control" id="msg_content"
						rows="10" cols="70"></textarea></td>
			</tr>
		</table>
	</form>
	<div id="boardBut">
		<button type="button" class="but" id="msgInsert">보내기</button>
		<button type="button" class="but" id="close">닫기</button>
	</div>
	</div>
</body>
</html>
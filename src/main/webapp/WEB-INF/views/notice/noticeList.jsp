<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="tag" uri="/WEB-INF/tld/noticecustom_tag.tld"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>공지사항 페이지</title>
<link rel="stylesheet" type="text/css"
	href="/resources/include/css/notice.css" />
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- 부가적인 테마 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

	});

	function goPage(page) {
		$("#page").val(page);
		$("#paging").attr("method", "GET");
		$("#paging").attr("action", "/notice/noticeList.do");
		$("#paging").submit();
	}

	function goDetail(nt_no, page, pageSize) {
		location.href = "/notice/noticeDetail.do?nt_no=" + nt_no + "&page="
				+ page + "&pageSize=" + pageSize;
	}
</script>
<style type="text/css">
#container {
	position: relative;
	margin: auto;
	width: 1024px;
	margin-top: 150px;
	padding-bottom: 200px;
}

.myPageContent {
	width: 960px;
}

.info_title {
	height: 32px;
	margin: 50px 0px 20px 55px;
	border-bottom: 1px solid #ededee;
	font-size: 25px;
	line-height: 30px;
	line-height: 1.2em;
	font-weight: 600;
	padding: 0 0 40px;
}

.info_title2 {
	margin: 10px 10px 10px 60px;
	padding: 0px 20px 0 10px;
	background: url(/resources/include/img/bl_h3.gif) no-repeat left;
	font-size: 15px;
	font-weight: bold;
}

#userNoticelist {
	width: 95%;
	padding-left: 50px
}

#userNoticePage {
	margin-top: 30px;
}
</style>
</head>
<body>
	<jsp:include page="../mainpage/header.jsp" />
	<div id="container">
		<%-- <jsp:include page="noticeAside.jsp" /> --%>

		<section class="myPageContent">
			<h2 class="info_title">공지사항</h2>
			<h4 class="info_title2">게시판</h4>
			<div id="userNoticelist">
				<form id="paging" name="paging">
					<input type="hidden" id="page" name="page" value="${data.page}" />
				</form>

				<table class="table table-bordered"
					class="notice table table-hover " cellspacing="0" cellpadding="0">
					<colgroup>
						<col width="10%" />
						<col width="40%" />
						<col width="20%" />
						<col width="20%" />
						<col width="10%" />
					</colgroup>
					<thead>
						<tr>
							<th></th>
							<th>제목</th>
							<th>작성자</th>
							<th>작성일</th>
							<th>조회</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${not empty noticeList }">
								<c:forEach var="notice" items="${noticeList}" varStatus="status">
									<tr align="center">
										<td>${count - (status.count-1)}</td>
										<td align="left"><a
											href="javascript:goDetail(${notice.nt_no},${data.page},${data.pageSize})">${notice.nt_title}</a>
										</td>
										<td>${notice.m_id }</td>
										<td>${notice.nt_time }</td>
										<td>${notice.nt_count }</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="5" align="center">등록된 게시물이 존재하지 않습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<div id="userNoticePage">
				<tag:paging page="${param.page}" total="${total}"
					list_size="${data.pageSize}" />
			</div>
		</section>
	</div>


	<jsp:include page="../mainpage/footer.jsp" />
</body>
</html>
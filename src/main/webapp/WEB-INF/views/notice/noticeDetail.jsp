<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css"
	href="/resources/include/css/notice.css" />
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- 부가적인 테마 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript">
	var goUrl = "";
	$(document).ready(function() {
		$("#updateForm").click(function() {
			goUrl = "/admin/notice/updateForm.do";
			$("#data").attr("method", "POST");
			$("#data").attr("action", goUrl);
			$("#data").submit();
		});

		$("#noticeDelete").click(function() {
			goUrl = "/admin/notice/noticeDelete.do";
			$("#data").attr("method", "POST");
			$("#data").attr("action", goUrl);
			$("#data").submit();
		});

		$("#noticeList").click(function() {
			location.href = "/notice/noticeList.do";
		});
	});
</script>
<style type="text/css">
#container {
	position: relative;
	margin: 0 auto;
	width: 1024px;
}

.myPageContent {
	margin-top: 100px;
	
	height: 800px;
	width: 80%;
}

.info_title {
	height: 32px;
	margin: 50px 0px 20px 55px;
	border-bottom: 1px solid #ededee;
	font-size: 25px;
	line-height: 30px;
	line-height: 1.2em;
	font-weight: 600;
	padding: 0 0 40px;
}

.info_title2 {
	margin: 10px 10px 10px 60px;
	padding: 0px 20px 0 10px;
	background: url(/resources/include/img/bl_h3.gif) no-repeat left;
	font-size: 15px;
	font-weight: bold;
}

#detail {
	width: 700px;
	margin: 0 auto;
}

.ctr {
	height: 300px;
}

#noticeBtn2 {
	padding-left: 350px;
}
</style>
</head>
<body>
	<jsp:include page="../mainpage/header.jsp" />
	<div id="container">
		<section class="myPageContent">
			<h2 class="info_title">공지사항</h2>
			<h4 class="info_title2">게시글</h4>
			<div class="content">
				<form name="data" id="data" method="POST">
					<input type="hidden" name="nt_no" id="nt_no"
						value="${detail.nt_no}" />
				</form>
				<div id="detail">
					<table class="table table-bordered" cellspacing="0" cellpadding="0">
						<colgroup>
							<col width="10%" />
							<col width="25%" />
							<col width="10%" />
							<col width="25%" />
						</colgroup>
						<tbody>
							<tr>
								<td class="ac">작성자</td>
								<td>${detail.m_id}</td>
								<td class="ac">작성일</td>
								<td>${detail.nt_time}</td>
							</tr>
							<tr>
								<td class="ac">제목</td>
								<td>${detail.nt_title}</td>
								<td class="ac">조회수</td>
								<td>${detail.nt_count}</td>
							</tr>
							<tr class="ctr">
								<td class="ac">내용</td>
								<td colspan="3">${detail.nt_content}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div id="noticeBtn2">
					<button class="btn btn-default" type="button" id="noticeList">목록으로</button>
				</div>

			</div>
		</section>

	</div>
	<jsp:include page="../mainpage/footer.jsp" />
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/userInfo.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript" src="/resources/include/js/friend.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(function() {
	if ($("#hiddenInfoPrivate").val() == 0) {
		$(".info_table").empty();
		$(".info_table2").empty();
		$(".info_title2").text("정보를 공개하지 않은 회원입니다.");
	}
	//쪽지 보내기 누를때
	$(document).on("click", "#msgWriterBtn", function() {
		var receiver = $("#hiddenInfoMid").val();
		var sender = $("#hiddenLog_id").val();
		msgWriter(receiver);
	});
	//친구신청 누를때
	$(document).on("click", "#callFriendBtn", function() {
		var receiver = $("#hiddenInfoMid").val();
		var sender = $("#hiddenLog_id").val();
		callFriend(receiver, sender);
		location.reload(true);
	});
	//친구신청 취소 누를때
	$(document).on("click", "#unCallFriendBtn", function() {
		var receiver = $("#hiddenInfoMid").val();
		var sender = $("#hiddenLog_id").val();
		unCallFriend(receiver, sender);
		location.reload(true);
	});
	//친구신청 거부 누를때
	$(document).on("click", "#refusalCallFriendBtn", function() {
		var sender = $("#hiddenInfoMid").val();
		var receiver = $("#hiddenLog_id").val();
		unCallFriend(receiver, sender);
		location.reload(true);
	});
	//친구신청 승낙 누를때
	$(document).on("click", "#consentCallFriendBtn", function() {
		var sender = $("#hiddenInfoMid").val();
		var receiver = $("#hiddenLog_id").val();
		consentCallFriend(receiver, sender);
		location.reload(true);
	});		
	//친구삭제 누를때
	$(document).on("click", "#deleteFriendBtn", function() {
		var receiver = $("#hiddenInfoMid").val();
		var sender = $("#hiddenLog_id").val();
		deleteFriend(receiver, sender);
		location.reload(true);
	});	
	
	
	//닫기버튼 누를때
	$(document).on("click", "#closeBtn", function() {
	close();
	});
});

</script>

<title>${userInfo.m_name}님의 정보</title>
</head>
<body>	
	<input id="hiddenLog_id" name="hiddenLog_id" type="hidden" value="${log_id}" />
	<input id="hiddenInfoMid" name="hiddenInfoMid" type="hidden" value="${userInfo.m_id}" />
	<input id="hiddenInfoGrade" name="hiddenInfoGrade" type="hidden" value="${userInfo.m_grade}" />
	<input id="hiddenInfoState1" name="hiddenInfoState1" type="hidden" value="${userInfo.m_state1}" />
	<input id="hiddenInfoPrivate" name="hiddenInfoPrivate" type="hidden" value="${userInfo.m_private}" />
	<input id="hiddenInfoBirth" name="hiddenInfoBirth" type="hidden" value="${userInfo.m_birth}" />
	<input id="hiddenInfoJoindate" name="hiddenInfoJoindate" type="hidden" value='${userInfo.m_joindate}' />
	<input id="hiddenlastAttendDay" name="hiddenlastAttendDay" type="hidden" value='${lastAttendDay}' />
	<br />
	<h2 class="info_title">회원정보</h2>
	<h4 class="info_title2">정보 조회</h4>
	<table class="info_table">
		<colgroup>
			<col width="120">
			<col width="230">
			<col width="120">
			<col width="230">
		</colgroup>
		<tbody>
			<tr>
				<th> <span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;회원 아이디</th>
				<td>${userInfo.m_id}</td>
				<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;이름</th>
				<td>${userInfo.m_name}</td>
			</tr>
			<tr>
				<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;이메일</th>
				<td>${userInfo.m_email}</td>
				<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;학년</th>
				<td id="infoGrade"></td>
			</tr>
			<tr>
				<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;지역</th>
				<td>${userInfo.m_region}</td>
				<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;성별</th>
				<td>${userInfo.m_sex}</td>
			</tr>
			<tr>
				<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;회원상태</th>
				<td id="infoState1"></td>
				<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;마지막 로그인</th>
				<td id="lastAttendDay"></td>
			</tr>
			<tr>
				<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;생년월일</th>
				<td id="infoBirth"></td>
				<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;가입일</th>
				<td id="joinDay"></td>
			</tr>
		</table>
		
		<table class="info_table2">
			<tr>
				<th colspan="2" style="font-weight: bold;"><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;자기소개</th>
			</tr>
			<tr>
				<td>
					<c:if test="${userInfo.m_profile != null && userInfo.m_profile != ''}">
						<img src="/uploadStorage/${userInfo.m_profile}" width="150px" height="200px" >
					</c:if>
				</td>
				<td rowspan="2">${userInfo.m_pr}</td>
			</tr>
			<tr>
				<td style="text-align: center; font-weight: bold;">프로필사진</td>
			</tr>
		</tbody>
	</table>
	<button id='msgWriterBtn' name='msgWriterBtn'>쪽지 보내기</button>
	<c:if test="${friendState == 0}">
		<button id='callFriendBtn' name='callFriendBtn'>친구신청</button>
	</c:if>
	<c:if test="${friendState == 1}">
		<button id='unCallFriendBtn' name='unCallFriendBtn'>친구신청취소</button>
	</c:if>
	<c:if test="${friendState == 2}">
		<button id='deleteFriendBtn' name='deleteFriendBtn'>친구삭제</button>
	</c:if>
	<c:if test="${friendState == 3}">
		<button id='consentCallFriendBtn' name='consentCallFriendBtn'>친구신청수락</button>
		<button id='refusalCallFriendBtn' name='refusalCallFriendBtn'>친구신청거부</button>
	</c:if>
	<button id='blacklist' name='blacklist'>블랙리스트 등록</button>
	<button id='closeBtn' name='closeBtn'>닫기</button>
</body>
</html>
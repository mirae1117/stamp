<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>포인트</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	//리스트 요청 함수
	$(document).ready(function() {
		$( window ).resize(function() {
			$(".myPageContent").css("width",($(window).width()-$(".mypageAside").width()-20));
			});
		listPoint();
	});
	/* 포인트 목록 뽑아오기 */
	function listPoint() {
		$("#pointTable").empty();
		$.getJSON("/mypage/listPoint.do", function(data) {
			if (data.length == 0) {
				$("#pointTable").append("<tr><td colspan='3'>포인트 내역이 없습니다</td></tr>");
			}else {
				$("#pointTable").append("<tr><td>시간</td><td>점수</td><td>분류</td><td>내용</td></tr>");
				$(data).each(function() {
					var p_time = this.p_time;
					var p_point = this.p_point;
					var p_state = this.p_state;
					var p_detail = this.p_detail;
					addNewItem(p_time, p_point, p_state, p_detail);
				});
			}
			$(".myPageContent").css("width",($(window).width()-$(".mypageAside").width()-20));
		}).fail(function() {
			alert("포인트 목록을 불러오는데 실패하였습니다. 잠시후에 다시 시도해 주세요");
		});
	}

	//친구목록을 화면에 추가하기 위한 함수
	function addNewItem(p_time, p_point, p_state, p_detail) {
		//새로운 글이 추가될 li태그 객체
		var new_tr = $("<tr>");

		//검색된 시간
		var p_time_td = $("<td class='p_time_td'>");
		p_time_td.html(p_time);
		//검색된 포인트
		var p_point_td = $("<td class='p_point_tr'>");
		p_point_td.html(p_point + "점");
		//검색된 상태
		var p_state_td = $("<td class='p_state_tr'>");
		var p_state_out = "";
		switch (p_state) {
		case 1:
			p_state_out = "충전"
			break;
		case 2:
			p_state_out = "출석"
			break;
		case 3:
			p_state_out = "답변작성"
			break;
		case 4:
			p_state_out = "질문작성"
			break;
		case 5:
			p_state_out = "회원가입"
			break;
		case 6:
			p_state_out = "이벤트"
			break;
		case 7:
			p_state_out = "운영자상벌"
			break;
		case 8:
			p_state_out = "포인트사용"
			break;
		default:
			p_state_out = "기타"
			break;
		}
		p_state_td.html(p_state_out);
		//검색된 내용
		var p_detail_td = $("<td class='p_detail_tr'>");
		p_detail_td.html(p_detail);

		//조립하기
		new_tr.append(p_time_td).append(p_point_td).append(p_state_td).append(p_detail_td);
		$("#pointTable").append(new_tr);

	}
</script>
<style type="text/css">
.info_title{
height:32px;
margin:50px 0px 20px 55px;
border-bottom: 1px solid #ededee;
font-size: 25px;
line-height: 30px;
line-height: 1.2em;
font-weight: 600;
padding: 0 0 40px;
}
.info_title2{
margin: 10px 10px 10px 60px;
padding:0px 20px 0 10px;
  background: url(/resources/include/img/bl_h3.gif) no-repeat left;
  font-size: 15px;
  font-weight: bold;
}
#pointTable{
font-size: 15px;
width: 700px;
}
.pointList{
margin-top:20px;
margin-left: 60px;
}
.id_span {
	
}

.name_span {
	padding-left: 30px;
}

.callFriendBtn, .unCallFriendBtn, .refusalCallFriendBtn {
	margin-left: 20px;
}
/* @media (max-width: 1012px) {
  #container{
    width: 1020px !important;
  }
} */
</style>
</head>
<body>
	<jsp:include page="../mainpage/header.jsp" />
	<div id="container">
	<jsp:include page="myAside.jsp" />
	<section class = "myPageContent">
	<h2 class="info_title">포인트 목록</h2>
		<h4 class="info_title2">포인트 사용내역(최근 내역 20건)</h4>
		
	<div class="table-responsive pointList">
	<table id="pointTable" class="table table-bordered">
	</table>
	</div>
	</section>
	</div>
	<jsp:include page="../mainpage/footer.jsp" />
</body>
</html>
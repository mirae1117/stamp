<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>정보수정 확인창</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<style type="text/css">
.form-control{
width: 170px;
display: inline-block;
}
#infoUpdateLogin th, #infoUpdateLogin td{
padding-top: 10px;
padding-bottom: 10px;
}
@media (max-width: 550px) {
  #container {
    width: 600px;
  }
}
</style>
</head>
<body>
	<jsp:include page="../mainpage/header.jsp" />
	<div id="container">
	<jsp:include page="myAside.jsp" />
	<section class = "myPageContent">
	<form id = "infoUpdateLogin" name="infoUpdateLogin">
	<input type="hidden" value='${log_id}' id="log_id" name="log_id">
	<table>
		<tr>
		<th colspan="2"><p style="font-size:20px; font-weight: bold;">정보를 수정하시겠습니까?</p></th>
		</tr>
		<tr>
			<th style="padding-right: 10px;">비밀번호</th>
			<td><input type="password" class="form-control" id="log_pw" name="log_pw" size="15"/></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align: center;">
				<button type="button" class="btn btn-success" id="infoUpdateBtn" name="infoUpdateBtn">확인</button>
				<button type="button" class="btn btn-default" id="outBtn" name="outBtn">나가기</button></td>
		</tr>
	</table>
	</form>
	</section>
	</div>
	<jsp:include page="../mainpage/footer.jsp" />
	<script type="text/javascript">
	$(function() {
		$("#infoUpdateBtn").click(function() {
			//입력값 체크
			if (!chkSubmit($('#log_pw'), "비밀번호를"))
					return;
			else {
				$("#infoUpdateLogin").attr({
					"method" : "POST",
					"action" : "/mypage/infoUpdateForm.do"
				});
				$("#infoUpdateLogin").submit();
			}
		});
	});
	</script>
</body>
</html>
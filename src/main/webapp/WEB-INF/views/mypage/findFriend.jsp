<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>친구 찾기</title>

<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript" src="/resources/include/js/friend.js"></script>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript">

$(document).on("click", ".callFriendBtn", function() {
	var other_member = $(this).parents("tr").attr("other_member");
	/** 글 저장을 위한 Post    방식의 Ajax 연동 처리 */
	$.ajax({
		url : "/friend/callFriend.do", //전송 url
		type : "post", // 전송 시  method 방식 
		headers : {
			"Content-Type" : "application/json",
			"X-HTTP-Method-Override" : "POST"
		},
		dataType : "text",
		data : JSON.stringify({
			m_receiver : other_member,
			m_sender : $("#log_id").val()
		}),
		error : function() { //실행시 오류가 발생하였을 경우 alert('시스템 오류 입니다. 관리자에게 문의   하세요');
		},
		success : function(resultData) {
			if (resultData != null && resultData != '') {
				var msg = "";
				switch (resultData) {
				case 'msg1':
					msg = '친구신청을 완료했습니다.';
					break;
				case 'msg2':
					msg = '추가로 친구신청이 불가능 합니다.';
					break;
				default:
					msg = '알수없는 에러. msg = ' + resultData;
					break;
				}
				alert(msg);
				location.reload(true);
			}
		}
	});
	
});
//쪽지 보내기 누를때
$(document).on("click", ".msgWriterBtn", function() {
	var receiver = $(this).parents("tr").attr("other_member");
	msgWriter(receiver);
});
function f_search() {
	$("#member_list").empty();
	var f_keyw = $("#f_keyw").val().toLowerCase();
	var url = "/friend/search/" + f_keyw + ".do";
	$.getJSON(url, function(data) {
		if (data.length == 0) {
			$("#member_list").append("<li>검색 된 회원이 없습니다</li>");
		}
		$(data).each(function() {
			var m_id = this.m_id;
			var m_name = this.m_name;
			addNewItem(m_id, m_name);
		});
	}).fail(function() {
		alert("친구 목록을 불러오는데 실패하였습니다. 잠시후에 다시 시도해 주세요");
	});
}

/* 검색된 회원을 화면에 추가하기 위한 함수 */
function addNewItem(m_id, m_name) {
	//검색된 회원이 지정될 <p>태그
	var otherMember = $("<tr>");		
	otherMember.attr("other_member", m_id);


	//검색된 아이디
	var id_span = $("<td class='other_member_id_span'>");
	id_span.html("id : <a href=javascript:otherUserInfo('" + m_id + "')>" + m_id + "</a>");

	//검색된 이름
	var name_span = $("<td class='name_span'>");
	name_span.html(m_name + "님");

	//쪽지 보내기
	var msgWriterBtn = $("<input class='btn btn-default'>");
	msgWriterBtn.attr({
		"type" : "button",
		"value" : "쪽지보내기"
	});
	msgWriterBtn.addClass("msgWriterBtn");
	
	//친구신청 버튼
	var callFriendBtn = $("<input class='btn btn-default'>");
	callFriendBtn.attr({
		"type" : "button",
		"value" : "친구신청"
	});
	callFriendBtn.addClass("callFriendBtn");
	var btn_td =$("<td class='btn_td'>");
	btn_td.append(msgWriterBtn).append(callFriendBtn);
	
	//조립하기
	otherMember.append(id_span).append(name_span).append(btn_td)
	$("#member_list").append(otherMember);
}

</script>
<style type="text/css">
.info_title{
height:32px;
margin:50px 0px 20px 55px;
border-bottom: 1px solid #ededee;
font-size: 25px;
line-height: 30px;
line-height: 1.2em;
font-weight: 600;
padding: 0 0 40px;
}
.info_title2{
margin: 10px 10px 10px 60px;
padding:0px 20px 0 10px;
  background: url(/resources/include/img/bl_h3.gif) no-repeat left;
  font-size: 15px;
  font-weight: bold;
}
.form-control{
width: 170px;
display: inline-block;
}
.friend_search{
padding-left: 70px;
}
#member_list{
margin-top:3px;

}
#member_list td{
font-size: 15px;
vertical-align: middle;
}
#f_search{
display: inline;
}
@media (max-width: 750px) {
  #container{
    width: 800px !important;
  }
}
</style>
</head>
<body>
	<jsp:include page="../mainpage/header.jsp" />
	<div id="container">
	<jsp:include page="myAside.jsp" />
	<section class = "myPageContent">
	<h2 class="info_title">친구 정보</h2>
	<input type="hidden" value='${log_id}' id="log_id" name="log_id">
	<h4 class="info_title2">친구찾기</h4>
	<div class="friend_search">
	<table>
		<tr>
			<td><input type="text" class="form-control" name='f_keyw' id='f_keyw' size="20"/></td>
			<td><button type="button" class="btn btn-default" name='f_search' id='f_search' onclick="f_search()">검색</button></td>
		</tr>
	</table>
	<table id="member_list" class="table table-hover">
		<!-- 친구찾기 검색시 -->
	</table>
	</div>
	</section>
	</div>
	<jsp:include page="../mainpage/footer.jsp" />
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>친구</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript" src="/resources/include/js/friend.js"></script>
<script type="text/javascript">
	//리스트 요청 함수
	$(function() {
		$('a[data-toggle="tab"]').on('hidden.bs.tab', function() {
			alert("이벤트 실행됨");
		});
		
		serveCall();
		receiveCall();
		listFriend();

		//친구신청 취소 누를때
		$(document).on("click", ".unCallFriendBtn", function() {
			var receiver = $(this).parents("tr").attr("call_Friend");
			var sender = $("#log_id").val();
			unCallFriend(receiver, sender);
			location.reload(true);
		});
		//친구신청 거부 누를때
		$(document).on("click", ".refusalCallFriendBtn", function() {
			var sender = $(this).parents("tr").attr("receive_Call_Friend");
			var receiver = $("#log_id").val();
			unCallFriend(receiver, sender);
			location.reload(true);
		});
		//친구신청 승낙 누를때
		$(document).on("click", ".consentCallFriendBtn", function() {
			var sender = $(this).parents("tr").attr("receive_Call_Friend");
			var receiver = $("#log_id").val();
			consentCallFriend(receiver, sender);
			location.reload(true);
		});		
		//친구삭제 누를때
		$(document).on("click", ".deleteFriendBtn", function() {
			var receiver = $(this).parents("tr").attr("friend");
			var sender = $("#log_id").val();
			deleteFriend(receiver, sender);
		location.reload(true);
			
			
		});
		//쪽지 보내기 누를때1
		$(document).on("click", ".msgWriterBtn1", function() {
			var receiver = $(this).parents("tr").attr("call_Friend");
			msgWriter(receiver);
		});
		//쪽지 보내기 누를때2
		$(document).on("click", ".msgWriterBtn2", function() {
			var receiver = $(this).parents("tr").attr("receive_Call_Friend");
			msgWriter(receiver);
		});
		//쪽지 보내기 누를때3
		$(document).on("click", ".msgWriterBtn3", function() {
			var receiver = $(this).parents("tr").attr("friend");
			msgWriter(receiver);
		});
		
	});
	/* 신청중인 목록 뽑아오기 */
	function serveCall() {
		$("#serveCallFriend").empty();
		$.getJSON("/friend/serveCall.do", function(data) {
			if (data.length == 0) {
				$("#serveCallFriend").append("<tr><td colspan='3'>신청한 친구가 없습니다</td></tr>");
			}

			$(data).each(function() {
				var m_id = this.m_id;
				var m_name = this.m_name;
				addNewItem2(m_id, m_name);
			});
		}).fail(function() {
			alert("친구 목록을 불러오는데 실패하였습니다. 잠시후에 다시 시도해 주세요");
		});
	}
	/* 신청받은 목록 뽑아오기 */
	function receiveCall() {
		$("#receiveCallFriend").empty();
		$.getJSON("/friend/receiveCall.do", function(data) {
			if (data.length == 0) {
				$("#receiveCallFriend").append("<tr><td colspan='3'>신청받은 친구 내역이 없습니다</td></tr>");
			}

			$(data).each(function() {
				var m_id = this.m_id;
				var m_name = this.m_name;
				addNewItem3(m_id, m_name);
			});
		}).fail(function() {
			alert("친구 목록을 불러오는데 실패하였습니다. 잠시후에 다시 시도해 주세요");
		});
	}
	/* 친구 목록 뽑아오기 */
	function listFriend() {
		$("#receiveCallFriend").empty();
		$.getJSON("/friend/listFriend.do", function(data) {
			if (data.length == 0) {
				$("#friendList").append("<tr><td colspan='3'>친구 내역이 없습니다</td></tr>");
			}

			$(data).each(function() {
				var m_id = this.m_id;
				var m_name = this.m_name;
				addNewItem4(m_id, m_name);
			});
		}).fail(function() {
			alert("친구 목록을 불러오는데 실패하였습니다. 잠시후에 다시 시도해 주세요");
		});
	}
	//신청 중인 회원을 화면에 추가하기 위한 함수
	function addNewItem2(m_id, m_name) {
		//검색된 회원이 지정될 <p>태그
		var call_Friend = $("<tr>");
		call_Friend.attr("call_Friend", m_id);
		
		//검색된 아이디
		var id_span = $("<td class='call_Friend_id_span'>");
		id_span.html("ID : <a href=javascript:otherUserInfo('" + m_id + "')>" + m_id + "</a>");

		//검색된 이름
		var name_span = $("<td class='name_span'>");
		name_span.html(m_name + "님");
		
		//쪽지 보내기
		var msgWriterBtn1 = $("<input class='btn btn-default'>");
		msgWriterBtn1.attr({
			"type" : "button",
			"value" : "쪽지보내기"
		});
		msgWriterBtn1.addClass("msgWriterBtn1");

		//친구신청 취소 버튼
		var unCallFriendBtn = $("<input class='btn btn-default'>");
		unCallFriendBtn.attr({
			"type" : "button",
			"value" : "친구신청 취소"
		});
		unCallFriendBtn.addClass("unCallFriendBtn");
		var btn_td =$("<td class='btn_td'>");
		btn_td.append(msgWriterBtn1).append(unCallFriendBtn);
		//조립하기
		
		call_Friend.append(id_span).append(name_span).append(btn_td)
		$("#serveCallFriend").append(call_Friend);
	}
	//친구 신청받은 내역을 화면에 추가하기 위한 함수
	function addNewItem3(m_id, m_name) {

		//검색된 회원이 지정될 <p>태그
		var receive_Call_Friend = $("<tr>");
		receive_Call_Friend.attr("receive_Call_Friend", m_id);

		//검색된 아이디
		var id_span = $("<td class='receive_Call_Friend_id_span'>");
		id_span.html("ID : <a href=javascript:otherUserInfo('" + m_id + "')>" + m_id + "</a>");

		//검색된 이름
		var name_span = $("<td class='name_span'>");
		name_span.html(m_name + "님");
		
		//쪽지 보내기
		var msgWriterBtn2 = $("<input class='btn btn-default'>");
		msgWriterBtn2.attr({
			"type" : "button",
			"value" : "쪽지보내기"
		});
		msgWriterBtn2.addClass("msgWriterBtn2");
		
		//친구신청 거절 버튼
		var refusalCallFriendBtn = $("<input class='btn btn-default'>");
		refusalCallFriendBtn.attr({
			"type" : "button",
			"value" : "친구신청 거절"
		});
		refusalCallFriendBtn.addClass("refusalCallFriendBtn");

		//친구신청 승낙 버튼
		var consentCallFriendBtn = $("<input class='btn btn-default'>");
		consentCallFriendBtn.attr({
			"type" : "button",
			"value" : "친구신청 승낙"
		});
		consentCallFriendBtn.addClass("consentCallFriendBtn");
		
		var btn_td =$("<td class='btn_td'>");
		btn_td.append(msgWriterBtn2).append(refusalCallFriendBtn).append(consentCallFriendBtn)
		//조립하기
		receive_Call_Friend.append(id_span).append(name_span).append(btn_td)
		$("#receiveCallFriend").append(receive_Call_Friend);
	}
	//친구목록을 화면에 추가하기 위한 함수
	function addNewItem4(m_id, m_name) {
		//검색된 회원이 지정될 <p>태그
		var receive_Call_Friend = $("<tr>");
		receive_Call_Friend.attr("friend", m_id);
		
		//검색된 아이디
		var id_span = $("<td class='friendList_id_span'>");
		id_span.html("ID : <a href=javascript:otherUserInfo('" + m_id + "')>" + m_id + "</a>");

		//검색된 이름
		var name_span = $("<td class='name_span'>");
		name_span.html(m_name + "님");


		//쪽지 보내기
		var msgWriterBtn3 = $("<input class='btn btn-default'>");
		msgWriterBtn3.attr({
			"type" : "button",
			"value" : "쪽지보내기"
		});
		msgWriterBtn3.addClass("msgWriterBtn3");
		
		//친구 삭제 버튼
		var deleteFriendBtn = $("<input class='btn btn-default'>");
		deleteFriendBtn.attr({
			"type" : "button",
			"value" : "친구삭제"
		});
		deleteFriendBtn.addClass("deleteFriendBtn");
		
		var btn_td =$("<td class='btn_td'>");
		btn_td.append(msgWriterBtn3).append(deleteFriendBtn)
		//조립하기
		receive_Call_Friend.append(id_span).append(name_span).append(btn_td)
		$("#friendList").append(receive_Call_Friend);
	}
</script>

<style type="text/css">
.info_title{
height:32px;
margin:50px 0px 20px 55px;
border-bottom: 1px solid #ededee;
font-size: 25px;
line-height: 30px;
line-height: 1.2em;
font-weight: 600;
padding: 0 0 40px;
}
.id_span {
	
}

.name_span {
	padding-left: 30px;
}

.callFriendBtn, .unCallFriendBtn, .refusalCallFriendBtn .msgWriterBtn1 .msgWriterBtn2 .msgWriterBtn3{
	margin-left: 20px;
}
#friend_tabpane{
padding-left: 60px;
}
#myTab{
font-size: 15px;}
#myTabContent ul{
font-size: 20px;
}
#myTabContent{

margin-top: -5px;}
#serveCallFriend td{
font-size: 15px;
vertical-align: middle;

}
#receiveCallFriend td{
font-size: 15px;
vertical-align: middle;
}
#friendList td{
font-size: 15px;
vertical-align: middle;
}
@media (max-width: 850px) {
  #container{
    width: 860px !important;
  }
}
</style>
</head>
<body>
	<jsp:include page="../mainpage/header.jsp" />
	<div id="container">
	<jsp:include page="myAside.jsp" />
	<section class = "myPageContent">
	<div id="friendContent">
	<h2 class="info_title">친구 정보</h2>
	<div id="friend_tabpane">
	<input type="hidden" value='${log_id}' id="log_id" name="log_id">

	<ul id="myTab" class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a
						data-target="#friend_call" id="friend_call-tab" role="tab" data-toggle="tab"
						aria-controls="friend_call" aria-expanded="true">친구 신청 </a></li>
						
					<li role="presentation"><a data-target="#friend_recall"
						role="tab" id="friend_recall-tab" data-toggle="tab"
						aria-controls="friend_recall" aria-expanded="false">친구 요청 </a></li>
						
						<li role="presentation"><a data-target="#friend_list"
						role="tab" id="friend_list-tab" data-toggle="tab"
						aria-controls="friend_list" aria-expanded="false">친구</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div role="tabpanel" class="tab-pane fade active in" id="friend_call"
						aria-labelledby="friend_call-tab">
						<table id="serveCallFriend" class="table table-hover"></table>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="friend_recall"
						aria-labelledby="friend_recall-tab">
						<table id="receiveCallFriend" class="table table-hover"></table></div>
						
						<div role="tabpanel" class="tab-pane fade" id="friend_list"
						aria-labelledby="friend_list-tab" class="table table-striped">
		<table id="friendList" class="table table-hover"></table></div>
						
		
		<!-- (추가예정) 현재 친구인 (상태1) 목록을 뽑아옴  친구삭제도 추가 -->
				</div>
	</div>
	</div>
	</section>
	</div>
	<jsp:include page="../mainpage/footer.jsp" />
</body>
</html>
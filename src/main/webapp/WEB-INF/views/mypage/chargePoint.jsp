<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>포인트 충전</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript">

$(function() {
	/*포인트 충전 클릭시 처리 이벤트*/
	$("#chargePointBtn").click(function() {
		//입력값 체크
		if ($("#p_point > option:selected").val() == '0'){
			alert("충전 포인트를 확인해 주세요.");
			return;
		}
		else if (!chkSubmit($('#p_detail'), "충전 내용을")){
			return;
		}
		else {
			$.ajax({
				url : "/mypage/chargePoint.do", //전송 url
				type : "post", // 전송 시  method 방식 
				headers : {
					"Content-Type" : "application/json",
					"X-HTTP-Method-Override" : "POST"
				},
				dataType : "text",
				data : JSON.stringify({
					p_point : $("#p_point > option:selected").val(),
					m_id : $("#log_id").val(),
					p_detail : $('#p_detail').val()
				}),
				error : function() { //실행시 오류가 발생하였을 경우 
					alert('시스템 오류 입니다. 관리자에게 문의   하세요');
				},
				success : function(resultData) {
					if (resultData != null && resultData != '') {
						var msg = "";
						switch (resultData) {
						case 'msg1':
							msg = "포인트 충전이 완료 되었습니다. \n 충전포인트 : " + $("#p_point > option:selected").val() + "\n 충전 내용 : " + $("#p_detail").val();
							break;
						default:
							msg = '알수없는 에러. msg = ' + resultData;
							break;
						}
						alert(msg);
						window.opener.document.location.href = window.opener.document.URL;
						close();
					}
				}
			});
		}
	});
	$("#close").click(function() {
		close();
	});
});

</script>
<style type="text/css">

</style>
</head>
<body>
	<input type="hidden" name="log_id" id="log_id" value="${log_id}">
	<form id="chargePointForm"  name="chargePointForm">
		<table class="table table-bordered">
			<tr>
				<td>충전 포인트</td>
				<td><select class="form-control" id="p_point" name="p_point">
						<option value="0" selected="selected">-선택해주세요-</option>
						<option value="100">100</option>
						<option value="500">500</option>
						<option value="1000">1000</option>
						<option value="3000">3000</option>
						<option value="5000">5000</option>
						<option value="10000">10000</option>
						<option value="30000">30000</option>
						<option value="50000">50000</option>
				</select></td>
			</tr>
			<tr>
				<td>충전 내용</td>
				<td><input type="text" class="form-control" name=p_detail id="p_detail" value=""/></td>
			</tr>
			<tr style="text-align: center;">
			<td colspan="2"><button id="chargePointBtn" class="btn btn-success" name="chargePointBtn">충전</button>
			<button id="close" class="btn btn-default" name="close">나가기</button></td>
			</tr>
		</table>
	</form>
</body>
</html>
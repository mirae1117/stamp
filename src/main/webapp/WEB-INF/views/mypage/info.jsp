<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>마이페이지</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript" src="/resources/include/js/userInfo.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$( window ).resize(function() {
		$(".myPageContent").css("width",($(window).width()-$(".mypageAside").width()-20));
		});
	$(".myPageContent").css("width",($(window).width()-$(".mypageAside").width()-20));
});
	function infoUpdate() {
		location.href = "/mypage/infoUpdate.do";
	}	
	function friends() {
		location.href = "/mypage/friends.do";
	}
	function points() {
		location.href = "/mypage/points.do";
	}
</script>
<style type="text/css">


section ul, section li{
clear: left;
}
.info_table{
margin: 0 auto;
font-size: 15px;
margin-left: 60px;
}
.info_table2{
margin-top:10px;
margin-left:60px;
font-size: 15px;
}
.info_title{
height:32px;
margin:50px 0px 20px 55px;
border-bottom: 1px solid #ededee;
font-size: 25px;
line-height: 30px;
line-height: 1.2em;
font-weight: 600;
padding: 0 0 40px;
}
.info_title2{
margin: 10px 10px 10px 60px;
padding:0px 20px 0 10px;
  background: url(/resources/include/img/bl_h3.gif) no-repeat left;
  font-size: 15px;
  font-weight: bold;
}

.info_table th{
height: 40px;
font-weight: bold;
}
.info_table td{
padding: 10px;}

.info_table2 td{
padding:10px;
vertical-align: top;}
.tableWrap{
width: 100%;
}
.info_table th{
width: 150px}
.info_table td{
width: 250px}
@media (max-width: 972px) {
  .info_table{
    width: 860px !important;
  }
}
</style>
</head>
<body>
<div id="wrap">
	<jsp:include page="../mainpage/header.jsp" />
	<div id="container">
	<jsp:include page="myAside.jsp" />
	<section class = "myPageContent">
	<input id="hiddenInfoGrade" name="hiddenInfoGrade" type="hidden"
		value="${userInfo.m_grade}" />
	<input id="hiddenInfoState1" name="hiddenInfoState1" type="hidden"
		value="${userInfo.m_state1}" />
	<input id="hiddenInfoPrivate" name="hiddenInfoPrivate" type="hidden"
		value="${userInfo.m_private}" />
	<input id="hiddenInfoBirth" name="hiddenInfoBirth" type="hidden"
		value="${userInfo.m_birth}" />
	<input id="hiddenInfoJoindate" name="hiddenInfoJoindate" type="hidden"
		value='${userInfo.m_joindate}' />
	<input id="hiddenlastAttendDay" name="hiddenlastAttendDay" type="hidden"
		value='${lastAttendDay}' />
		<br />
		<h2 class="info_title">회원정보</h2>
		<h4 class="info_title2">정보 조회</h4>
		<div class="tableWrap">
		<table class="info_table">
		<tbody>
		<tr>
		<th> <span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;회원 아이디</th>
		<td>${userInfo.m_id}</td>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;이름</th>
		<td>${userInfo.m_name}</td></tr>
		<tr>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;이메일</th>
		<td>${userInfo.m_email}</td>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;학년</th>
		<td id="infoGrade"></td>
		</tr>
		<tr>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;지역</th>
		<td>${userInfo.m_region}</td>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;성별</th>
		<td>${userInfo.m_sex}</td>
		</tr>
		<tr>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;회원상태</th>
		<td id="infoState1"></td>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;공개여부</th>
		<td id="infoPrivate"></td></tr>
		<tr>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;생년월일</th>
		<td id="infoBirth"></td>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;가입일</th>
		<td id="joinDay"></td></tr>
		<tr>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;마지막 로그인</th>
		<td id="lastAttendDay"></td>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;총 포인트</th>
		<td id="memberPoints">${memberPoints}점</td></tr>
		</table>
		
		<table class="info_table2">
		<tr>
		<th colspan="2" style="font-weight: bold;"><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;자기소개</th></tr>
		<tr>
		<td><c:if
				test="${userInfo.m_profile != null && userInfo.m_profile != ''}">
				<img src="/uploadStorage/${userInfo.m_profile}" width="150px" height="200px" >
			</c:if></td>
		
		<td rowspan="2">${userInfo.m_pr}</td>
		
		</tr>
		<tr>
		<td style="text-align: center; font-weight: bold;">프로필사진</td></tr>
		</tbody>

	</table>
	</div>
	</section>
	</div>
	<jsp:include page="../mainpage/footer.jsp" />
	</div>
</body>
</html>
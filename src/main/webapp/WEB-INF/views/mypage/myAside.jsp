<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<script type="text/javascript">
	$(function() {
		$(document).on("click", "#chargePoint", function() {
			window.open("/mypage/chargePoint.do", "", "width=600,height=400");
		});
	});
</script>

<style type="text/css">
h1, h2, h3, h4, h5, h6, p, dl, dt, dd, table, thead, tbody, tr, th, td {
	margin: 0;
	padding: 0;
	font-weight: normal;
}

.mypage_lnb {
	list-style: none;
}

.mypageAside {
	margin-left: 15px;
	width: 220px;
	display: inline-block;
	float: left;
}

.myPageContent {
	margin-top: 50px;
	float: left;
}

.mypage_title {
	margin: 100px 0 20px;
	height: 36px;
	background: url(/resources/include/img/lnb_tit.gif) no-repeat left
		bottom;
	font-size: 22px;
	color: #272727;
}

.mypage_lnb li {
	margin-top: 6px;
}

#container {
	position: relative;
	margin: auto;
	width: 100%;
}
</style>
<aside class="mypageAside">
	<h3 class="mypage_title">마이 페이지</h3>
	<ul class="mypage_lnb">
		<li>회원정보
			<ul>
				<li><a href="/mypage/info.do">회원정보 조회</a></li>
				<li><a href="/mypage/infoUpdate.do">회원정보 수정, 탈퇴</a></li>
			</ul>
		</li>
		<li>포인트
			<ul>
				<li><a href="/mypage/points.do">포인트 내역</a></li>
				<li><a href="#" id="chargePoint">포인트 충전</a></li>
			</ul>
		</li>
		<li>친구
			<ul>
				<li><a href="/mypage/friends.do">친구 정보</a></li>
				<li><a href="/mypage/findFriend.do">친구 찾기</a></li>
			</ul>
		</li>
		<li>쪽지함
			<ul>
				<li><a href="/message/messageBox/getBox.do">받은 쪽지함</a></li>
				<li><a href="/message/messageBox/sendBox.do">보낸 쪽지함</a></li>
			</ul>
		</li>
	</ul>
</aside>
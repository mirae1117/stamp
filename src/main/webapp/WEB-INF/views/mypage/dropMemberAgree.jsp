<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>정보수정 확인창</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	$(function() {
		$("#dropBtn").click(function() {
			//입력값 체크
			if (!chkSubmit($('#log_pw'), "비밀번호를"))
				return;
			else {
				$("#dropLogin").attr({
					"method" : "POST",
					"action" : "/mypage/dropMember.do"
				});
				$("#dropLogin").submit();
			}
		});
	});
</script>
<style type="text/css">
.form-control {
	width: 170px;
	display: inline-block;
}

.dropAgree_btn {
	padding-top: 230px;
	padding-left: 180px;
}

.drop_btn {
	padding-top: 10px;
	padding-left: 300px;
}

.section {
	margin-top: 150px;
}
</style>
</head>
<body>
	<jsp:include page="../mainpage/header.jsp" />
	<div id="container">
		<jsp:include page="myAside.jsp" />
		<div id="section">
			<section class="myPageContent">

				<div class="dropAgree_btn">

					<p style="font-size: 15px; color: red;">
						회원 탈퇴를 하면 더이상 서비스 이용이 불가능합니다.<br /> 회원정보는 법적문제에따라 약 6개월 보관후에
						정기적으로 삭제됩니다.<br /> <strong style="font-size: 20px;">탈퇴하시겠습니까?</strong>
					</p>
					<form id="dropLogin" name="dropLogin"
						style="padding-top: 20px; padding-left: 100px;">
						<input type="hidden" value='${log_id}' id="log_id" name="log_id">
						비밀번호 <input type="password" class="form-control" id="log_pw"
							name="log_pw" />

					</form>
				</div>
				<div class="drop_btn">
					<button type="button" class="btn btn-danger" id="dropBtn"
						name="infoUpdateBtn">예</button>
					<button type="button" class="btn btn-default" id="outBtn"
						name="outBtn">나가기</button>
				</div>

			</section>
		</div>
	</div>
</body>
</html>
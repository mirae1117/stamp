<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>나의 정보 수정 폼</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var emailAt = $('#m_email').val().search("@");
		var email1 = "";
		var email2 = "";
		var birth = new Date($('#hiddenBirth').val());
		var today = new Date();
		for (var i = 0; i < $('#m_email').val().length; i++) {
			if (i < emailAt) {
				email1 += $('#m_email').val().charAt(i);
			} else if (i >= emailAt + 1){
				email2 += $('#m_email').val().charAt(i);
			}
		}
		$('#email1').val(email1);
		$('#email2').val(email2);
		$("input:radio[name='m_sex']:radio[value='"+$('#hiddenM_sex').val()+"']").prop("checked",true);
		$("select[name=m_region] option[value="+$('#hiddenRegion').val()+"]").attr("selected",true);
		$('#m_grade').val($('#hiddenGrade').val());
		$("input:radio[name='m_private']:radio[value='"+$('#hiddenM_private').val()+"']").prop("checked",true);
		/* 정보 수정 버튼 클릭시 처리 이벤트*/
		$("#mUpdateBtn").click(function() {
			//입력값 체크
			if (!chkSubmit($('#m_pw'), "비밀번호를"))
					return;
			else if ($('#m_pw').val() != $('#m_pw2').val()){
				alert("비밀번호가 서로 다릅니다");
				return;
			}
			else if (!chkSubmit($('#email1'), "이메일 앞자리를"))
				return;
			else if (!chkSubmit($('#email2'), "이메일 뒷자리를"))
				return;
			else {
				$("#m_email").val($("#email1").val() + "@" + $("#email2").val());
				$("#infoUpdateForm").attr({
					"method" : "POST",
					"action" : "/mypage/infoUpdatePro.do"
				});
				$("#infoUpdateForm").submit();
			}
		}); /*탈퇴 버튼 클릭시 처리 이벤트*/
		$("#dropMemberBtn").click(function() {
			location.href="/mypage/dropMember.do";
		}); /*취소 버튼 클릭시 처리 이벤트*/
		$("#cancel").click(function() {
			location.href="/mypage/info.do";
		});
		$("#emailList").change(function() {
			if ($("#emailList option:selected").val() == '0') {
				$("#email2").val('');
				$("#email2").attr("readonly", false);
			} else  {
				if ($("#emailList option:selected").val() == '-1') {
					$("#email2").val('');
				} else {
					$("#email2").val($("#emailList").val());
				}
				$("#email2").attr("disabled", true);
			}
		});
	});
</script>
<style type="text/css">
.info_title{
height:32px;
margin:50px 0px 20px 55px;
border-bottom: 1px solid #ededee;
font-size: 25px;
line-height: 30px;
line-height: 1.2em;
font-weight: 600;
padding: 0 0 40px;
}
.info_title2{
margin: 10px 10px 10px 60px;
padding:0px 20px 0 10px;
  background: url(/resources/include/img/bl_h3.gif) no-repeat left;
  font-size: 15px;
  font-weight: bold;
}
.info_table{
margin: 0;
margin-left:65px;
font-size: 15px;
}
.info_table3{
margin: 0;
margin-top:10px;
margin-left:65px;
font-size: 15px;
}
.info_table th{
height: 40px;
font-weight: bold;
}
.info_table td{
padding: 10px;
}

.info_table3 td{
vertical-align: top;
}
.form-control{
width: 170px;
display: inline-block;
}
textarea.form-control{
width: 80%;
height: 150px;
}
.mypageUpdate_btn{
   padding-top: 30px;
   padding-left: 250px;
   margin-bottom: 50px;
}
</style>
</head>
<body>
<div id="wrap">
	<jsp:include page="../mainpage/header.jsp" />
	<div class="container">
	<section class = "myPageContent">
	<h2 class="info_title">회원정보</h2>
		<h4 class="info_title2">회원정보 수정, 탈퇴</h4>
	<form name="infoUpdateForm" id="infoUpdateForm"	enctype="multipart/form-data">
		<table class="info_table">
		<colgroup>
		<col width="120">
		<col width="230">
		<col width="150">
		<col width="230">
		</colgroup>
		<tbody>
		<tr>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;회원 아이디</th><td><input type="hidden" id="m_id" name="m_id"  value="${userInfo.m_id}">${userInfo.m_id}</td>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;이름</th><td>	<input type="text" class="form-control" id="m_name" name="m_name" value="${userInfo.m_name}"></td></tr>
		<tr>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;비밀번호</th><td><input type="password" class="form-control"  id="m_pw" name="m_pw"></td>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;비밀번호 확인</th><td> <input type="password" class="form-control" id="m_pw2" name="m_pw2"> </td></tr>
		<tr>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;이메일</th> 
		<td class="form-inline" colspan="3">
		<input type="text" class="form-control" id="email1" name="email1" />@
		<input type="text" id="email2" class="form-control" name="email2" readonly="readonly" size="10"/>
		<select class="form-control" id="emailList" name="emailList">
						<option value='-1'>-이메일 선택-</option>
						<option value='0'>직접입력</option>
						<option value='gmail.com'>gmail.com</option>
						<option value='naver.com'>naver.com</option>
						<option value='nate.com'>nate.com</option>
						<option value='hanmail.net'>hanmail.net</option>
						<option value='daum.com'>daum.com</option>	
					</select>
			<input type="hidden" id="m_email" name="m_email" value="${userInfo.m_email}"/></td></tr>
		<tr><th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;성별</th> <td colspan="3"><input type="radio" name="m_sex" id="m_sex" value="남성">남성
				<input type="radio" name="m_sex" id="m_sex" value="여성">여성
				<input type="hidden" id="hiddenM_sex" name="hiddenM_sex" value="${userInfo.m_sex}"/></td></tr>
		<tr>
		 
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;지역</th>	<td><select id="m_region" class="form-control" name="m_region">
					<option value="서울">서울</option>
					<option value="경기">경기</option>
					<option value="인천">인천</option>
					<option value="충북">충북</option>
					<option value="충남">충남</option>
					<option value="대전">대전</option>
				</select> <br />
				<input type="hidden" name="hiddenRegion" id="hiddenRegion" value="${userInfo.m_region}" /></td> 
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;학년</th><td>	<select id="m_grade" class="form-control" name="m_grade">
					<option value="0">미취학</option>
					<option value="1" selected="selected">초등학교 1학년</option>
					<option value="2">초등학교 2학년</option>
					<option value="3">초등학교 3학년</option>
					<option value="4">초등학교 4학년</option>
					<option value="5">초등학교 5학년</option>
					<option value="6">초등학교 6학년</option>
					<option value="7">중학교 1학년</option>
					<option value="8">중학교 2학년</option>
					<option value="9">중학교 3학년</option>
					<option value="10">고등학교 1학년</option>
					<option value="11">고등학교 2학년</option>
					<option value="12">고등학교 3학년</option>
					<option value="13">대학생</option>
					<option value="14">일반</option>
					<option value="15">기타</option>
				</select>
				<input type="hidden" name="hiddenGrade" id="hiddenGrade" value="${userInfo.m_grade}" /></td></tr>
		<tr>
		<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;생일</th><td><input type="date" class="form-control" name="m_birth" id="m_birth" value="${userInfo.m_birth}" disabled="disabled"> 
				<input type="hidden" name="hiddenBirth" id="hiddenBirth" value="${userInfo.m_birth}" /> 
				<input id="hiddenBirth" name="hiddenBirth" type="hidden" value='${userInfo.m_birth}' /></td>
				<th><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;개인정보공개여부</th><td>
				<input	type="radio" name="m_private" id="m_private" value="1">예 
						 <input type="radio" name="m_private" id="m_private" value="0">아니요<br />
						 <input type="hidden" id="hiddenM_private" name="hiddenM_private" value="${userInfo.m_private}"/></td>
				</tr>
				</table>
				<table class="info_table3">
				<tr>
				<th style="font-weight: bold;"><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;프로필사진</th>
				<th style="font-weight: bold;"><span class="glyphicon glyphicon-play" aria-hidden="true"></span>&nbsp;자기소개</th></tr>
				<tr>
				<td><c:if test="${userInfo.m_profile != null && userInfo.m_profile != ''}">
			<img src="/uploadStorage/${userInfo.m_profile}" width="150px" height="200px">
		</c:if></td>
				 <td rowspan="2"><textarea rows="4" cols="100" class="form-control" name="m_pr" id="m_pr">${userInfo.m_pr}</textarea></td>
		</tr>
		<tr>
		<td style="text-align: center; font-weight: bold;"><input type="file" name="file" id="file"></td></tr>
		</tbody> 
	</tbody>
	</table>
	</form>
	<div class="mypageUpdate_btn">
	<button type="button" class="btn btn-success" id="mUpdateBtn" name="mUpdateBtn" >회원 수정 완료</button>
	<button type="button" class="btn btn-danger" id="dropMemberBtn" name="dropMemberBtn">회원 탈퇴</button>
	<button type="button" class="btn btn-default" id="cancel" name="cancel">취소</button>
	</div>
	</section>
	</div>
	<jsp:include page="../mainpage/footer.jsp" />
	</div>
</body>
</html>
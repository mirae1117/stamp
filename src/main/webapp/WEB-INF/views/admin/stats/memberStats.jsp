<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<!DOCTYPE>
<html>
<head>
<meta charset=UTF-8">
<title>통계 페이지</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="/resources/include/css/stats.css" />
<script src="http://code.jquery.com/jquery-latest.min.js"></script>

<!-- 구글 차트 작성에 필요한 js 파일 -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#logo").click(function() {
			location.href = "/admin/adminPage.do";
		});
		
		$("#home").click(function() {
			location.href = "/main/main.do";
		});
	});

	//차트 라이브러리 로딩
	google.load('visualization', '1', {
		'packages' : [ 'corechart' ]
	});

	//콜백 함수 작성
	google.setOnLoadCallback(drawChart1);
	google.setOnLoadCallback(drawChart2);
	google.setOnLoadCallback(drawChart3);
	google.setOnLoadCallback(drawChart4);
	google.setOnLoadCallback(drawChart5);
	google.setOnLoadCallback(drawChart6);

	//월별 가입자 차트 그리기 함수
	function drawChart1() {
		//json 데이터 받아오기
		// dataType : "json" - 결과 값이 json형식
		// async : false - 비동기식 옵션을 끔 (동기식)
		// responseText - 서버의 응답 텍스트
		var jsonData = $.ajax({
			url : "${path}/stats/memberJoinChart.do",
			dataType : "json",
			async : false
		}).responseText;
		console.log(jsonData);
		//json 데이터를 데이터테이블로 변환
		var data = new google.visualization.DataTable(jsonData);
		//차트 그리기
		var chart = new google.visualization.LineChart(document
				.getElementById('chart_div1'));
		//draw(데이터, 옵션)
		chart.draw(data, {
			title : "월별 가입자 현황",
			width : 1250,
			height : 350
		});
	}

	//월별 가입자 차트 그리기 함수
	function drawChart2() {
		//json 데이터 받아오기
		// dataType : "json" - 결과 값이 json형식
		// async : false - 비동기식 옵션을 끔 (동기식)
		// responseText - 서버의 응답 텍스트
		var jsonData = $.ajax({
			url : "${path}/stats/memberGenderChart.do",
			dataType : "json",
			async : false
		}).responseText;
		console.log(jsonData);
		//json 데이터를 데이터테이블로 변환
		var data = new google.visualization.DataTable(jsonData);
		//차트 그리기
		var chart = new google.visualization.BarChart(document
				.getElementById('chart_div2'));
		//draw(데이터, 옵션)
		chart.draw(data, {
			title : "남녀 가입자 현황",
			width : 1100,
			height : 350,
			bar : {
				groupWidth : '50%'
			}
		});
	}

	//학년별 가입자 차트 그리기 함수
	function drawChart3() {
		//json 데이터 받아오기
		// dataType : "json" - 결과 값이 json형식
		// async : false - 비동기식 옵션을 끔 (동기식)
		// responseText - 서버의 응답 텍스트
		var jsonData = $.ajax({
			url : "${path}/stats/memberGradeChart.do",
			dataType : "json",
			async : false
		}).responseText;
		console.log(jsonData);
		//json 데이터를 데이터테이블로 변환
		var data = new google.visualization.DataTable(jsonData);
		//차트 그리기
		var chart = new google.visualization.PieChart(document
				.getElementById('chart_div3'));
		//draw(데이터, 옵션)
		chart.draw(data, {
			title : "학년별 가입자 현황",
			width : 1500,
			height : 450
		});
	}

	//월별 게시글 차트 그리기 함수
	function drawChart4() {
		//json 데이터 받아오기
		// dataType : "json" - 결과 값이 json형식
		// async : false - 비동기식 옵션을 끔 (동기식)
		// responseText - 서버의 응답 텍스트
		var jsonData = $.ajax({
			url : "${path}/stats/questionChart.do",
			dataType : "json",
			async : false
		}).responseText;
		console.log(jsonData);
		//json 데이터를 데이터테이블로 변환
		var data = new google.visualization.DataTable(jsonData);
		//차트 그리기
		var chart = new google.visualization.LineChart(document
				.getElementById('chart_div4'));
		//draw(데이터, 옵션)
		chart.draw(data, {
			title : "월별 게시글 현황",
			width : 1250,
			height : 350
		});
	}

	function drawChart5() {
		//json 데이터 받아오기
		// dataType : "json" - 결과 값이 json형식
		// async : false - 비동기식 옵션을 끔 (동기식)
		// responseText - 서버의 응답 텍스트
		var jsonData = $.ajax({
			url : "${path}/stats/questionSubjectChart.do",
			dataType : "json",
			async : false
		}).responseText;
		console.log(jsonData);
		//json 데이터를 데이터테이블로 변환
		var data = new google.visualization.DataTable(jsonData);
		//차트 그리기
		var chart = new google.visualization.ColumnChart(document
				.getElementById('chart_div5'));
		//draw(데이터, 옵션)
		chart.draw(data, {
			title : "과목별 게시글 현황",
			width : 1250,
			height : 350,
			bar : {
				groupWidth : '50%'
			}
		});

	}

	//월별 포인트 충전 차트 그리기 함수
	function drawChart6() {
		//json 데이터 받아오기
		// dataType : "json" - 결과 값이 json형식
		// async : false - 비동기식 옵션을 끔 (동기식)
		// responseText - 서버의 응답 텍스트
		var jsonData = $.ajax({
			url : "${path}/stats/pointChart.do",
			dataType : "json",
			async : false
		}).responseText;
		console.log(jsonData);
		//json 데이터를 데이터테이블로 변환
		var data = new google.visualization.DataTable(jsonData);
		//차트 그리기
		var chart = new google.visualization.LineChart(document
				.getElementById('chart_div6'));
		//draw(데이터, 옵션)
		chart.draw(data, {
			title : "월별 포인트 충전 현황",
			width : 1250,
			height : 350
		});
	}
</script>
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
</head>
<body>
	<div id="container">
		<header id="header" class="header">
			<img src="/resources/include/img/logo.png" id="logo">
			<label id="home"><a>홈으로</a></label>
		</header>

		<nav id="nav" class="nav">
			<ul>
				<li><a href="/admin/notice/noticeList.do">notice</a></li>
				<li><a href="/admin/question/questionList.do">question</a></li>
				<li><a href="/admin/manage/manageList.do">manage</a></li>
				<li><a href="/admin/stats/memberStats.do">stats</a></li>
			</ul>
		</nav>


		<div id="content" class="content">
			<div id="split">	
					<input id="member" type="radio" name="tab" checked="checked">
					<input id="question" type="radio" name="tab">
					<input id="point" type="radio" name="tab">
					<label for="member">Member</label>
					<label for="question">Question</label>
					<label for="point">Point</label>
				
				<div id="stats1" style="margin-left: -50px">
					<div id="chart_div1" style="margin-top: 50px"></div>
					<div id="chart_div2" style="margin-top: 50px"></div>
					<div id="chart_div3" style="margin-top: 50px"></div>
				</div>
				<div id="stats2">
					<div id="chart_div4" style="margin-top: 50px"></div>
					<div id="chart_div5" style="margin-top: 50px"></div>
				</div>
				<div id="stats3">
					<div id="chart_div6" style="margin-top: 50px"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
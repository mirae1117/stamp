<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/resources/include/css/manage.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script> 
<script type="text/javascript" src="/resources/include/js/common.js"></script> 
<script type="text/javascript">
	$(document).ready(function() {
		switch ($('#hiddenGrade').val()) {
		case '1':
			grade = '초등학교 1학년';
			break;
		case '2':
			grade = '초등학교 2학년';
			break;
		case '3':
			grade = '초등학교 3학년';
			break;
		case '4':
			grade = '초등학교 4학년';
			break;
		case '5':
			grade = '초등학교 5학년';
			break;
		case '6':
			grade = '초등학교 6학년';
			break;
		case '7':
			grade = '중학교 1학년';
			break;
		case '8':
			grade = '중학교 2학년';
			break;
		case '9':
			grade = '중학교 3학년';
			break;
		case '10':
			grade = '고등학교 1학년';
			break;
		case '11':
			grade = '고등학교 2학년';
			break;
		case '12':
			grade = '고등학교 3학년';
			break;
		case '13':
			grade = '대학생';
			break;
		case '14':
			grade = '일반';
			break;

		default:
			grade = '기타';
			break;
		}
		
		switch ($('#hiddenPrivate').val()) {
		case '0' : 
			pri = '공개';
			break;
		default:
			pri = '비공개';
			break;
		}
		
		switch ($('#hiddenState').val()) {
		case '10' :
			state = '일반회원';
			break;
		case '20' : 
			state = '관리자';
			break;
		case '80' :
			state = '정지회원';
			break;
		case '90' :
			state = '탈퇴회원';
			break;
		}
		
		$("#grade").text(grade);
		$("#private").text(pri);
		$("#state").text(state);
		
		$("#manageList").click(function() {
			location.href = "/admin/manage/manageList.do";  
		});
		
		$("#logo").click(function() {
			location.href = "/admin/adminPage.do";
		});
		
		$("#home").click(function() {
			location.href = "/main/main.do";
		});
		
		
	});
</script>
</head>
<body>
	<div id="container">
		<header id="header" class="header"> 
			<img src="/resources/include/img/logo.png" id="logo">
			<label id="home"><a>홈으로</a></label>
		</header>

		<nav id="nav" class="nav">
			<ul>
				<li><a href="/admin/notice/noticeList.do">notice</a></li>
				<li><a href="/admin/question/questionList.do">question</a></li>
				<li><a href="/admin/manage/manageList.do">manage</a></li>
				<li><a href="/admin/stats/memberStats.do">stats</a></li>
			</ul>
		</nav>

		<div id="content" class="content">
			<form name="data" id="data" method="POST">   
				<input type="hidden" name="m_id" id="m_id" value="${detail.m_id}"/>
				<input type="hidden" name="hiddenGrade" id="hiddenGrade" value="${detail.m_grade}">
				<input type="hidden" name="hiddenPrivate" id="hiddenPrivate" value="${detail.m_private}">
				<input type="hidden" name="hiddenState" id="hiddenState" value="${detail.m_state1}">  
			</form>
			<div id="manageDetail">
				<table cellspacing="0" cellpadding="0">
					<tbody>
						<tr>
							<td class="ac">아이디</td>
							<td>${detail.m_id}</td>
							<td class="ac">이름</td>
							<td>${detail.m_name}</td>
							<td class="ac">생년월일</td>
							<td>${detail.m_birth}</td>
						</tr>
						<tr>
							<td class="ac">이메일</td>
							<td>${detail.m_email}</td>
							<td class="ac">가입날짜</td>
							<td colspan="3">${detail.m_joindate}</td>
						</tr>
						<tr>
							<td class="ac">학년</td>
							<td><label id="grade"></td>
							<td class="ac">지역</td>
							<td>${detail.m_region}</td>
							<td class="ac">성별</td>
							<td>${detail.m_sex}</td>
						</tr>
						<tr>
							<td class="ac">구분</td>
							<td><label id="state"></td>
							<td class="ac">개인정보공개</td>
							<td colspan="3"><label id="private"></td>	
						</tr>
						<tr>
							<td class="ctr">자기소개</td>
							<td height="200" colspan="5">${detail.m_pr}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div id="manageBtn">
				<input type="button" value="목록" class="btn" id="manageList">
			</div>
		</div>
	</div>
</body>
</html>
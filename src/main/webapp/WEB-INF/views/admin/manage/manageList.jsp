<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="tag" uri="/WEB-INF/tld/managecustom_tag.tld"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>회원관리 페이지</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/resources/include/css/manage.css" /> 
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
		//검색 후 검색 대상과 검색 단어 출력
		if("<c:out value='${data.keyword}' />"!="") {
			$("#keyword").val("<c:out value='${data.keyword}' />");
			$("#search").val("<c:out value='${data.search}' />");
		}
		//검색 대상이 변경될 때마다 처리 이벤트
		$("#search").change(function() {
			if($("#search").val()=="all") {
				$("#keyword").val("글 전체 목록");
			} else if($("#search").val()!="all") {
				$("#keyword").val("");
				$("#keyword").focus();
			}
		});
		
		//검색 버튼 클릭시 처리 이벤트
		$("#searchData").click(function() {
			if($("#search").val()=="all") {
				$("#keyword").val("");
			} else {
				if (!chkSubmit($('#keyword'),"검색어를")) return; 
			}
			goPage(1);
		});
		
		$("#logo").click(function() {
			location.href = "/admin/adminPage.do";
		});
		
		$("#home").click(function() {
			location.href = "/main/main.do";
		});
	});
	
	//정렬 버튼 클릭시 처리 함수
	function setOrder(order_by) {
		$("#order_by").val(order_by);
		if($("#order_sc").val()=='DESC') {
			$("#order_sc").val('ASC');
		} else {
			$("#order_sc").val('DESC');
		}
		goPage(1);
	}
	
	function goPage(page) {
		$("#page").val(page);
		$("#m_search").attr("method","get");
		$("#m_search").attr("action","/admin/manage/manageList.do");
		$("#m_search").submit();
	}
	
</script>
</head>
<body>
	<div id="container">
		<header id="header" class="header"> 
			<img src="/resources/include/img/logo.png" id="logo">
			<label id="home"><a>홈으로</a></label>
		</header>

		<nav id="nav" class="nav">
			<ul>
				<li><a href="/admin/notice/noticeList.do">notice</a></li>
				<li><a href="/admin/question/questionList.do">question</a></li>
				<li><a href="/admin/manage/manageList.do">manage</a></li>
				<li><a href="/admin/stats/memberStats.do">stats</a></li>
			</ul>
		</nav>

		<div id="content" class="content">
			<div id="manageList">
			<form id="m_search" name="m_search">
				<input type="hidden" id="page" name="page" value="${data.page}"/>
				<input type="hidden" id="order_by" name="order_by" value="${data.order_by}"/>        
				<input type="hidden" id="order_sc" name="order_sc" value="${data.order_sc}"/> 
			
				<table id="manageSearch">
					<tr>
						<td>
							<select id="search" name="search">
								<option value="all">전체</option>
								<option value="m_id">아이디</option>
								<option value="m_email">이메일</option>
							</select>
						
							<input type="text" name="keyword" id="keyword" placeholder="검색어를 입력하세요"/>
							<input type="button" value="검색" id="searchData"/>
						</td>
					</tr>
				</table>
				</form>
				<table cellspacing="0" cellpadding="0" border="1">
					<thead>
						<tr>
							<th>아이디</th>
							<th>생년월일</th>
							<th>이메일</th>
							<th>    
							 	<a href="javascript:setOrder('m_grade');">학년 
							 		<c:choose>
							 	    	<c:when test="${data.order_by=='m_grade'  and  data.order_sc=='ASC'}">▲</c:when> 
							 	    	<c:when test="${data.order_by=='m_grade'  and  data.order_sc=='DESC'}">▼</c:when>
							 	    	<c:otherwise>▲</c:otherwise>      
							 	    </c:choose>     
							 	</a>        
							</th> 
							<th>    
							 	<a href="javascript:setOrder('m_region');">지역 
							 		<c:choose>
							 	    	<c:when test="${data.order_by=='m_region'  and  data.order_sc=='ASC'}">▲</c:when> 
							 	    	<c:when test="${data.order_by=='m_region'  and  data.order_sc=='DESC'}">▼</c:when>
							 	    	<c:otherwise>▲</c:otherwise>      
							 	    </c:choose>     
							 	</a>        
							</th> 
							<th>    
							 	<a href="javascript:setOrder('m_sex');">성별 
							 		<c:choose>
							 	    	<c:when test="${data.order_by=='m_sex'  and  data.order_sc=='ASC'}">▲</c:when> 
							 	    	<c:when test="${data.order_by=='m_sex'  and  data.order_sc=='DESC'}">▼</c:when>
							 	    	<c:otherwise>▲</c:otherwise>      
							 	    </c:choose>     
							 	</a>        
							</th> 
							<th>    
							 	<a href="javascript:setOrder('m_state1');">회원구분 
							 		<c:choose>
							 	    	<c:when test="${data.order_by=='m_state1'  and  data.order_sc=='ASC'}">▲</c:when> 
							 	    	<c:when test="${data.order_by=='m_state1'  and  data.order_sc=='DESC'}">▼</c:when>
							 	    	<c:otherwise>▲</c:otherwise>      
							 	    </c:choose>     
							 	</a>        
							</th> 
							<th>    
							 	<a href="javascript:setOrder('m_joindate');">가입날짜
							 		<c:choose>
							 	    	<c:when test="${data.order_by=='m_joindate'  and  data.order_sc=='ASC'}">▲</c:when> 
							 	    	<c:when test="${data.order_by=='m_joindate'  and  data.order_sc=='DESC'}">▼</c:when>
							 	    	<c:otherwise>▲</c:otherwise>      
							 	    </c:choose>     
							 	</a>        
							</th> 
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${not empty manageList}">
								<c:forEach var="manage" items="${manageList}" varStatus="status">
									<tr align="center">
										<td>
											<a href="/admin/manage/manageDetail.do?m_id=${manage.m_id}&page=${data.page}&pageSize${data.pageSize}">${manage.m_id}</a>
										</td>
										<td>${manage.m_birth}</td>
										<td>${manage.m_email}</td>
										<td>${manage.m_grade}</td>
										<td>${manage.m_region}</td>
										<td>${manage.m_sex}</td>
										<td>${manage.m_state1}</td>
										<td>${manage.m_joindate}</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="6" align="center">등록된 회원이 존재하지 않습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
				<div id="managePage">
					<tag:paging page="${param.page}" total="${total}" list_size="${data.pageSize}" />
				</div>
		</div>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="tag" uri="/WEB-INF/tld/noticecustom_tag.tld"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>공지사항 페이지</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="/resources/include/css/notice.css" />
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/*  $("#pageSize").change(function() {
			goPage(1);
		});  */

		$("#writeForm").click(function() {
			location.href = "/admin/notice/writeForm.do";
		});
		
		$("#logo").click(function() {
			location.href = "/admin/adminPage.do";
		});

		$("#home").click(function() {
			location.href = "/main/main.do";
		});
	});

	function goPage(page) {
		$("#page").val(page);
		$("#paging").attr("method", "POST");
		$("#paging").attr("action", "/admin/notice/noticeList.do");
		$("#paging").submit();
	}

	function goDetail(nt_no, page, pageSize) {
		location.href = "/admin/notice/noticeDetail.do?nt_no=" + nt_no
				+ "&page=" + page + "&pageSize=" + pageSize;
	}
</script>
</head>
<body>
	<div id="container">
		<header id="header" class="header">
			<img src="/resources/include/img/logo.png" id="logo">
			<label id="home"><a>홈으로</a></label>
		</header>

		<nav id="nav" class="nav">
			<ul>
				<li><a href="/admin/notice/noticeList.do">notice</a></li>
				<li><a href="/admin/question/questionList.do">question</a></li>
				<li><a href="/admin/manage/manageList.do">manage</a></li>
				<li><a href="/admin/stats/memberStats.do">stats</a></li>
			</ul>
		</nav>

		<div id="content" class="content list">
			<form id="paging" name="paging">
				<input type="hidden" id="page" name="page" value="${data.page}" />
			</form>

			<table class="table notice table-hover" cellspacing="0" cellpadding="0">
				<colgroup>
					<col width="10%" />
					<col width="40%" />
					<col width="20%" />
					<col width="20%" />
					<col width="10%" />
				</colgroup>
				<thead>
					<tr>
						<th></th>
						<th>제목</th>
						<th>작성자</th>
						<th>작성일</th>
						<th>조회</th>
					</tr>
				</thead>
				<tbody>
					<c:choose>
						<c:when test="${not empty noticeList }">
							<c:forEach var="notice" items="${noticeList}" varStatus="status">
								<tr align="center">
									<td>${count - (status.count-1)}</td>
									<td align="left"><a
										href="javascript:goDetail(${notice.nt_no},${data.page},${data.pageSize})">${notice.nt_title}</a>
									</td>
									<td>${notice.m_id }</td>
									<td>${notice.nt_time }</td>
									<td>${notice.nt_count }</td>
								</tr>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<tr>
								<td colspan="5" align="center">등록된 게시물이 존재하지 않습니다.</td>
							</tr>
						</c:otherwise>
					</c:choose>
				</tbody>
			</table>
			<div id="noticeBtn1">
				<input type="button" value="글쓰기" class="btn" id="writeForm">
			</div>

			<div id="noticePage">
				<tag:paging page="${param.page}" total="${total}"
					list_size="${data.pageSize}" />
			</div>
		</div>
	</div>
</body>
</html>
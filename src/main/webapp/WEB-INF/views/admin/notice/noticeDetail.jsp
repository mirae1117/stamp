<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/resources/include/css/notice.css" /> 
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	var goUrl = "";
	$(document).ready(function() {
		$("#updateForm").click(function() {
			goUrl = "/admin/notice/updateForm.do";
			$("#data").attr("method", "POST");
			$("#data").attr("action",goUrl);
			$("#data").submit();
		});
		
		$("#noticeDelete").click(function() {
			goUrl = "/admin/notice/noticeDelete.do";
			$("#data").attr("method", "POST");
			$("#data").attr("action",goUrl);
			$("#data").submit();
		});
		
		$("#noticeList").click(function() {
			location.href = "/admin/notice/noticeList.do";  
		});
		
		$("#logo").click(function() {
			location.href = "/admin/adminPage.do";
		});
		
		$("#home").click(function() {
			location.href = "/main/main.do";
		});
	});

</script> 
</head>
<body>
	<div id="container">
		<header id="header" class="header"> 
			<img src="/resources/include/img/logo.png" id="logo">
			<label id="home"><a>홈으로</a></label>
		</header>

		<nav id="nav" class="nav">
			<ul>
				<li><a href="/admin/notice/noticeList.do">notice</a></li>
				<li><a href="/admin/question/questionList.do">question</a></li>
				<li><a href="/admin/manage/manageList.do">manage</a></li>
				<li><a href="/admin/stats/memberStats.do">stats</a></li>
			</ul>
		</nav>

		<div id="content" class="content">
			<form name="data" id="data" method="POST">   
				<input type="hidden" name="nt_no" id="nt_no" value="${detail.nt_no}"/>  
			</form> 
			<div id="noticeDetail">
				<table cellspacing="0" cellpadding="0">
					<colgroup>
						<col width="25%"/>
						<col width="25%"/>
						<col width="25%"/>
						<col width="25%"/>
					</colgroup>
					<tbody>
						<tr>
							<td class="ac">작성자</td>
							<td>${detail.m_id}</td>
							<td class="ac">작성일</td>
							<td>${detail.nt_time}</td>
						</tr>
						<tr>
							<td class="ac">제목</td>
							<td>${detail.nt_title}</td>
							<td class="ac">조회수</td>
							<td>${detail.nt_count}</td>
						</tr>
						<tr class="ctr">
							<td class="ac">내용</td>
							<td colspan="3">${detail.nt_content}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div id="noticeBtn2">
				<input type="button" value="수정" class="btn" id="updateForm">
				<input type="button" value="삭제" class="btn" id="noticeDelete">
				<input type="button" value="목록" class="btn" id="noticeList">
			</div>
		</div>
	</div>
</body>
</html>
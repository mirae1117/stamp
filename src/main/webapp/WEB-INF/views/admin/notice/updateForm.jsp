<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/resources/include/css/notice.css" /> 
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#noticeUpdate").click(function() {
		if (!chkSubmit($('#nt_title'), "제목을"))     return; 
			else if (!chkSubmit($('#nt_content'), "작성할 내용을"))     return; 
			else {     
				$("#writeForm").attr("method", "POST");     
				$("#writeForm").attr("action", "/admin/notice/noticeUpdate.do");     
				$("#writeForm").submit();    
			} 
		});
		$("#noticeList").click(function() {   
			location.href = "/admin/notice/noticeList.do";  
		});
		
		$("#logo").click(function() {
			location.href = "/admin/adminPage.do";
		});
		
		$("#home").click(function() {
			location.href = "/main/main.do";
		});
	});
</script>
</head>
<body>
	<div id="container">
		<header id="header" class="header"> 
			<img src="/resources/include/img/logo.png" id="logo">
			<label id="home"><a>홈으로</a></label>
		</header>

		<nav id="nav" class="nav">
			<ul>
				<li><a href="/admin/notice/noticeList.do">notice</a></li>
				<li><a href="/admin/question/questionList.do">question</a></li>
				<li><a href="/admin/manage/manageList.do">manage</a></li>
				<li><a href="/admin/stats/memberStats.do">stats</a></li>
			</ul>
		</nav>

		<div id="content" class="content">
			<form name="writeForm" id="writeForm">   
				<input type="hidden" name="nt_no" id="nt_no" value="${updateData.nt_no}"/>  
				<table cellspacing="0" cellpadding="0" id="noticeWrite">
						<tr>
							<td>작성자</td>
							<td>${updateData.m_id}</td>
						</tr>
						<tr>
							<td>제목</td>
							<td><input type="text" name="nt_title" id="nt_title"
									value="${updateData.nt_title}"/></td>
						</tr>
						<tr>
							<td>내용</td>
							<td height="200">
								<textarea name="nt_content" id="nt_content"
								rows="10" cols="70">${updateData.nt_content}</textarea>
							</td>
						</tr>
				</table>
			</form>
			<div id="noticeBtn">
				<input type="button" value="수정" class="btn" id="noticeUpdate">
				<input type="button" value="목록" class="btn" id="noticeList">
			</div>
		</div>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>관리자 페이지</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="/resources/include/css/notice.css" />
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
	$(document).ready(function() {
		$('a[data-toggle="tab"]').on('hidden.bs.tab', function() {
			alert("이벤트 실행됨");
		});
		
		$("#logo").click(function() {
			location.href = "/admin/adminPage.do";
		});
		
		$("#home").click(function() {
			location.href = "/main/main.do";
		});
	});
	//차트 라이브러리 로딩
	google.load('visualization', '1', {
		'packages' : [ 'corechart' ]
	});

	//콜백 함수 작성
	google.setOnLoadCallback(drawChart1);

	//월별 가입자 차트 그리기 함수
	function drawChart1() {
		//json 데이터 받아오기
		// dataType : "json" - 결과 값이 json형식
		// async : false - 비동기식 옵션을 끔 (동기식)
		// responseText - 서버의 응답 텍스트
		var jsonData = $.ajax({
			url : "${path}/stats/memberJoinChart.do",
			dataType : "json",
			async : false
		}).responseText;
		//json 데이터를 데이터테이블로 변환
		var data = new google.visualization.DataTable(jsonData);
		//차트 그리기
		var chart = new google.visualization.LineChart(document
				.getElementById('chart_div1'));
		//draw(데이터, 옵션)
		chart.draw(data, {
			title : "월별 가입자 현황",
			width : 1250,
			height : 350
		});
	}

</script>
</head>
<body>
	<div id="container">
		<header id="header">
			<img src="/resources/include/img/logo.png" id="logo">
			<label id="home"><a>홈으로</a></label>
		</header>

		<nav id="nav">
			<ul>
				<li><a href="/admin/notice/noticeList.do">notice</a></li>
				<li><a href="/admin/question/questionList.do">question</a></li>
				<li><a href="/admin/manage/manageList.do">manage</a></li>
				<li><a href="/admin/stats/memberStats.do">stats</a></li>
			</ul>
		</nav>

		<div id="aside"></div>

		<div id="content">
			<input type="hidden" id="hiddenMemberCount" name="hiddenMemberCount" value="${memberCount}">
			<input type="hidden" id="hiddenQuestionCount" name="hiddenQuestionCount" value="${questionCount}">
			<input type="hidden" id="hiddenvisitantCount" name="hiddenvisitantCount" value="${visitantCount}">
			
			<div class="tabpane">
				<ul id="myTab" class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a
						data-target="#notice" id="notice-tab" role="tab" data-toggle="tab"
						aria-controls="notice" aria-expanded="true">Notice</a></li>
					<li role="presentation" class=""><a data-target="#question"
						role="tab" id="question-tab" data-toggle="tab"
						aria-controls="question" aria-expanded="false">Question</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div role="tabpanel" class="tab-pane fade active in" id="notice"
						aria-labelledby="notice-tab">
						<table id="mainNotice" cellspacing="0" cellpadding="0">
							<colgroup>
								<col width="80%" />
								<col width="20%" />
							</colgroup>

							<c:choose>
								<c:when test="${not empty mainNoticeList }">
									<c:forEach var="mainNoticeList" items="${mainNoticeList}">
										<tr align="center">
											<td align="left">
											<a href="/admin/notice/noticeDetail.do?nt_no=${mainNoticeList.nt_no}">${mainNoticeList.nt_title}</a>
											</td>
											<td>${mainNoticeList.nt_time }</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="5" align="center">등록된 게시물이 존재하지 않습니다.</td>
									</tr>
								</c:otherwise>
							</c:choose>
						</table>
					</div>
					<div role="tabpanel" class="tab-pane fade" id="question"
						aria-labelledby="question-tab">
						<table id="mainNotice" cellspacing="0" cellpadding="0">
							<colgroup>
								<col width="80%" />
								<col width="20%" />
							</colgroup>

							<c:choose>
								<c:when test="${not empty mainQuestionList }">
									<c:forEach var="mainQuestionList" items="${mainQuestionList}">
										<tr align="center">
											<td align="left">
											<a href="/admin/question/questionDetail.do?q_no=${mainQuestionList.q_no}")">${mainQuestionList.q_title}</a>
											</td>
											<td>${mainQuestionList.q_time }</td>
										</tr>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
										<td colspan="5" align="center">등록된 게시물이 존재하지 않습니다.</td>
									</tr>
								</c:otherwise>
							</c:choose>
						</table>
					</div>
				</div>
			</div>

			<div id="visitant">
				<table>
					<colgroup>
						<col width="80%" />
						<col width="20%" />
					</colgroup>
					<tr>
						<td align="left">총 회원수 : </td>
						<td align="center">
						<span style="color: red">${memberCount}</span>
						</td>	
					</tr>
					<tr>
						<td align="left">총 게시글 : </td>
						<td align="center">${questionCount}</td>
					</tr>
					<tr>
						<td align="left">오늘 방문자 : </td>
						<td align="center">${visitantCount}</td>
					</tr>
				</table>
			</div>

			<div id="chart_div1"></div>

		</div>
	</div>
</body>
</html>
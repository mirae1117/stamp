<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="tag" uri="/WEB-INF/tld/questioncustom_tag.tld"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>질문 게시판 페이지</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="/resources/include/css/question.css" />
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
		$("#logo").click(function() {
			location.href = "/admin/adminPage.do";
		});
		
		$("#home").click(function() {
			location.href = "/main/main.do";
		});
		
		//검색 후 검색 대상과 검색 단어 출력
		if ("<c:out value='${data.keyword}' />" != "") {
			$("#keyword").val("<c:out value='${data.keyword}' />");
			$("#search").val("<c:out value='${data.search}' />");
		}

		/* 검색 대상이 변경될 때마다 처리 이벤트 */
		$("#search").change(function() {
			if ($("#search").val() == "all") {
				$("#keyword").val("글 목록 전체");
			} else if ($("#search").val() != "all") {
				$("#keyword").val("");
				$("#keyword").focus();
			}
		});

		/* 검색 버튼 클릭 시 처리 이벤트 */
		$("#searchData").click(function() {
			if ($("#search").val() == "all") {
				$("#keyword").val("");
			} else {
				if (!chkSubmit($('#keyword'), "검색어를"))
					return;
			}
			goPage(1);
		});

		function goPage(page) {
			$("#page").val(page);
			$("#q_search").attr("method", "get");
			$("#q_search").attr("action", "/admin/question/questionList.do");
			$("#q_search").submit();
		}

	});
</script>
</head>
<body>
	<div id="container">
		<header id="header" class="header"> 
			<img src="/resources/include/img/logo.png" id="logo">
			<label id="home"><a>홈으로</a></label>
		</header>

		<nav id="nav" class="nav">
			<ul>
				<li><a href="/admin/notice/noticeList.do">notice</a></li>
				<li><a href="/admin/question/questionList.do">question</a></li>
				<li><a href="/admin/manage/manageList.do">manage</a></li>
				<li><a href="/admin/stats/memberStats.do">stats</a></li>
			</ul>
		</nav>

		<div id="content" class="content">
			<div id="questionList">
				<form id="q_search" name="q_search">
					<input type="hidden" id="page" name="page" value="${data.page}" />
				
					<table id="questionSearch">
						<tr>
							<td><select id="search" name="search">
									<option value="all">전체</option>
									<option value="q_title">제목</option>
									<option value="m_id">아이디</option>
									<option value="q_subject">과목</option>
									<option value="q_grade">학년</option>
							</select> <input type="text" name="keyword" id="keyword"
								placeholder="검색어를 입력하세요" /> <input type="button" value="검색"
								id="searchData" /></td>
						</tr>
					</table>
				</form>

				<table cellspacing="0" cellpadding="0" border="1">
					<colgroup>
						<col width="40%" />
						<col width="20%" />
						<col width="10%" />
						<col width="10%" />
						<col width="20%" />
					</colgroup>
					<thead>
						<tr>
							<th>제목</th>
							<th>아이디</th>
							<th>과목</th>
							<th>학년</th>
							<th>등록일</th>
						</tr>
					</thead>
					<tbody>
						<c:choose>
							<c:when test="${not empty questionList}">
								<c:forEach var="questionList" items="${questionList}"
									varStatus="status">
									<tr align="center"
										<c:if test="${questionList.q_state == 7}">
											class ="colorRed"
										</c:if>
										<c:if test="${questionList.q_state == 8}">
											class ="colorOrange"
										</c:if>
										<c:if test="${questionList.q_state == 9}">
											class ="colorDeepPink"
										</c:if>
									>
										<td><a
											href="/admin/question/questionDetail.do?q_no=${questionList.q_no}&page=${data.page}&pageSize${data.pageSize}">${questionList.q_title}</a>
										</td>
										<td>${questionList.m_name}(${questionList.m_id})</td>
										<td>${questionList.q_subject}</td>
										<td>${questionList.q_grade}</td>
										<td>${questionList.q_time}</td>
									</tr>
								</c:forEach>
							</c:when>
							<c:otherwise>
								<tr>
									<td colspan="6" align="center">등록된 회원이 존재하지 않습니다.</td>
								</tr>
							</c:otherwise>
						</c:choose>
					</tbody>
				</table>
			</div>
			<div id="questionPage">
				<tag:paging page="${param.page}" total="${total}" list_size="${data.pageSize}" />
			</div>
		</div>
	</div>
</body>
</html>
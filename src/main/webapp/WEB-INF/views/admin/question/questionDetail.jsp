<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="/resources/include/css/question.css" />
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var log_id = $("#log_id").val();
		var m_id = $("#m_id").val();
		switch ($('#hiddenInfoGrade').val()) {
		case '1':
			grade = '초등학교 1학년';
			break;
		case '2':
			grade = '초등학교 2학년';
			break;
		case '3':
			grade = '초등학교 3학년';
			break;
		case '4':
			grade = '초등학교 4학년';
			break;
		case '5':
			grade = '초등학교 5학년';
			break;
		case '6':
			grade = '초등학교 6학년';
			break;
		case '7':
			grade = '중학교 1학년';
			break;
		case '8':
			grade = '중학교 2학년';
			break;
		case '9':
			grade = '중학교 3학년';
			break;
		case '10':
			grade = '고등학교 1학년';
			break;
		case '11':
			grade = '고등학교 2학년';
			break;
		case '12':
			grade = '고등학교 3학년';
			break;
		case '13':
			grade = '대학생';
			break;
		case '14':
			grade = '일반';
			break;

		default:
			grade = '기타';
			break;
		}
		switch ($('#hiddenInfoSubject').val()) {
		case '1':
			subject = '수학';
			break;
		case '2':
			subject = '과학';
			break;
		case '3':
			subject = '영어';
			break;
		case '4':
			subject = '국어';
			break;
		case '5':
			subject = '한문';
			break;
		case '6':
			subject = '역사';
			break;
		case '7':
			subject = '경제';
			break;
		case '8':
			subject = '사회';
			break;
		case '9':
			subject = '국가자격시험';
			break;

		default:
			grade = '기타';
			break;
		}
		$('#infoGrade').text(grade);
		$('#infoSubject').text(subject);

		$("#questionList").click(function() {
			location.href = "/admin/question/questionList.do";

		});
		
		$("#logo").click(function() {
			location.href = "/admin/adminPage.do";
		});
		
		$("#home").click(function() {
			location.href = "/main/main.do";
		});
	});
</script>
</head>
<body>
	<div id="container">
		<header id="header" class="header"> 
			<img src="/resources/include/img/logo.png" id="logo">
			<label id="home"><a>홈으로</a></label>
		</header>

		<nav id="nav" class="nav">
			<ul>
				<li><a href="/admin/notice/noticeList.do">notice</a></li>
				<li><a href="/admin/question/questionList.do">question</a></li>
				<li><a href="/admin/manage/manageList.do">manage</a></li>
				<li><a href="/admin/stats/memberStats.do">stats</a></li>
			</ul>
		</nav>

		<div id="content" class="content">
			<form name="data" id="data" method="POST">
				<input type="hidden" name="m_id" id="m_id" value="${detail.q_no}" />
				<input type="hidden" id="hiddenInfoGrade" name="hiddenInfoGrade" value="${detail.q_grade }"> 
				<input type="hidden" id="hiddenInfoSubject" name="hiddenInfoSubject" value="${detail.q_subject }">
				
				<div id="questionDetail">
					<table cellspacing="0" cellpadding="0">
						<tr>
							<td align="center" class="ac">작성자</td>
							<td>${detail.m_id }</td>
							<td align="center" class="ac">조회수</td>
							<td>${detail.q_count }</td>
						</tr>
						<tr>
							<td align="center" class="ac">과목</td>
							<td><label id="infoSubject"></label></td>
							<td align="center" class="ac">학년</td>
							<td><label id="infoGrade"></label></td>
						</tr>
						<tr>
							<td align="center" class="ac">제목</td>
							<td colspan="3">${detail.q_title}</td>
						</tr>
						<tr>
							<td align="center" class="ac">내용</td>
							<td colspan="3" height="200">${detail.q_content }</td>
						</tr>
						<tr>
							<td align="center" class="ac" height="150">첨부파일</td>
							<td colspan="3" align="center"><img id="img"
								src="/uploadStorage/${detail.q_file}" /></td>
						</tr>

					</table>
				</div>
			</form>
			<div id="questionBtn">
				<input type="button" value="목록" class="btn" id="questionList">
			</div>
		</div>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<title>아이디/비밀번호 찾기</title>
<script type="text/javascript">
var findFlag = 1;
$(function() {
	var result = '${msg}';
	if (result != null && result != '') {
		alert(result);
	}
	$(document).on("click", "#find", function() {
		switch (findFlag) {
		case 1:
			break;
		case 2:
			break;
		default:
			alert("시스템에러");
			return;
			break;
		}
		if (findFlag == 2 && !chkSubmit($('#m_id'), "아이디를"))
			return;	
		else if (!chkSubmit($('#email1'), "이메일 앞자리를"))
			return;
		else if (!chkSubmit($('#email2'), "이메일 뒷자리를"))
			return;
		else if ($("#emailList").val() == '-1'){
			alert("이메일 뒷자리를 확인해 주세요.");
			return;
		}
		else {
			if ($("#emailList").val() == '0') {
				$("#m_email").val($("#email1").val()+"@"+ $("#email2").val());
			} else {
				$("#m_email").val($("#email1").val()+"@"+ $("#emailList").val());
			}
			 $("#findForm").attr({
				"method" : "POST",
				"action" : "/join/findIdPassword.do"
			});
			$("#findForm").submit();
		}
		
		
		
		
	});
	$(document).on("click", "#close", function() {
		close();
	});
	$("#emailList").change(function() {
		if ($("#emailList option:selected").val() == '0') {
			$("#email2").val('');
			$("#email2").attr("disabled", false);
		} else  {
			if ($("#emailList option:selected").val() == '-1') {
				$("#email2").val('');
			} else {
				$("#email2").val($("#emailList").val());
			}
			$("#email2").attr("disabled", true);
		}
	});
});
function findIdForm() {
	findFlag = 1;
	$("#h3title").html("아이디 찾기");
	$("#id_tr").hide();
	$("#idTitle").show();
	$("#pwTitle").hide();
}
function findPwForm() {
	findFlag = 2;
	$("#h3title").html("비밀번호 찾기");
	$("#id_tr").show();
	$("#idTitle").hide();
	$("#pwTitle").show();
}
</script>
<style type="text/css">
#id_tr{
display: none;
}
.form-control{
display: inline-block;
width: 150px;}
#idTitle{
font-size: 15px;
}
#pwTitle{
font-size: 15px;
}
#email_tr th {
   text-align:center;
    width: 15%;
    background: #ececec;
    border-top: 1px solid #dddddf;
}
#email_tr td {
    height: 30px;
    padding: 10px 0 10px 20px;
    border-top: 1px solid #dddddf;
    border-bottom: 1px solid #dddddf;
    line-height: 20px;
    vertical-align: middle;
    word-break: break-all;
    line-height: 20px;
}

#id_tr th{
   text-align:center;
    width: 15%;
    background: #ececec;
    border-top: 1px solid #dddddf;
}

#id_tr td{
   height: 30px;
    padding: 10px 0 10px 20px;
    border-top: 1px solid #dddddf;
    border-bottom: 1px solid #dddddf;
    line-height: 20px;
    vertical-align: middle;
    word-break: break-all;
    line-height: 20px;
}

#find_btn{
padding-top: 20px;
padding-left: 230px;}
</style>
</head>
<body>
	<a href="javascript:findIdForm()">아이디 찾기</a>
	
	<a href="javascript:findPwForm()">비밀번호 찾기</a>
	<div>
		<h3 id="h3title">아이디 찾기</h3>
		<h5 id="idTitle">회원 가입시 입력하셨던 이메일 주소를 입력후 아이디 찾기 버튼을 선택해주세요.</h5>
		<h5 hidden="" id="pwTitle">회원 가입시 등록한 아이디와 이메일을 입력 후 비밀번호 찾기 버튼을 선택해 주세요.</h5>
		
		<form id="findForm" name="findForm">
		<table style="width: 100%">
		<colgroup>
		<col width="15%">
		<col width="85%">
		</colgroup>
		<tbody>
		<tr id="id_tr"><th>ID</th><td><input type="text" class="form-control" id="m_id" name="m_id"></td></tr>
		<tr id="email_tr"><th class="info">이메일</th>
				<td><input type="text" class="form-control" id="email1" name="email1" /> @
				<input type="text" class="form-control" id="email2" name="email2" disabled="disabled"/>
				<select class="form-control" id="emailList" name="emailList">
									<option value='-1'>-이메일 선택-</option>
									<option value='0'>직접입력</option>
									<option value='gmail.com'>gmail.com</option>
									<option value='naver.com'>naver.com</option>
									<option value='nate.com'>nate.com</option>
									<option value='hanmail.net'>hanmail.net</option>
									<option value='daum.com'>daum.com</option>
							</select>
							<input type="hidden" id="m_email" name="m_email" /></td></tr>
							</tbody>
		</table>
		<div id="find_btn">
		<button type="button" id="find" class="btn btn-success" name="find">찾기</button>
		
		<button type="button" id="close" class="btn btn-default" name="close">닫기</button>
		</div>
		</form>
		
	</div>
</body>
</html>
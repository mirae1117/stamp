<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/*인증 메일 전송 버튼 클릭시 처리 이벤트*/
		$("#joingo").click(function() {
			//입력값 체크
			if (!chkSubmit($('#g_key'), "인증키를"))
				return;
			else {
				$("#guardianForm").attr({
					"method" : "POST",
					"action" : "/join/joinPaCertify.do"
				});
				$("#guardianForm").submit();
			}
		}); /*취소 버튼 클릭시 처리 이벤트*/
		$("#cancel").click(function() {
			location.href="/join/joinAge.do";
		});
	});
</script>
<style type="text/css">

h1, h2, h3, h4, h5, h6, p, dl, dt, dd, table, thead, tbody, tr, th, td {
	margin: 0;
	padding: 0;
	font-weight: normal;
}

#joinContent {
	width: 900px;
	position: relative;
	margin: 0 auto;
	height: 800px;
	clear: both;
}

#join_title {
	position: relative;
	margin-bottom: 78px;
	padding-top: 89px;
	width: 100%;
	border-bottom: 1px solid #e5e5e5;
}

#join_title h3 {
	position: absolute;
	top: 74px;
	left: 50%;
	padding: 0 30px;
	color: #323232;
	font-size: 34px;
	background: #fff;
	text-transform: uppercase;
	
}
.key_form{
margin-top: 10px;
    border-top: 1px solid #e6e6e6;
    border-bottom: 2px solid #e6e6e6;
    margin-top:10px;
        width: 100%;
    float: left;
    clear: both;
}
th{
float: left;
 padding: 15px 0 10px 30px;
    border-top: 1px solid #e6e6e6;
    color: #494949;
    font-weight: bold;
    font-size:15px;
    width: 120px;}

.join_key{
color: white;
background-color: blue;
}
th.info {
    padding-left: 30px !important;
    background: #ececec url(/resources/include/img/ico_essential.png) no-repeat 11px 50%;
}

.form-control{
width: 170px;
display: inline-block;
}
td {
    height: 30px;
    padding: 10px 0 10px 20px;
    border-top: 1px solid #ececec;
    line-height: 20px;
    vertical-align: middle;
    word-break: break-all;
}
#agree_btn{
    width: 100%;
    position: relative;
    margin: 50px auto;
    text-align: center;
    clear: both;
    padding-top: 50px;
    
}
.cap2_key{
    margin-top: 10px;
    padding-left: 10px;
    color: #777777;
    font-size: 15px;
    line-height: 16px;
    background: url(/resources/include/img/m_blt_squre.png) 0 center no-repeat;
    background-size: 3px;
}
</style>
<title>법정 대리인 인증</title>
</head>
<body>
<div id="header">
		<jsp:include page="../mainpage/header.jsp"></jsp:include>
	</div>
	<section id="joinContent">
	<div id="join_title">
			<h3 style="margin-left: -104px;">회원 가입</h3>
		</div>
	<h3 style="margin-top: 200px;">법정 대리인 인증 페이지</h3>
	<p class="cap2_key">인증키를 입력하여 회원가입을 진행해 주시기 바랍니다.</p>
	<div class="key_form">
	<form name="guardianForm" id="guardianForm">
	<input type="hidden" id="g_email" name="g_email" value="${guardian.g_email}" />
	<table>
	<colgroup>
		<col width="22%">
		<col width="">
	</colgroup>
	<tr>
	<th>인증키 확인</th>
	
	<td><input type="text" class="form-control" name="g_key" id="g_key" /></td></tr>
	</table>
	</form>
	</div>
	<div id="agree_btn">
	<button type="button" class="btn btn-success" id="joingo" name="joingo">회원가입</button>
	<button type="button" class="btn btn-default" id="cancel" name="cancel">뒤로가기</button>
	</div>
	</section>
	<div id="footer">
		<jsp:include page="../mainpage/footer.jsp"></jsp:include>
	</div>
</body>
</html>
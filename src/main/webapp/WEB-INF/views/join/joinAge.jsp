<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<style type="text/css">

h1, h2, h3, h4, h5, h6, p, dl, dt, dd, table, thead, tbody, tr, th, td {
    margin: 0;
    padding: 0;
    font-weight: normal;
}
#wrap{
width: 100%;
height: 100%;}

#joinContent {
	clear:both;
	width: 900px;
	position: relative;
	margin:0 auto;
	
}

#join_title {
	position: relative;
    margin-bottom: 78px;
    padding-top: 89px;
    width: 100%
}
h3{
    position: absolute;
    top: 74px;
    left: 50%;
    padding: 0 30px;
    color: #323232;
    font-size: 34px;
    background: #fff;
    text-transform: uppercase;
}
#join_cap{
margin-bottom: 70px;
}

#join_list {
    position: relative;
    width: 758px;
    margin: 0 auto;
    padding-top: 60px;
    border-top: 3px solid #213d2a;
    border-left: 1px solid #ccc;
    border-right: 1px solid #ccc;
    border-bottom: 1px solid #ccc;
    margin-bottom: 50px;
}

.cap2{
    margin-bottom: 10px;
    padding-left: 10px;
    color: #777777;
    font-size: 15px;
    line-height: 16px;
    background: url(/resources/include/img/m_blt_squre.png) 0 center no-repeat;
    background-size: 3px;
}
.select{
margin-top: 50px;
margin-left: 70px;
margin-bottom: 10px;
}
#join_txt{
    clear: both;
    margin-top: 30px;
    padding: 30px 20px;
    background-color: #f7f7f7;
    font-size: 12px;
    
}


#join_list img {
	margin-left: 90px;
	
}


</style>
<title>가입 회원 종류 선택</title>
</head>
<body>

	<div id="header">
	<jsp:include page="../mainpage/header.jsp"></jsp:include>
	</div>
	<section id="joinContent">
		<div id="join_title">
		<h3 style="margin-left: -104px;">회원 가입</h3>
		</div>
		<div id="join_cap">
			<p style="font-size: 25pt; margin-bottom: 30px;">
				문제를 같이 푸는 <strong style="color: green;">그루터기</strong>에 오신것을 <strong>환영합니다!</strong>
			</p>
			<p class="cap2">그루터기 회원가입으로
				그루터기에서 제공하는 문제풀이 서비스 및 그룹활동을 이용하실 수 있습니다.</p>
			<p class="cap2">그루지기는 전연령이 회원 가입 가능 합니다.</p>
		</div>
		
		<h4 class="select">회원종류 선택</h4>
		<div id="join_list">
			<a href="/join/joinPaAgree.do"><img
				src="/resources/include/img/join_gate2.gif" alt="가입하기"></a> <a
				href="/join/joinAgree.do"><img
				src="/resources/include/img/join_gate1.gif" alt="가입하기"></a>
			
			
			<div id="join_txt">
			<p>정보통신망 이용 촉진 및 정보 보호 등에 관한 법률의 개인정보보호지침 제 22조에 의하면,<br>
			서비스 제공자나 만 14세 미만 아동의 개인정보를 수집 시 법정 대리인(부모님)의 동의를 얻어야합니다.<br>
			만 14세 미만 회원님은 부모님의 이메일로 동의를 받을 후 가입 가능합니다.</p>
			</div>
		</div>
		</section>
		<div id="footer">
		<jsp:include page="../mainpage/footer.jsp"></jsp:include>
		</div>
		
		
</body>
</html>
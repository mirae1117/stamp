<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>일반 회원 가입 폼</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
var booleanIdChk = false;
	$(document).ready(function() {
		/*가입 버튼 클릭시 처리 이벤트*/
		$("#joinPro").click(function() {
			
			//입력값 체크
			if(booleanIdChk == false){
				alert("아이디 중복 체크를 해야 합니다.");
				return;
			}
			else if (!chkSubmit($('#m_name'), "이름을"))
				return;	
			else if (!chkSubmit($('#m_pw'), "비밀번호를"))
					return;
			else if ($('#m_pw').val() != $('#m_pw2').val()){
				alert("비밀번호가 서로 다릅니다");
				return;
			}
			else if (!chkSubmit($('#email1'), "이메일 앞자리를"))
				return;
			else if (!chkSubmit($('#email2'), "이메일 뒷자리를"))
				return;
			else if ($("#emailList").val() == '-1'){
				alert("이메일 뒷자리를 확인해 주세요.");
				return;
			}
			else if (!$("#m_sex1").is(":checked") && !$("#m_sex2").is(":checked")){
				alert("성별을 확인해 주세요.");
				return;
			}
			else if ($("#m_region > option:selected").val() == '-1'){
				alert("지역을 확인해 주세요.");
				return;
			}
			else if ($("#m_grade > option:selected").val() == '-1'){
				alert("학년을 확인해 주세요.");
				return;
			}
			else if (!chkSubmit($('#m_birth'), "생일을"))
				return;
			else if (ageCalc()<0){
				alert("만 14세 이하는 어린이 회원으로 가입하여야 합니다");
				return;
			}
			else if (!$("#m_private1").is(":checked") && !$("#m_private2").is(":checked")){
				alert("개인정보 공개여부를 확인해 주세요.");
				return;
			}
			else {
				if ($("#m_sex1").is(":checked")) {
					$("#m_sex").val("남성");
				}
				if ($("#m_sex2").is(":checked")) {
					$("#m_sex").val("여성");
				}
				if ($("#m_private1").is(":checked")) {
					$("#m_private").val("1");
				}
				if ($("#m_private2").is(":checked")) {
					$("#m_private").val("0");
				}
				if ($("#emailList").val() == '0') {
					$("#m_email").val($("#email1").val()+"@"+ $("#email2").val());
				} else {
					$("#m_email").val($("#email1").val()+"@"+ $("#emailList").val());
				}
				$("#joinForm").attr({
					"method" : "POST",
					"action" : "/join/joinPro.do"
				});
				$("#joinForm").submit();
			}
		}); /*취소 버튼 클릭시 처리 이벤트*/
		$("#cancel").click(function() {
			location.href="/join/joinAge.do";
		});
		//이메일 자동입력
		$("#emailList").change(function() {
			if ($("#emailList option:selected").val() == '0') {
				$("#email2").val('');
				$("#email2").attr("disabled", false);
			} else  {
				if ($("#emailList option:selected").val() == '-1') {
					$("#email2").val('');
				} else {
					$("#email2").val($("#emailList").val());
				}
				$("#email2").attr("disabled", true);
			}
		});
	});
	//14세 이상은 0이상 미만은 0미만을 리턴
	function ageCalc() {
		var now = new Date();
		//한국 자정기준으로 바꿔줌
		if (now.getHours()>15) {
			now.setDate(now.getDate()+1);
		}
		now.setHours(9);
		now.setMinutes(0);
		now.setSeconds(0);
		now.setMilliseconds(0);
		var birth = new Date($("#m_birth").val());
		nowYear = now.getFullYear();
		birthYear = birth.getFullYear();
		return now - birth - 441849600000;
	}
	/* 아이디 중복 검사 */
	function memberChk() {
		if ($("#checkBeforeId").val().replace(/\s/g, "") == ""){
			alert("아이디를 확인해주세요.");
		false;
		}
		else{
			$("#m_idChkAsk").empty();
			var m_id = $("#checkBeforeId").val().toLowerCase();
			$("#m_id").val(m_id);
			var url = "/join/memberChk/" + m_id + ".do";
			$.getJSON(url, function(data) {
				 console.log(data.length);
				if (data == 0) {
					booleanIdChk = true;
					$("#m_idChkAsk").append("<font class=''>" + m_id + "는 사용가능한 아이디 입니다</font>");
				} else if (data == 1) {
					booleanIdChk = false;
					$("#m_idChkAsk").append("<font class='colorRed'>" + m_id + " 는중복된 아이디 입니다</font>");
				} else {
					booleanIdChk = false;
					$("#m_idChkAsk").append("<font class='colorRed'>리턴값 에러 시스템 관리자에게 문의하세요</font>");	
				} 
			}).fail(function() {
				booleanIdChk = false;
				alert("중복 아이디 검사를 실패하였습니다. 잠시후에 다시 시도해 주세요");
			});
		}
	}
</script>
<style type="text/css">

h1, h2, h3, h4, h5, h6, p, dl, dt, dd, table, thead, tbody, tr, th, td {
	margin: 0;
	padding: 0;
	font-weight: normal;
}

#joinContent {
	width: 900px;
	position: relative;
	margin: 0 auto;
	clear: both;
}

#join_title {
	position: relative;
	margin-bottom: 78px;
	padding-top: 89px;
	width: 100%;
}

#join_title h3 {
	position: absolute;
	top: 74px;
	left: 50%;
	padding: 0 30px;
	color: #323232;
	font-size: 34px;
	background: #fff;
	text-transform: uppercase;
}

.cap2 {
	margin-bottom: 10px;
	padding-left: 10px;
	font-size: 20px;
	font-weight: 560; line-height : 16px;
	background: url(/resources/include/img/m_blt_squre.png) 0 center
		no-repeat;
	background-size: 3px;
	line-height: 16px;
}
th.info {
    padding-left: 30px !important;
    background: #ececec url(/resources/include/img/ico_essential.png) no-repeat 11px 50%;
}
th {
    height: 30px;
    padding: 10px 0 10px 30px;
    border-top: 1px solid #fff;
    text-align: left;
    line-height: 20px;
    font-family: nGothic;
    font-weight: normal;
    background: #ececec;
}
td {
    height: 30px;
    padding: 10px 0 10px 20px;
    border-top: 1px solid #ececec;
    line-height: 20px;
    vertical-align: middle;
    word-break: break-all;
}
table{
ont-family: NanumGothic, '나눔고딕', nGothic, dotum, gulim;
    font-size: 13px;
    font-weight: normal;
    color: #333;}
.form-control{
width: 170px;
display: inline-block;
}
textarea.form-control{
width: 100%;
}
#agree_btn{
    width: 100%;
    position: relative;
    margin: 50px auto;
    text-align: center;
    
}
</style>
</head>
<body>
	<div id="header">
		<jsp:include page="../mainpage/header.jsp"></jsp:include>
	</div>
	<section id="joinContent">
		<div id="join_title">
			<h3 style="margin-left: -104px;">회원 가입</h3>
		</div>
		<h2 class="cap2">회원정보 입력<span style="font-size: 14px; color: #727272;">
		<img src="/resources/include/img/ico_essential.png" alt="필수항목">는 필수입력 항목 입니다.</span></h2>
<div id="join_form">
		<form name="joinForm" id="joinForm" enctype="multipart/form-data">
			<table>
			<colgroup>
			<col width="22%">
			<col width="78%">
			<tr>
			<th class="info">아이디</th>
			<td class="form-inline">
				<input type="text" class="form-control" id="checkBeforeId" name="checkBeforeId"><span id="m_idChkAsk"></span>
				<input type="button" id="memberChkBtn" name="memberChkBtn" class="btn btn-default" onclick="memberChk()" value="ID 중복 검사">
				<input type="hidden" id="m_id" name="m_id" onclick="memberChk()" value="ID 중복 검사"><br /> </td></tr>
				<tr>
				<th class="info">이름</th>
				<td class="form-inline"><input type="text" class="form-control" id="m_name" name="m_name"></td></tr>
				<tr>
				<th class="info">비밀번호</th>
				<td class="form-inline"><input type="password" class="form-control" id="m_pw" name="m_pw"></td>
				</tr>
				<tr>
				<th class="info">비밀번호 확인</th>
				<td class="form-inline"><input type="password" class="form-control" id="m_pw2" name="m_pw2"></td></tr>
				
				<tr>
				
				<th class="info">이메일</th>
				<td class="form-inline"><input type="text" class="form-control" id="email1" name="email1" /> @
				<input type="text" class="form-control" id="email2" name="email2" disabled="disabled"/>
				<select class="form-control" id="emailList" name="emailList">
									<option value='-1'>-이메일 선택-</option>
									<option value='0'>직접입력</option>
									<option value='gmail.com'>gmail.com</option>
									<option value='naver.com'>naver.com</option>
									<option value='nate.com'>nate.com</option>
									<option value='hanmail.net'>hanmail.net</option>
									<option value='daum.com'>daum.com</option>
							</select>
							<input type="hidden" id="m_email" name="m_email" /></td></tr>
				<tr>
				
				 <th class="info">성별</th>
				<td class="form-inline">
				<input type="hidden" name="m_sex" id="m_sex">
				<input type="radio" name="sex" id="m_sex1" value="남성">남성 &nbsp;&nbsp;&nbsp;
				<input type="radio" name="sex" id="m_sex2" value="여성">여성</td></tr>
				<tr>
				
				<th class="info"> 지역</th>
				<td class="form-inline"><select class="form-control" id="m_region" name="m_region">
					<option value="서울">서울</option>
					<option value="경기">경기</option>
					<option value="인천">인천</option>
					<option value="충북">충북</option>
					<option value="충남">충남</option>
					<option value="대전">대전</option>
				</select></td></tr>
				<tr>
				<th class="info">학년</th>
				<td class="form-inline"><select class="form-control" id="m_grade" name="m_grade">
					<option value="0">미취학</option>
					<option value="1" selected="selected">초등학교 1학년</option>
					<option value="2">초등학교 2학년</option>
					<option value="3">초등학교 3학년</option>
					<option value="4">초등학교 4학년</option>
					<option value="5">초등학교 5학년</option>
					<option value="6">초등학교 6학년</option>
					<option value="7">중학교 1학년</option>
					<option value="8">중학교 2학년</option>
					<option value="9">중학교 3학년</option>
					<option value="10">고등학교 1학년</option>
					<option value="11">고등학교 2학년</option>
					<option value="12">고등학교 3학년</option>
					<option value="13">대학생</option>
					<option value="14">일반</option>
					<option value="15">기타</option>
				</select></td></tr>
				<tr>
				<th class="info">생일</th>
				<td class="form-inline"><input type="date" class="form-control" name="m_birth" id="m_birth"></td></tr>
				<tr>
				<th class="info">자기소개</th>
				<td><textarea class="form-control" rows="4" cols="800" name="m_pr" id="m_pr"></textarea></td></tr>
				<tr>
				<th class="info">프로필사진</th>
				<td><input type="file" name="file" id="file"></td></tr>
				<tr>
				<th class="info">개인정보공개여부</th>
				<td>
				<input type="hidden" name="m_private" id="m_private">
				<input type="radio" name="private" id="m_private1" value="1">예 &nbsp;&nbsp;&nbsp;
				<input type="radio" name="private" id="m_private2" value="0">아니요</td></tr>
				
			</table>
		</form>
		</div>
		<div id="agree_btn">
		<button type="button" class="btn btn-success" id="joinPro" name="joinPro">가입 완료</button>
		<button type="button" class="btn btn-default" id="cancel" name="cancel">취소</button></div>
	</section>
	<div id="footer">
		<jsp:include page="../mainpage/footer.jsp"></jsp:include>
	</div>

</body>
</html>
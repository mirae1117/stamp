<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<title>비밀번호 변경</title>
<style type="text/css">
#id_tr{
display: none;
}
</style>
<script type="text/javascript">
$(function() {
	var result = '${msg}';
	if (result != null && result != '') {
		alert(result);
	}
	$(document).on("click", "#change", function() {
		if (!chkSubmit($('#m_pw'), "비밀번호를"))
			return;
		else if ($('#m_pw').val() != $('#m_pw2').val()){
			alert("비밀번호가 서로 다릅니다");
			return;
		}
		else {
			 $("#changeForm").attr({
				"method" : "POST",
				"action" : "/join/pwFindChange.do"
			});
			$("#changeForm").submit();
		}
	});
	$(document).on("click", "#close", function() {
		close();
	});
});
</script>
<style type="text/css">
.form-control{
display: inline-block;
width: 150px;}

#change_pw th{
 text-align:center;
    width: 20%;
    background: #ececec;
    border-top: 1px solid #dddddf;
}
#change_pw td{
 height: 30px;
    padding: 10px 0 10px 20px;
    border-top: 1px solid #dddddf;
    border-bottom: 1px solid #dddddf;
    line-height: 20px;
    vertical-align: middle;
    word-break: break-all;
    line-height: 20px;
}
#change_btn{
padding-top: 20px;
padding-left: 230px;}
</style>
</head>
<body>
	<div>
		<h3 id="h3title">새 비밀번호로 변경하세요.</h3>
		<form id="changeForm" name="changeForm">
		<input type="hidden" id="m_id" name="m_id" value="${findPwChangeId}" />		
		 <table style="width: 100%; margin-top: 10px">
			<tr id="change_pw">
				<th class="info">비밀번호</th>
				<td><input type="password" class="form-control" id="m_pw" name="m_pw"></td>
			</tr>
			<tr id="change_pw">
				<th class="info">비밀번호 확인</th>
				<td><input type="password" class="form-control" id="m_pw2" name="m_pw2"></td></tr>
			</table>
			<div id="change_btn">
					<button type="button" id="change" name="change" class="btn btn-success">바꾸기</button>
					<button type="button" id="close" name="close" class="btn btn-default">닫기</button>
			</div>
		
		</form>
	</div>
</body>
</html>
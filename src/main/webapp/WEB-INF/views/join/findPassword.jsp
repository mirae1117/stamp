<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<title>비밀번호 찾기 인증</title>
<style type="text/css">
#id_tr{
display: none;
}
.form-control{
display: inline-block;
width: 150px;}
#find_key th{
 text-align:center;
    width: 15%;
    background: #ececec;
    border-top: 1px solid #dddddf;
}
#find_key td{
 height: 30px;
    padding: 10px 0 10px 20px;
    border-top: 1px solid #dddddf;
    border-bottom: 1px solid #dddddf;
    line-height: 20px;
    vertical-align: middle;
    word-break: break-all;
    line-height: 20px;
}
#find_btn{
padding-top: 20px;
padding-left: 230px;}
</style>
<script type="text/javascript">
var findFlag = 1;
$(function() {
	var result = '${msg}';
	if (result != null && result != '') {
		alert(result);
	}
	$(document).on("click", "#close", function() {
		close();
	});
	/*인증 메일 전송 버튼 클릭시 처리 이벤트*/
	$("#confirm").click(function() {
		//입력값 체크
		if (!chkSubmit($('#g_key'), "인증키를"))
			return;
		else {
			$("#confirmPwForm").attr({
				"method" : "POST",
				"action" : "/join/confirmPw.do"
			});
			$("#confirmPwForm").submit();
		}
	});
});
</script>
</head>
<body>
	<div>
		<h3 id="h3title">비밀번호 찾기 인증</h3>
		<p>인증키를 입력하여 인증을 하시기 바랍니다.</p>
		<form id="confirmPwForm" name="confirmPwForm">
		<input type="hidden" id="g_email" name="g_email" value="${findId_email}" />
		 <table style="width: 100%">
		 <colgroup>
		 <col width="15%">
		 <col width="85%">
		 </colgroup>
		 <tbody>
		 
			<tr id="find_key">
				<th>인증키 확인</th>
				<td><input type="text" class="form-control" name="g_key" id="g_key" /></td>
			</tr>
			</tbody>
			</table>
			<div id="find_btn">
		<button type="button" id="confirm" class="btn btn-success" name="confirm">인증</button>
		<button type="button" id="close" class="btn btn-default" name="close">닫기</button>
		</div>
		</form>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>미성년자 회원 가입</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						/*인증 메일 전송 버튼 클릭시 처리 이벤트*/
						$("#guardianAttestationBtn")
								.click(
										function() {
											//입력값 체크
											if (!$('#chk1').is(':checked')) {
												$('#chk1').focus();
												alert("자녀의 개인정보 수집 및 이용에 동의를 확인해 주세요.");
												return;
											} else if (!$('#chk2').is(
													':checked')) {
												$('#chk2').focus();
												alert("법정대리인의 개인정보 수집 및 이용에 동의를 확인해 주세요.");
												return;
											} else if (!chkSubmit($('#email1'),
													"법정대리인의 이메일 앞자리를"))
												return;
											else if (!chkSubmit($('#email2'),
													"법정대리인의 이메일 뒷자리를"))
												return;
											else if ($("#emailList").val() == '-1'){
												alert("법정대리인의 이메일 뒷자리를 확인해 주세요.");
												return;
											}
											else {
												if ($("#emailList").val() == '0') {
													$("#g_email").val($("#email1").val()+"@"+ $("#email2").val());
												} else {
													$("#g_email").val($("#email1").val()+"@"+ $("#emailList").val());
												}
												
												$("#guardianAttestationForm")
														.attr(
																{
																	"method" : "POST",
																	"action" : "/join/joinPaAgree.do"
																});
												$("#guardianAttestationForm")
														.submit();
											}
										}); /*취소 버튼 클릭시 처리 이벤트*/
						$("#cancel").click(function() {
							location.href = "/join/joinAge.do";
						});
						$("#allChk").change(function() {
							if ($("#allChk").is(":checked")) {
								allChk();
							} else {
								allChkOut();
							}
						});
						$("#emailList").change(function() {
							if ($("#emailList option:selected").val() == '0') {
								$("#email2").val('');
								$("#email2").attr("disabled", false);
							} else  {
								if ($("#emailList option:selected").val() == '-1') {
									$("#email2").val('');
								} else {
									$("#email2").val($("#emailList").val());
								}
								$("#email2").attr("disabled", true);
							}
						});
					});
	function allChk() {
		for (var i = 0; i < document.clause.length; i++) {
			document.clause.elements[i].checked = true;
		}

	}
	function allChkOut() {
		for (var i = 0; i < document.clause.length; i++) {
			document.clause.elements[i].checked = false;
		}

	}
</script>
<style type="text/css">


h1, h2, h3, h4, h5, h6, p, dl, dt, dd, table, thead, tbody, tr, th, td {
	margin: 0;
	padding: 0;
	font-weight: normal;
}

#joinContent {
	width: 900px;
	position: relative;
	margin: 0 auto;
	clear: both;
}

#join_title {
	position: relative;
	margin-bottom: 78px;
	padding-top: 89px;
	width: 100%;
	border-bottom: 1px solid #e5e5e5;
}

#join_title h3 {
	position: absolute;
	top: 74px;
	left: 50%;
	padding: 0 30px;
	color: #323232;
	font-size: 34px;
	background: #fff;
	text-transform: uppercase;
}

.agree_box {
	margin-top: 15px;
	padding: 27px 40px;
	height: 595px;
	line-height: 24px;
	color: #888888;
	font-size: 16px;
	font-weight: 400;
	border: 1px solid #d6d6d6;
	overflow-y: auto;
}

#chk3 {
	float: right;
	font-size: 15px;
	font-weight: 600;
}

h4 {
	font-size: 20px;
	font-weight: 600;
}

#clause {
	margin-bottom: 50px;
}
#writeEmail{
margin-top: 15px;
}

#writeEmail th {
	padding: 20px 40px;
	width: 20%;
	background: #ececec;
	border-top: 1px solid #dddddf;
}

#writeEmail td {
	height: 30px;
	padding: 10px 0 10px 20px;
	border-top: 1px solid #dddddf;
	border-bottom: 1px solid #dddddf; line-height : 20px;
	vertical-align: middle;
	word-break: break-all;
	line-height: 20px;
}

#writeEmail input[type=text] {
	height: 40px;
	display:inline-block;
	width:150px;
	padding: 4px 10px;
	margin: 0;
	border: 1px solid #d4d4d4;
	line-height: 20px;
	background: #fff;
}

select.form-control{
display:inline-block;
	height: 40px;
	width:150px;
	margin: 0; border : 1px solid #d4d4d4;
	color: #727272;
	text-align: center;
	border: 1px solid #d4d4d4;
}
#agree_btn{
    width: 100%;
    position: relative;
    margin: 50px auto;
    text-align: center;
    
}
</style>
</head>
<body>
	<div id="header">
		<jsp:include page="../mainpage/header.jsp"></jsp:include>
	</div>
	<section id="joinContent">
		<div id="join_title">
			<h3 style="margin-left: -104px;">회원 가입</h3>
		</div>

		<div id="join_agree">

			<form id="clause" name="clause">
				<h4>법정대리인 인증 약관</h4>
				<div class="agree_box"><jsp:include
						page="/resources/include/txt/guardianAgreement.jsp" /></div>
				<br /> <input type="checkbox" id="chk1" name="chk1"><font
					style="font-size: 15px; font-weight: 600;">자녀의 개인정보 수집 및 이용에
					동의 합니다.</font>&nbsp;&nbsp;&nbsp; <input type="checkbox" id="chk2"
					name="chk2" /><font style="font-size: 15px; font-weight: 600;">만
					14세 미만 아동에 대한 법정대리인의 개인정보 수집 및 이용에 동의 합니다.</font> <br /> <br /> <span
					id="chk3"> <input type="checkbox" name="allChk" id="allChk">약관에
					전체 동의합니다
				</span>

			</form>


			<h4>법정대리인 이메일</h4>
			<div id="writeEmail">
				<table style="width: 100%;">

					<colgroup>
						<col width="20%">
						<col width="80%">
					</colgroup>
					<tbody>
						<tr>
							<th>이메일</th>
							<td><input type="text" class="form-control" id="email1" name="email1" />&nbsp;@
								<input type="text" class="form-control" id="email2" name="email2"  disabled="disabled"/>&nbsp;&nbsp; 
								<select class="form-control" id="emailList" name="emailList">
									<option value='-1'>-이메일 선택-</option>
									<option value='0'>직접입력</option>
									<option value='gmail.com'>gmail.com</option>
									<option value='naver.com'>naver.com</option>
									<option value='nate.com'>nate.com</option>
									<option value='hanmail.net'>hanmail.net</option>
									<option value='daum.com'>daum.com</option>
							</select>
								<form id="guardianAttestationForm"
									name="guardianAttestationForm">
									<input type="hidden" id="g_email" name="g_email" />
								</form></td>
						</tr>
					</tbody>
				</table>
				<div id="agree_btn">
					<button type="button" class="btn btn-success" id="guardianAttestationBtn"
						name="guardianAttestationBtn">다음</button>
						 <button type="button" class="btn btn-default" id="cancel" name="cancel">뒤로가기</button>
				</div>
			</div>
		</div>
	</section>
	<div id="footer">
		<jsp:include page="../mainpage/footer.jsp"></jsp:include>
	</div>
</body>
</html>
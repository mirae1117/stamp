<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script type="text/javascript" src="/resources/include/js/common.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		/*인증 메일 전송 버튼 클릭시 처리 이벤트*/
		$("#joingo").click(function() {
			//입력값 체크
			if (!$('#chk1').is(':checked')) {
				$('#chk1').focus();
				alert("개인정보 수집 및 이용 동의를 확인해 주세요.");
				return;
			} else if (!$('#chk2').is(':checked')) {
				$('#chk2').focus();
				alert("개인정보 위탁 동의를 확인해 주세요.");
				return;
			} else if (!$('#chk3').is(':checked')) {
				$('#chk2').focus();
				alert("이용 약관을 확인해 주세요.");
				return;
			}
			else {
				location.href = "/join/joinForm.do";
			}
		}); /*취소 버튼 클릭시 처리 이벤트*/
	      $("#cancel").click(function() {
	         location.href="/join/joinAge.do";
	      });
	      $("#allChk").change(function(){
	           if($("#allChk").is(":checked")){
	              allChk();
	           }else{
	              allChkOut();
	           }
	       });
	      
	   });
	   function allChk() {
	      for (var i = 0; i < document.clause.length; i++) {
	         document.clause.elements[i].checked=true;
	      }
	      
	   }   
	   function allChkOut() {
	      for (var i = 0; i < document.clause.length; i++) {
	         document.clause.elements[i].checked=false;
	      }
	      
	   }
</script>
<style type="text/css">

h1, h2, h3, h4, h5, h6, p, dl, dt, dd, table, thead, tbody, tr, th, td {
	margin: 0;
	padding: 0;
	font-weight: normal;
}
#joinContent {
	width: 900px;
	position: relative;
	margin: 0 auto;
	clear: both;
}
#join_title {
	position: relative;
	margin-bottom: 78px;
	padding-top: 89px;
	width: 100%;
	border-bottom: 1px solid #e5e5e5;
}
#join_title h3 {
	position: absolute;
	top: 74px;
	left: 50%;
	padding: 0 30px;
	color: #323232;
	font-size: 34px;
	background: #fff;
	text-transform: uppercase;
}
.cap2{
    margin-bottom: 10px;
    padding-left: 10px;
    color: #777777;
    font-size: 15px;
    line-height: 16px;
    background: url(/resources/include/img/m_blt_squre.png) 0 center no-repeat;
    background-size: 3px;
}

h4 {
	font-size: 20px;
	font-weight: 600;
}
#join_agree {
	margin-top: 30px;
}
.agree_box {
	margin-top: 10px;
	padding: 27px 20px;
	height: 200px;
	line-height: 24px;
	color: #888888;
	font-size: 16px;
	font-weight: 400;
	border: 1px solid #d6d6d6;
	overflow-y: auto;
}
.agree_chk {
    text-align: right;
    font-size: 15px;
font-weight: 600;
}
.agree_chk2{
text-align: right;
font-size: 15px;
font-weight: 600;
}
#agree_btn{
    width: 100%;
    position: relative;
    margin: 50px auto;
    text-align: center;
    
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>일반회원 가입</title>
</head>
<body>
		<jsp:include page="../mainpage/header.jsp"></jsp:include>
<section id="joinContent">
<div id="join_title">
			<h3 style="margin-left: -104px;">회원 가입</h3>
		</div>

<p class="cap2">그루터기는 커뮤니티 관리를 위해 아래와 같은 개인정보를 수집합니다.</p>

<div id="join_agree">
	<form id="clause" name="clause">
		<h4>개인정보 수집 및 이용 동의</h4>
		<div class="agree_box">
		<jsp:include page="/resources/include/txt/informationCollectedAgreement.jsp" /></div>
		<p class="agree_chk"><input type="checkbox" id="chk1" name="chk1">약관에 동의합니다.</p>
		<h4>개인정보 위탁 동의</h4>
		<div class="agree_box"><jsp:include page="/resources/include/txt/privacyAgreement.jsp" /></div>
		<p class="agree_chk"><input type="checkbox" id="chk2" name="chk2">약관에 동의합니다</p>
		<h4>이용 약관</h4>
		<div class="agree_box"><jsp:include page="/resources/include/txt/termsUse.jsp" /></div>
		<p class="agree_chk"><input type="checkbox" id="chk3" name="chk3">약관에 동의합니다</p><br />
		<p class="agree_chk2"><input type="checkbox" name="allChk" id="allChk" >약관에 모두 동의합니다</p>
		<div id="agree_btn">
		<button type="button" class="btn btn-success" id="joingo" name="joingo">다음</button>
		<button type="button" class="btn btn-default" id="cancel" name="cancel">뒤로가기</button>
		</div>
	</form>
	</div>
	</section>
	<div id="footer">
		<jsp:include page="../mainpage/footer.jsp"></jsp:include>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<style type="text/css">
.boardScroll {
	margin-top: 70px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>문제 풀이</title>
</head>
<body>
	<jsp:include page="../mainpage/header.jsp" />
	<section class="boardScroll">
		<jsp:include page="boardScroll.jsp" />
	</section>
	<jsp:include page="../mainpage/footer.jsp" />
</body>
</html>
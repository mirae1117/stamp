<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">
.searchForm {
	
}

#main {
	padding-top: 5px;
	max-width: 960px;
}

#img {
	width: 242px;
	height: 250px;
	margin: auto;
}

#list {
	padding-top: 10px;
}

.list-group-item {
	width: 150px;
}

.card-content {
	display: inline;
	width: 250px;
}

.glyphicon {
	margin-right: 10%;
}

.thumbnail {
	width: 320px;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('#info_grade').val($('#q_grade').val());

		//var info_grade = $("#q_grade").val();
		//alert(info_grade);
		//$("#info_grade").val(info_grade).attr("selected", "selected");
		//$("#info_grade").val(info_grade);

		/* 	질문하기 버튼 클릭   */
		$("#questionBtn").click(function() {
			if ($("#log_id").val() == "") {
				alert("로그인 후 이용해주세요.");
				return;
			} else {
				location.href = "/qboard/writeForm.do";
			}
		});

		//학년검색
		$("#search").click(function() {
			$("#searchForm").attr("method", "GET");
			$("#searchForm").attr("action", "/qboard/boardList.do");
			$("#searchForm").submit();
		});
		$("#info_grade").change(function() {
			$("#q_grade").val($("#info_grade").val());
		});

		//과목검색
		$(".list-group-item").click(function() {
			$("#q_subject").val($(this).val());
			//alert(q_subject);
			//alert(q_grade);
			$("#searchForm").attr("method", "GET");
			$("#searchForm").attr("action", "/qboard/boardList.do");
			//$.data("#listForm", "q_subject", q_subject);
			$("#searchForm").submit();

		});

		/* 		$(".thumbnail").click(function() {
		 var q_no = $("#q_no").val();
		 alert(q_no);
		 location.href = "/qboard/boardDetail.do?q_no=" + q_no;
		 });
		 */

	});
</script>


<script type="text/javascript">
	$(document)
			.ready(
					function() {

						$(document)
								.scroll(
										function() {
											maxHeight = $(document).height();
											currentScroll = $(window)
													.scrollTop()
													+ $(window).height();
											if (maxHeight <= currentScroll) {
												var firstqno = $(
														".thumbnail:last>#q_no")
														.val() - 1;
												//alert(firstqno);
												$
														.ajax({
															type : 'post',
															url : "/qboard/scrollDown.do",
															headers : {
																"Content-Type" : "application/json",
																"X-HTTP-Method-Override" : "POST"
															},
															dataType : 'json',
															data : JSON
																	.stringify({
																		q_no : firstqno
																	}),
															success : function(
																	data) {
																var str = "";
																console
																		.log(this);
																if (data != "") {
																	$(data)
																			.each(
																					function() {
																						//str += "<div class='"+"row"+"'>";
																						str += "<div class='col-sm-6 col-md-4'><div class='thumbnail'><div class='row'><div class='col-xs-12 col-md-8'>"
																								+ "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span class='glyphicon glyphicon-user'>"
																								+ this.m_name;

																						str += "(<a href='javascript:otherUserInfo('"
																								+ this.m_id
																								+ "')'>"
																								+ this.m_id
																								+ "</a>)";
																						str += "</span></div>";
																						str += "<div class='col-xs-6 col-md-4'>&nbsp&nbsp&nbsp&nbsp<span class='glyphicon glyphicon-eye-open'>"
																								+ this.q_count
																								+ "</span></div></div>";

																						str += "<input type='hidden' id='q_no' name='q_no' value='"+this.q_no+"'>";
																						str += "<a href=/qboard/boardDetail.do?q_no="
																								+ this.q_no
																								+ "><img id='img' src='/uploadStorage/"+this.q_file+"'/> ";
																						str += "</a>";
																						str += "<div><div>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<label>글번호.</label>"
																								+ this.q_no
																								+ "</div>";
																						str += "<div>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<label>제목</label>"
																								+ this.q_title
																								+ "</div></div></div></div>";
																					});

																	$(
																			"#main>.row")
																			.append(
																					str);
																	errImg();
																} else {
																	alert("더 불러올 데이터가 없습니다.");
																}//else
															}//success
														});//ajax
											}
										});
					});
</script>

<div id="wrap">
	<form id="searchForm" name="searchForm">
		<c:if test="${param.q_subject == 0 || param.q_subject == null}">
			<input type="hidden" id="q_subject" name="q_subject" value="0">
		</c:if>
		<c:if test="${param.q_subject != 0 && param.q_subject != null}">
			<input type="hidden" id="q_subject" name="q_subject"
				value="${param.q_subject}">
		</c:if>
		<c:if test="${param.q_grade == -1 || param.q_grade == null}">
			<input type="hidden" id="q_grade" name="q_grade" value="-1">
		</c:if>
		<c:if test="${param.q_grade != -1 && param.q_grade != null}">
			<input type="hidden" id="q_grade" name="q_grade"
				value="${param.q_grade}">
		</c:if>
	</form>
	<div class="form-inline">

		<div class="btn-group">
			<button type="button" class="btn btn-default dropdown-toggle"
				data-toggle="dropdown" aria-expanded="false">
				과목<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li class="list-group-item" value="0">-전체</li>
				<li class="list-group-item" value="1">-수학</li>
				<li class="list-group-item" value="2">-과학</li>
				<li class="list-group-item" value="3">-영어</li>
				<li class="list-group-item" value="4">-국어</li>
				<li class="list-group-item" value="5">-한문</li>
				<li class="list-group-item" value="6">-역사</li>
				<li class="list-group-item" value="7">-경제</li>
				<li class="list-group-item" value="8">-사회</li>
				<li class="list-group-item" value="9">-국가자격시험</li>
				<li class="list-group-item" value="10">-기타</li>
			</ul>
		</div>
		<div class="form-group">
			<!-- 검색창 -->
			<select id="info_grade" name="info_grade" class="form-control"
				style="width: 150px">
				<option value="-1">학년</option>
				<option value="1">초1</option>
				<option value="2">초2</option>
				<option value="3">초3</option>
				<option value="4">초4</option>
				<option value="5">초5</option>
				<option value="6">초6</option>
				<option value="7">중1</option>
				<option value="8">중2</option>
				<option value="9">중3</option>
				<option value="10">고1</option>
				<option value="11">고2</option>
				<option value="12">고3</option>
				<option value="13">대학생</option>
				<option value="14">일반</option>
				<option value="15">기타</option>
			</select>
			<button type="button" class="btn btn-primary" id="search"
				name="search">검색</button>

			<button type="button" class="btn btn-default" name="questionBtn"
				id="questionBtn">
				질문하기 <span class="glyphicon glyphicon-pencil"></span>
			</button>

		</div>
	</div>



	<form name="listForm" id="listForm">
		<!-- 게시판 리스트 시작 -->
		<div id="main">

			<div class="row">
				<c:choose>
					<c:when test="${not empty boardList }">
						<c:forEach var="question" items="${boardList }" varStatus="status">
							<div class="col-sm-6 col-md-4">
								<div class="thumbnail">
									<div class="row">
										<div class="col-xs-12 col-md-8">
											&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<span
												class="glyphicon glyphicon-user">${question.m_name}(<a
												href="javascript:otherUserInfo('${question.m_id}')">${question.m_id}</a>)
											</span>
										</div>
										<div class="col-xs-6 col-md-4">
											&nbsp&nbsp&nbsp&nbsp<span
												class="glyphicon glyphicon-eye-open">${question.q_count }</span>
										</div>
									</div>
									<input type="hidden" id="q_no" name="q_no"
										value="${question.q_no}"> <a
										href=/qboard/boardDetail.do?q_no=${question.q_no}> <img
										id="img" src="/uploadStorage/${question.q_file}" />
									</a>

									<div>
										<div>
											&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<label>글번호.</label>${question.q_no}</div>
										<div>
											&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<label>제목</label>${question.q_title }
										</div>
									</div>

								</div>
							</div>
						</c:forEach>
					</c:when>
				</c:choose>
			</div>
		</div>
	</form>
</div>
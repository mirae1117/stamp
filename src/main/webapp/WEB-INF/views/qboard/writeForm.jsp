<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>글쓰기</title>

<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<style>
@import url(http://fonts.googleapis.com/earlyaccess/nanumgothic.css);

body {
	font-family: 'Nanum Gothic', sans-serif;
}

#column_1 {
	background-color: #86E57F;
}

#main {
	width: 680px;
	margin-top: 80px;
	padding: auto;
}

.col-sm-10 {
	margin-left: 57%;
}

.control-label {
	margin-left: 45%;
}
</style>



<script type="text/javascript">
	$(document).ready(function() {
		$("#boardInsertBtn").click(function() {
			if ($("#q_grade").val() == "") {
				alert("학년을 입력해주세요");
				return;
			} else if ($("#q_subject").val() == "") {
				alert("과목을 입력해주세요");
				return;
			} else if ($("#q_title").val() == "") {
				alert("제목을 입력해주세요");
				return;
			} else if ($("#q_content").val() == "") {
				alert("내용을 입력해주세요");
				return;
			} else {

				$("#q_writeForm").attr({
					"method" : "post",
					"action" : "/qboard/boardInsert.do"

				});
				$("#q_writeForm").submit();
			}
		});

		$("#boardListBtn").click(function() {
			location.href = "/qboard/boardList.do";

		});
	});
</script>
</head>
<body>

	<jsp:include page="../mainpage/header.jsp" />

	<div id="main">
		<h3 align="center">질문작성</h3>
		<form id="q_writeForm" name="q_writeForm" class="form-horizontal"
			enctype="multipart/form-data">
			<input type="hidden" name="csrf" value="${CSRF_TOKEN}" />



			<div class="form-group">
				<label class="col-sm-2 control-label">학년</label>
				<div class="col-sm-10">
					<select id="q_grade" name="q_grade" class="form-control">
						<option></option>
						<option value="1">초1</option>
						<option value="2">초2</option>
						<option value="3">초3</option>
						<option value="4">초4</option>
						<option value="5">초5</option>
						<option value="6">초6</option>
						<option value="7">중1</option>
						<option value="8">중2</option>
						<option value="9">중3</option>
						<option value="10">고1</option>
						<option value="11">고2</option>
						<option value="12">고3</option>
						<option value="13">대학생</option>
						<option value="14">일반</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">과목</label>
				<div class="col-sm-10">
					<select id="q_subject" name="q_subject" class="form-control">
						<option></option>
						<option value="1">수학</option>
						<option value="2">과학</option>
						<option value="3">영어</option>
						<option value="4">국어</option>
						<option value="5">한문</option>
						<option value="6">역사</option>
						<option value="7">경제</option>
						<option value="8">사회</option>
						<option value="9">국가자격시험</option>
						<option value="10">기타</option>

					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">제목</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="q_title" id="q_title"
						placeholder="Title">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">내용</label>
				<div class="col-sm-10">
					<textarea class="form-control" name="q_content" id="q_content"
						placeholder="Content" rows="15px"></textarea>
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-2 control-label">파일 업로드</label>
				<div class="col-sm-10">
					<input type="file" id="file" name="file">
				</div>
			</div>

			<div align="center" class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="button" id="boardInsertBtn" name="boardInsertBtn"
						class="btn btn-default">글쓰기</button>
					<button type="button" id="boardListBtn" name="boardListBtn"
						class="btn btn-default">목록으로</button>
				</div>
			</div>
		</form>
	</div>

	<jsp:include page="../mainpage/footer.jsp" />

</body>
</html>
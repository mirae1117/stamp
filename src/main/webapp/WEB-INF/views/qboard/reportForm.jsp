<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- 부가적인 테마 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript">
		//AJAX 신고
		$(document).on("click", "#reportBtn", function() {
			var log_id = opener.getLog_id();
			var q_no = opener.getq_no();
			$("#reportModal").modal('hide');
			q_no = '1_' + q_no;
			$.ajax({
				url : "/report/reportInsert.do",
				type : 'post',
				data : {
					re_suspect : q_no,
					m_id : log_id,
					re_state : $("#re_state").val(),
					re_title : $("#re_title").val(),
					re_content : $("#re_content").val()

				},
				dataType : 'text',
				success : function(result) {
					console.log("result: " + result);
					if (result == 'SUCCESS') {
					}
				}
			});
				alert("신고 되었습니다.");
				close();
		});
		$(document).on("click", "#back", function() {
			close();	
		});
</script>
</head>
<body>
	<form id="reportForm" name="reportForm">
		<div class="modal-header">
			<h4 class="modal-title">게시글 신고</h4>
		</div>
		<div class="modal-body">

			<div class="form-group">
				<label class="control-label">신고종류</label> <select id="re_state"
					name="re_state" class="form-control">
					<option></option>
					<option value="1">홍보/영리</option>
					<option value="2">불법정보</option>
					<option value="3">음란/선정성</option>
					<option value="4">욕설/인신공격</option>
					<option value="5">개인정보노출</option>
					<option value="6">같은 글 반복 게시</option>
				</select>
			</div>

			<div class="form-group">
				<label class="control-label">제목</label> <input type="text"
					class="form-control" name="re_title" id="re_title">
			</div>
			<div class="form-group">
				<label class="control-label">내용</label>
				<textarea class="form-control" id="re_content" name="re_content"></textarea>
			</div>

		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dusnuss="modal"
				id="back" name="back" data-dismiss="modal">닫기</button>
			<button type="button" class="btn btn-primary" id="reportBtn"
				name="reportBtn">신고하기</button>
		</div>
	</form>

</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update Writing</title>

<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script>

<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- 부가적인 테마 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(function() {
		//이미지 미리보기
		var img = $("<img>");
		$('#imgView').hover(function() {
			img.attr({
				src : "/uploadStorage/${updateData.q_file}",
				width : "200px",
				height : "300px"
			});
			img.addClass("imgViewData");
			$('#imgArea').append(img);
		}, function() {
			img.remove();
		});
	});

	$(document).ready(function() {
		//select box 값 선택
		$("#q_grade").val("${updateData.q_grade}");
		$("#q_subject").val("${updateData.q_subject}");
		
		
		//업데이트 버튼
		$("#boardUpdateBtn").click(function() {
			$("#q_updateForm").attr({
				"method" : "POST",
				"action" : "/qboard/boardUpdate.do"
			});
			$("#q_updateForm").submit();
		});

		//뒤로가기버튼
		$("#back").click(function() {
			var q_no = $("#q_no").val();
			location.href = "/qboard/boardDetail.do?q_no=" + q_no
		});

	});
</script>



<style>
#main {
	width: 680px;
	margin: auto;
	padding: auto;
}
</style>
</head>
<body>
	<div id="main">
		<h3 align="center">글 수정하기</h3>
		<form id="q_updateForm" name="q_updateForm" class="form-horizontal"
			enctype="multipart/form-data">
			<input type="hidden" id="q_no" name="q_no" value="${updateData.q_no}" />

			<div class="form-group">
				<label class="col-sm-2 control-label">학년</label>
				<div class="col-sm-10">
					<select id="q_grade" name="q_grade" class="form-control">
						<option></option>
						<option value="1">초1</option>
						<option value="2">초2</option>
						<option value="3">초3</option>
						<option value="4">초4</option>
						<option value="5">초5</option>
						<option value="6">초6</option>
						<option value="7">중1</option>
						<option value="8">중2</option>
						<option value="9">중3</option>
						<option value="10">고1</option>
						<option value="11">고2</option>
						<option value="12">고3</option>
						<option value="13">대학생</option>
						<option value="14">일반</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">과목</label>
				<div class="col-sm-10">
					<select id="q_subject" name="q_subject" class="form-control">
						<option></option>
						<option value="1">수학</option>
						<option value="2">과학</option>
						<option value="3">영어</option>
						<option value="4">국어</option>
						<option value="5">한문</option>
						<option value="6">역사</option>
						<option value="7">경제</option>
						<option value="8">사회</option>
						<option value="9">국가자격시험</option>
						<option value="10">기타</option>

					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">제목</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="q_title" id="q_title"
						value="${updateData.q_title}">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">내용</label>
				<div class="col-sm-10">
					<textarea class="form-control" name="q_content" id="q_content"
						rows="15px">${updateData.q_content }</textarea>
				</div>
			</div>


			<div class="form-group">
				<label class="col-sm-2 control-label">파일 업로드</label>
				<div class="col-sm-10">
					<input type="file" id="file" name="file"> <label
						id="imgView">기존 이미지 파일: ${updateData.q_file}<span
						id="imgArea"></span></label>
				</div>
			</div>


			<div class="form-group" align="center">
				<button type="button" id="boardUpdateBtn" name="boardUpdateBtn"
					class="btn btn-default">
					<span class="glyphicon glyphicon-ok"></span>확인
				</button>
				<button type="button" id="back" name="back" class="btn btn-default">
					<span class="glyphicon glyphicon-remove"></span>취소
				</button>

			</div>
		</form>
	</div>

</body>
</html>
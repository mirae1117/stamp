<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>
<script type="text/javascript">

	$(function() {
		var q_no = $("#q_no").val();
		listAll(q_no);
		errImg();
	});

	function listAll(q_no) {
		$.getJSON(
				"/reply/all/" + q_no + ".do",
				function(data) {
					if (data.length == 0) {
						$("#comment_list").append(
								"<p>답변이 없습니다 맨처음의 답변을 달아주세요.</p>");
					}

					$(data).each(
							function() {
								var rep_no = this.rep_no;
								var m_id = this.m_id;
								var m_name = this.m_name;
								var rep_content = this.rep_content;
								var rep_file = this.rep_file;
								var rep_state = this.rep_state;
								var rep_time = this.rep_time;

								addNewItem(rep_no, m_id, m_name, rep_content, rep_file,
										rep_state, rep_time);
							});
				}).fail(function() {
			alert("덧글 목록을 불러오는데 실패하였습니다. 잠시후에 다시 시도해 주세요");
		});
	}

	function addNewItem(rep_no, m_id, m_name, rep_content, rep_file, rep_state,
			rep_time) {
		var new_li = $("<div>");
		new_li.attr("data-num", rep_no);
		new_li.addClass("comment_item");

		var writer_p = $("<p>");
		writer_p.addClass("writer");

		var button_p = $("<p>");

		var name_span = $("<span>");
		name_span.addClass("name");
		name_span.html("작성자  : " + m_name + "(<a href='#this' onclick='otherUserInfo(&#39;" + m_id +"&#39;)'>" + m_id +"</a>)");
		var state_span = $("<span>");
		state_span.html("rep_state : " + rep_state);

		var time_span = $("<span>");
		var writer_Time = new Date(rep_time);

		time_span.html("/작성시간 : " + writer_Time.getFullYear() + "년 "
				+ (writer_Time.getMonth() +1) + "월 " + writer_Time.getDate() + "일 "
				+ writer_Time.getHours() + "시 " + writer_Time.getMinutes()
				+ "분 ");

		// 수정하기 버튼
		var up_input = $("<input>");
		up_input.attr({
			"type" : "button",
			"value" : "수정하기",
			"class" : "btn btn-default"
		});
		up_input.addClass("update_form");
		// 삭제하기 버튼
		var del_input = $("<input>");
		del_input.attr({
			"type" : "button",
			"value" : "삭제하기",
			"class" : "btn btn-danger"
		});
		del_input.addClass("delete_btn");

		var content_p = $("<p>");
		content_p.addClass("con");
		content_p.html(rep_content);

		var content_img = $("<img>");
		content_img.attr({
			"src" : "/uploadStorage/" + rep_file,
			"width" : "200px",
			"height" : "250px",
			"onerror" : "javascript:this.src='/resources/include/img/no_img.jpg'"
		});
		content_img.addClass("content_img");

		var span_select = $("<span>");
		span_select.attr({
			"class" : "glyphicon glyphicon-thumbs-up"
		});
		span_select.html("채택");

		var span_report = $("<span>");
		span_report.attr({
			"class" : "glyphicon glyphicon-thumbs-down"
		});
		span_report.html("신고");

		writer_p.append(name_span).append(time_span);
		if (rep_state == 2) {
			if (m_id == $("#log_id").val()) {
				button_p.append(up_input).append(del_input);
			} else if ($("#m_id").val() == $("#log_id").val()) {
				button_p.append(del_input).append(span_select).append(span_report);
			} else {
				button_p.append(span_report);
			}
			new_li.append(writer_p).append(content_p).append(content_img).append(button_p).append("<hr />");
			$("#comment_list").append(new_li);
		//채택 된 답변
		} else if (rep_state == 1){
			if (m_id == $("#log_id").val()) {
			} else if ($("#m_id").val() == $("#log_id").val()) {
				button_p.append(span_report);
			} else {
				button_p.append(span_report);
			}
		
		$("#comment_list").append(new_li);
			new_li.append(writer_p).append(content_p).append(content_img).append(button_p).append("<hr />");
			$("#best_comment").append(new_li);
		}
	}

	/*  댓글 채택    */
	$(document).on("click", ".glyphicon-thumbs-up", function() {
		//	alert("dd");
		var rep_no = $(this).parents("div").attr("data-num");
		console.log("rep_no" + rep_no);
		$.ajax({
			url : '/reply/select' + rep_no + '.do',
			type : 'post',
			data : {
				rep_no : rep_no
			},
			dataType : 'text',
			success : function(result) {
				console.log("result: " + result);
				if (result == 'SUCCESS') {
					alert("채택 되었습니다.");
					location.reload(true);
				}
			}
		});
	});

	/* 	댓글 신고   */
	$(document).on("click", ".glyphicon-thumbs-down", function() {
		if ($("#log_id").val() == null || $("#log_id").val() == '') {
			alert("로그인을 하여야 신고가 가능합니다.");
			return false;
		}
		var rep_no = $(this).parents("div").attr("data-num");
		console.log("rep_no" + rep_no);
		rep_no = "2_" + rep_no;
		$.ajax({
			url : '/report/reportInsert.do',
			type : 'post',
			data : {
				re_suspect : rep_no,
				re_title : '댓글신고',
				re_content : '댓글신고',
				m_id : $("#log_id").val(),
				re_state : '1'
			},
			dataType : 'text',
			success : function(result) {
				console.log("result: " + result);
				alert("신고 되었습니다.");
			}
		});
	});

	/** 댓글사진 클릭시 모달이동 */
	$(document).on("click", ".content_img", function() {
		$(".modal_img").remove();
		var rep_file = $(this).attr("src");

		//alert(rep_file);

		var img = $("<img>");
		img.attr({
			"src" : rep_file,
			"height" : "500px",
			"width" : "400px",
			"class" : "modal_img"
		});
		$(".modal-body").append(img);
		$("#myModal").modal('show');

	});

	/** 수정버튼 클릭시 수정폼 출력 */
	$(document)
			.on(
					"click",
					".update_form",
					function() {
						var conText = $(this).parents("div").children().eq(1)
								.html();
						var rep_no = $(this).parents("div").attr("data-num");
						$(this).parents("div").find("input[type='button']")
								.hide();
						var conArea = $(this).parents("div").children().eq(1);

						conArea.html("");
						var data = "<form id='form2' name='form2'  method='post' enctype='multipart/form-data' action ='/reply/update.do'>";
						data += "<input id='rep_no' name='rep_no' type='hidden' value='" + rep_no +"' />";
						data += "<textarea class='form-control' name='rep_content' id='rep_content' rows='3'>"
								+ conText + "</textarea>";
						data += "<input name='file' id='file' type='file' />";
						data += "</form>";
						data += "<p align='right'>";
						data += "<button type='button' class='btn btn-success update_btn'>수정완료</button>";
						data += "<button type='button' class='btn btn-default cancel_btn'>수정취소</button>";
						//data += "</p>기존사진:" + rep_file;
						conArea.html(data);

					});
	$(document).on("click", ".cancel_btn", function() {
		var conText = $(this).parents("div").find("textarea").html();
		$(this).parents("div").find("input[type='button']").show();
		var conArea = $(this).parents("div").children().eq(1);
		conArea.html(conText);
	});
	/* 	글 수정을 위한 에이젝스 연동처리 */
	$(document).on("click", ".update_btn", function() {
		var rep_no = $(this).parents("div").attr("data-num");
		if ($("div[data-num='" + rep_no + "']>p>#form2>input[type='file']").val() == '' || $("div[data-num='" + rep_no + "']>p>#form2>input[type='file']").val() == null) {
			$("div[data-num='" + rep_no + "']>p>#form2>input[type='file']").attr({
				"disabled" : "disabled"
			});
		}
		 $("div[data-num='" + rep_no + "']>p>#form2").ajaxForm({
			dataType : "text",
			success : function(data) {
				$('#comment_list').html(data);
			}
		}).submit();
		location.reload(true); 
	});

	/* 글 삭제를 위한 에이잭스 연동처리 */
	$(document).on("click", ".delete_btn", function() {
		//alert("dd");
		var rep_no = $(this).parents("div").attr("data-num");
		console.log("rep_no" + rep_no);
		//alert(rep_no);
		var q_no = $("#q_no").val();
		//alert(q_no);
		$.ajax({
			url : '/reply/delete' + rep_no + ".do",
			type : 'post',

			dataType : 'text',
			success : function(result) {
				console.log("result: " + result);
				if (result == 'SUCCESS') {
					alert("삭제 되었습니다.");
					location.reload(true);
					//listAll(q_no);
				}
			}
		});
	});

	/* 	업로드 폼          */
	function uploadJqueryForm() {
		if ($("#log_id").val() == null || $("#log_id").val() == '') {
			alert("로그인을 하여야 댓글 작성이 가능합니다.");
			return false;
		}
		if ($("#rep_content").val() == null || $("#rep_content").val() == '') {
			alert("내용을 적어주세요.");
			return false;
		}
		if ($("#form1>input[type='file']").val() == '' || $("#form1>input[type='file']").val() == null) {
			$("#form1>input[type='file']").attr({
				"disabled" : "disabled"
			});
		}
		//$('#comment_list').html('');
		$("#form1").ajaxForm({
			dataType : "text",
			success : function(data) {
				$('#comment_list').html(data);
			}
		}).submit();
		location.reload(true);
	}
</script>

<style>
hr {
	color: black;
}
</style>	<hr />
	<h2>댓글을 달아주세요!</h2>
	<br />
	<form id="form1" method="post" action="/reply/upload.do"
		enctype="multipart/form-data">
		<input type="hidden" id="q_no" name="q_no" value="${detail.q_no }">
		<input type="hidden" id="m_id" name="m_id" value="${log_id }">
		<textarea class="form-control" rows="3" id="rep_content"
			name="rep_content"></textarea>
		<!-- File input -->

		<input name="file" id="file" type="file">

	</form>
	<p align="right">
		<button class="btn btn-primary" value="Submit"
			onclick="uploadJqueryForm()">댓글달기</button>
	</p>
	<hr />

	<div id="best_comment">
		<p>
			<img
				src="http://cdn.playbuzz.com/cdn/f8869f3d-8721-4136-a9b8-c596fed0e515/a2fc82f0-3318-4c35-8afa-c8c89020057b.jpg"
				width="30px" height="30px"> Best_Comment
		</p>
	</div>
	<div id="comment_list">
		<p>
			<img
				src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRfnSUtu7BmV2EBQlPAQZGpaQfgeArpnqsHcyTKjFuEfv199TJI4Q"
				width="30px" height="30px">Comment_List
		</p>
	</div>
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">사진확대</h4>
				</div>
				<div class="modal-body" align="center"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>글 상세보기</title>
<script type="text/javascript"
	src="http://code.jquery.com/jquery-latest.js"></script>

<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- 부가적인 테마 -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var log_id = $("#log_id").val();
		var m_id = $("#m_id").val();
		switch ($('#hiddenInfoGrade').val()) {
		case '1':
			grade = '초등학교 1학년';
			break;
		case '2':
			grade = '초등학교 2학년';
			break;
		case '3':
			grade = '초등학교 3학년';
			break;
		case '4':
			grade = '초등학교 4학년';
			break;
		case '5':
			grade = '초등학교 5학년';
			break;
		case '6':
			grade = '초등학교 6학년';
			break;
		case '7':
			grade = '중학교 1학년';
			break;
		case '8':
			grade = '중학교 2학년';
			break;
		case '9':
			grade = '중학교 3학년';
			break;
		case '10':
			grade = '고등학교 1학년';
			break;
		case '11':
			grade = '고등학교 2학년';
			break;
		case '12':
			grade = '고등학교 3학년';
			break;
		case '13':
			grade = '대학생';
			break;
		case '14':
			grade = '일반';
			break;

		default:
			grade = '기타';
			break;
		}
		switch ($('#hiddenInfoSubject').val()) {
		case '1':
			subject = '수학';
			break;
		case '2':
			subject = '과학';
			break;
		case '3':
			subject = '영어';
			break;
		case '4':
			subject = '국어';
			break;
		case '5':
			subject = '한문';
			break;
		case '6':
			subject = '역사';
			break;
		case '7':
			subject = '경제';
			break;
		case '8':
			subject = '사회';
			break;
		case '9':
			subject = '국가자격시험';
			break;

		default:
			grade = '기타';
			break;
		}
		$('#infoGrade').text(grade);
		$('#infoSubject').text(subject);

		$("#boardUpdateBtn").click(function() {
			$("#detailForm").attr("method", "POST");
			$("#detailForm").attr("action", "/qboard/updateForm.do");
			$("#detailForm").submit();
		});

		$("#boardDeleteBtn").click(function() {
			$("#detailForm").attr("method", "POST");
			$("#detailForm").attr("action", "/qboard/boardDelete.do");
			$("#detailForm").submit();
		});

		$("#boardListBtn").click(function() {
			location.href = "/qboard/boardList.do";

		});

	});
	function reportModalFade() {
		window.open("/report/reportForm.do", "", "width=600,height=400");
	}
	function getLog_id(){
		return $("#log_id").val();
	}	
	function getq_no(){
		return $("#q_no").text();
	}
</script>

<style>
#main, #replySection {
	width: 680px;
	padding: auto;
	margin: auto;
}

#img {
	width: 300px;
	height: 200px;
}

tr {
	border-bottom: 1px solid #BDBDBD;
}
</style>
</head>
<body>

	<jsp:include page="../mainpage/header.jsp" />
	<div id="main">
		<h3>글 상세보기</h3>

		<form id="detailForm" name="detailForm">
			<input type="hidden" id="q_no" name="q_no" value="${detail.q_no}" /><input
				type="hidden" name="q_count" value="${detail.q_count }" /> <input
				type="hidden" id="m_id" name="m_id" value=${detail.m_id } /> <input
				type="hidden" id="hiddenInfoGrade" name="hiddenInfoGrade"
				value="${detail.q_grade }"> <input type="hidden"
				id="hiddenInfoSubject" name="hiddenInfoSubject"
				value="${detail.q_subject }">

			<button type="button" id="boardListBtn" name="boardListBtn"
				class="btn btn-default">목록으로</button>


			<div id="content">
				<table style="width: 680px">
					<tr>
						<td align="center" bgcolor="#F0CA61">글번호</td>
						<td colspan="2" id="q_no">${detail.q_no }</td>
					</tr>
					<tr>
						<td align="center" bgcolor="#F0CA61">작성자</td>
						<td>
						${detail.m_name}(<a href="javascript:otherUserInfo('${detail.m_id}')">${detail.m_id})</a>
						</td>
						<td align="center" bgcolor="#F0CA61">조회수</td>
						<td>${detail.q_count }</td>
					</tr>
					<tr>
						<td align="center" bgcolor="#F0CA61">과목</td>
						<td><label id="infoSubject"></label></td>
						<td align="center" bgcolor="#F0CA61">학년</td>
						<td><label id="infoGrade"></label></td>
					</tr>
					<tr>
						<td align="center" bgcolor="#F0CA61">제목</td>
						<td colspan="3">${detail.q_title }</td>
					</tr>
					<tr>
						<td align="center" bgcolor="#F0CA61">내용</td>
						<td colspan="3">${detail.q_content }</td>
					</tr>
					<tr>
						<td align="center" bgcolor="#F0CA61">첨부파일</td>
						<td colspan="3" align="center">
						<c:if test="${detail.q_file != null && detail.q_file != ''}">
							<img id="img" src="/uploadStorage/${detail.q_file}" />
						</c:if>
						<c:if test="${detail.q_file == null || detail.q_file == ''}">
							첨부파일 없음
						</c:if>
						</td>
					</tr>

				</table>

			</div>

		</form>

		<div class="form-group">
		<c:if test="${log_id == detail.m_id }">
		<button type="button" id="boardUpdateBtn" name="boardUpdateBtn"
				class="btn btn-default">수정하기</button>
			<button type="button" id="boardDeleteBtn" name="boardDeleteBtn"
				class="btn btn-default">삭제하기</button>
		</c:if>
		<c:if test="${log_id != detail.m_id && log_id != null}">
		<!-- 	신고하기버튼       -->
			<button type="button" class="btn btn-danger" onclick="reportModalFade()">신고</button>
		</c:if>
		</div>
	</div>
	<!-- 신고하기 -->
	<div>
		<div class="modal fade" id="reportModal" tabindex="-1" role="dialog"
			aria-labelledby="reportModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<jsp:include page="/report/reportForm.do"></jsp:include>
				</div>
			</div>
		</div>
	</div>


	<!-- 	댓글     -->
	<div id="replySection">
		<jsp:include page="reply.jsp"></jsp:include>
	</div>





	<jsp:include page="../mainpage/footer.jsp" />
</body>
</html>
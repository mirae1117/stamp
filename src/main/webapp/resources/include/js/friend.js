	//친구신청 거절, 취소 동작 메소드
	function unCallFriend(receiver, sender) {
		$.ajax({
			url : "/friend/unCallFriend.do", //전송 url
			type : "post", // 전송 시  method 방식 
			headers : {
				"Content-Type" : "application/json",
				"X-HTTP-Method-Override" : "POST"
			},
			dataType : "text",
			data : JSON.stringify({
				m_receiver : receiver,
				m_sender : sender
			}),
			error : function() { //실행시 오류가 발생하였을 경우 alert('시스템 오류 입니다. 관리자에게 문의   하세요');
			},
			success : function(resultData) {
				if (resultData != null && resultData != '') {
					var msg = "";
					switch (resultData) {
					case 'msg1':
						msg = '친구신청이 취소 됐습니다';
						break;
					default:
						msg = '알수없는 에러. msg = ' + resultData;
						break;
					}
					alert(msg);
				}
			}
		});

	}
	//친구신청 수락 동작 메소드
	function consentCallFriend(receiver, sender) {
		$.ajax({
			url : "/friend/consentCallFriend.do", //전송 url
			type : "post", // 전송 시  method 방식 
			headers : {
				"Content-Type" : "application/json",
				"X-HTTP-Method-Override" : "POST"
			},
			dataType : "text",
			data : JSON.stringify({
				m_receiver : receiver,
				m_sender : sender
			}),
			error : function() { //실행시 오류가 발생하였을 경우 alert('시스템 오류 입니다. 관리자에게 문의   하세요');
			},
			success : function(resultData) {
				if (resultData != null && resultData != '') {
					var msg = "";
					switch (resultData) {
					case 'msg1':
						msg = '친구신청이 수락 됐습니다';
						break;
					default:
						msg = '알수없는 에러. msg = ' + resultData;
						break;
					}
					alert(msg);
				}
			}
		});

	}
	//친구 삭제 메소드
	function deleteFriend(receiver, sender) {
		$.ajax({
			url : "/friend/deleteFriend.do", //전송 url
			type : "post", // 전송 시  method 방식 
			headers : {
				"Content-Type" : "application/json",
				"X-HTTP-Method-Override" : "POST"
			},
			dataType : "text",
			data : JSON.stringify({
				m_receiver : receiver,
				m_sender : sender
			}),
			error : function() { //실행시 오류가 발생하였을 경우 alert('시스템 오류 입니다. 관리자에게 문의   하세요');
			},
			success : function(resultData) {
				if (resultData != null && resultData != '') {
					var msg = "";
					switch (resultData) {
					case 'msg1':
						msg = '친구가 삭제되었습니다.';
						break;
					default:
						msg = '알수없는 에러. msg = ' + resultData;
						break;
					}
					alert(msg);
				}
			}
		});

	}
	//친구 신청 메소드
	function callFriend(receiver,sender) {
		/** 글 저장을 위한 Post    방식의 Ajax 연동 처리 */
		$.ajax({
			url : "/friend/callFriend.do", //전송 url
			type : "post", // 전송 시  method 방식 
			headers : {
				"Content-Type" : "application/json",
				"X-HTTP-Method-Override" : "POST"
			},
			dataType : "text",
			data : JSON.stringify({
				m_receiver : receiver,
				m_sender : sender
			}),
			error : function() { //실행시 오류가 발생하였을 경우 alert('시스템 오류 입니다. 관리자에게 문의   하세요');
			},
			success : function(resultData) {
				if (resultData != null && resultData != '') {
					var msg = "";
					switch (resultData) {
					case 'msg1':
						msg = '친구신청을 완료했습니다.';
						break;
					case 'msg2':
						msg = '추가로 친구신청이 불가능 합니다.';
						break;
					default:
						msg = '알수없는 에러. msg = ' + resultData;
						break;
					}
					alert(msg);
					location.reload(true);
				}
			}
		});
	}
var grade = "시스템에러";
var state1 = "시스템에러";
var infoPrivate = "시스템에러";
var birthDay = new Date($('#hiddenInfoBirth').val());
$(document).ready(
		function() {
			switch ($('#hiddenInfoGrade').val()) {
			case '1':
				grade = '초등학교 1학년';
				break;
			case '2':
				grade = '초등학교 2학년';
				break;
			case '3':
				grade = '초등학교 3학년';
				break;
			case '4':
				grade = '초등학교 4학년';
				break;
			case '5':
				grade = '초등학교 5학년';
				break;
			case '6':
				grade = '초등학교 6학년';
				break;
			case '7':
				grade = '중학교 1학년';
				break;
			case '8':
				grade = '중학교 2학년';
				break;
			case '9':
				grade = '중학교 3학년';
				break;
			case '10':
				grade = '고등학교 1학년';
				break;
			case '11':
				grade = '고등학교 2학년';
				break;
			case '12':
				grade = '고등학교 3학년';
				break;
			case '13':
				grade = '대학생';
				break;
			case '14':
				grade = '일반';
				break;

			default:
				grade = '기타';
				break;
			}
			switch ($('#hiddenInfoState1').val()) {
			case '10':
			case '11':
			case '12':
			case '13':
			case '14':
			case '15':
			case '16':
			case '17':
			case '18':
			case '19':
				state1 = '회원';
				break;
			case '20':
			case '21':
			case '22':
			case '23':
			case '24':
			case '25':
			case '26':
			case '27':
			case '28':
			case '29':
				state1 = '관리자';
				break;

			default:
				state1 = '기타';
				break;
			}
			switch ($('#hiddenInfoPrivate').val()) {
			case '1':
				infoPrivate = '공개';
				break;
			case '0':
				infoPrivate = '비공개';
				break;
			default:
				infoPrivate = '시스템에러';
				break;
			}
			$('#infoGrade').text(grade);
			$('#infoState1').text(state1);
			$('#infoPrivate').text(infoPrivate);
			$('#infoBirth')
					.text(
							new Date($('#hiddenInfoBirth').val()).getFullYear()
									+ "년 "
									+ (new Date($('#hiddenInfoBirth').val())
											.getMonth() + 1)
									+ "월 "
									+ new Date($('#hiddenInfoBirth').val())
											.getDate() + "일");
			var theday = new Date($('#hiddenInfoJoindate').val());
			$('#joinDay').text(
					new Date($('#hiddenInfoJoindate').val()).getFullYear()
							+ "년 "
							+ (new Date($('#hiddenInfoJoindate').val())
									.getMonth() + 1)
							+ "월 "
							+ new Date($('#hiddenInfoJoindate').val())
									.getDate() + "일");
			$('#lastAttendDay').text(
					new Date($('#hiddenlastAttendDay').val()).getFullYear()
							+ "년 "
							+ (new Date($('#hiddenlastAttendDay').val())
									.getMonth() + 1)
							+ "월 "
							+ new Date($('#hiddenlastAttendDay').val())
									.getDate() + "일");
			if ($('#hiddenlastAttendDay').val() == '' || $('#hiddenlastAttendDay').val() == null) {
				$('#lastAttendDay').text("로그인을 하지 않은 회원입니다.");
			}

		});
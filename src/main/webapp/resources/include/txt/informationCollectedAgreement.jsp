<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style type="text/css">

h5{padding-left: 5px;
    color: #323232;
    font-size: 16px;
    font-weight: 600;
}
p{
font-size: 15px;
padding: 3px 0 0 20px;

}


</style>


<h5>※수집 개인정보의 항목</h5>
<p style="margin-top:10px;">그루지기의 운영을 위해 다음과 같은 개인정보를 수집, 이용하고 있습니다.</p>
<p>회원 탈퇴 시 소비자의 불만 및 분쟁해결 등을 위한 목적으로 30일 보관 후 파기합니다.</p>
<p>또한 대회 종료 후 수상자 관리 등 대회 운영 및 결과 처리를 위해 3개월 보관 후 파기합니다.</p>
<p>- 목적: 회원가입 및 서비스 이용에 따른 이용자 식별, 대회 관련 정보 안내</p>
<p>- 항목: (필수)ID, 비밀번호, 이메일주소</p>
<p>- 목적: 본인확인기관을 통한 본인인증 및 중복가입 여부 확인</p>
<p>- 항목: (필수)중복가입정보(DI)</p>
<p>필수 정보 수집에 대한 동의를 거부할 권리가 있습니다.</p>
<p>단, 동의를 거부할 경우 회원 가입 및 NYPC 참여에 제한이 있습니다.</p>
<p>그 밖의 사항은 개인정보 처리방침을 준수합니다.</p>
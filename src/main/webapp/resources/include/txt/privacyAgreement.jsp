<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
.bbsList.type2 {
    margin-top: 0;
    border: 1px solid #ececec;
    border-top: 0;
    }
    .bbsList {
    width: 100%;
    margin-top: 20px;
    table-layout: fixed;
}
thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
}
tr {
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
}
.bbsList.type2 thead th {
    border-left: 1px solid #fff;
    border-top: 1px solid #fff;
    background-image: none;
}
.bbsList thead th {
    height: 43px;
    text-align: center;
    font-size: 14px;
    font-family: nGothicB;
    font-weight: bold;
    background: #ececec;
    }
    tbody {
    display: table-row-group;
    vertical-align: middle;
    border-color: inherit;
}
.bbsList tbody td {
    padding: 12px 0 11px;
    border-bottom: 1px solid #ececec;
    text-align: center;
    color: #727272;
    line-height: 20px;
    font-family: nGothic;
    font-weight: normal;
}
</style>


<table class="bbsList type2">
	<colgroup>
	<col width="50%">
	<col width="50%">
	</colgroup>
	<thead>
		<tr>
			<th scope="col">개인정보 취급위탁을 받는 자</th>
			<th scope="col">개인정보 취급위탁을 하는 업무의 내용</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>르노삼성자동차 지점/대리점</td>
			<td>고객이 선택한 지점 시승예약 서비스 제공</td>
		</tr>
		<tr>
			<td>㈜이퓨전아이</td>
			<td>홈페이지 운영 및 유지보수</td>
		</tr>
		<tr>
			<td>㈜아림디엠</td>
			<td>우편물 및 경품 배송업무 대행</td>
		</tr>
		<tr>
			<td>Trans Cosmos Korea</td>
			<td>차량구매 및 정비서비스 관련 고객 상담 업무</td>
		</tr>
		<tr>
			<td>㈜삼성 SDS</td>
			<td>전산시스템 운영 및 유지보수</td>
		</tr>
		<tr>
			<td>㈜다우기술</td>
			<td>SMS 발송 업무 대행</td>
		</tr>
		<tr>
			<td>딜로이트컨설팅</td>
			<td>고객정보분석 및 프로모션 이벤트 업무대행</td>
		</tr>
	</tbody>
</table>
package com.stamp.common;

import com.stamp.vo.NoticeVO;

public class NoticePaging {
	public static void set(NoticeVO nvo) {
		int page = Util.nvl(nvo.getPage(), 1);
		int pageSize = Util.nvl(nvo.getPageSize(), 10);
		
		if(nvo.getPage()==null) nvo.setPage(page+"");
		if(nvo.getPageSize()==null) nvo.setPageSize(pageSize+"");
		
		int start_row = (page-1)*pageSize + 1;
		int end_row = (page-1)*pageSize + pageSize;
		
		nvo.setStart_row(start_row+"");
		nvo.setEnd_row(end_row+"");
	}
}

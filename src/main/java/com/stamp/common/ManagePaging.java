package com.stamp.common;

import com.stamp.vo.MemberVO;

public class ManagePaging {
	public static void set(MemberVO mvo) {
		int page = Util.nvl(mvo.getPage(), 1);
		int pageSize = Util.nvl(mvo.getPageSize(), 30);
		
		if(mvo.getPage()==null) mvo.setPage(page + "");
		if(mvo.getPageSize()==null) mvo.setPageSize(pageSize + "");
		
		int start_row = (page-1)*pageSize+1;
		int end_row = (page-1)*pageSize+pageSize;
		
		mvo.setStart_row(start_row + "");
		mvo.setEnd_row(end_row + "");
	}
}

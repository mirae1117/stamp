package com.stamp.common;

import com.stamp.vo.QuestionVO;

public class QuestionPaging {
	public static void set(QuestionVO qvo) {
		int page = Util.nvl(qvo.getPage(), 1);
		int pageSize = Util.nvl(qvo.getPageSize(), 30);
		
		if(qvo.getPage()==null) qvo.setPage(page + "");
		if(qvo.getPageSize()==null) qvo.setPageSize(pageSize + "");
		
		int start_row = (page-1)*pageSize+1;
		int end_row = (page-1)*pageSize+pageSize;
		
		qvo.setStart_row(start_row + "");
		qvo.setEnd_row(end_row + "");
	}
}

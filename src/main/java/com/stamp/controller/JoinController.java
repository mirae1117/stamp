package com.stamp.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.stamp.service.EmailSendUtil;
import com.stamp.service.FileUploadUtil;
import com.stamp.service.JoinService;
import com.stamp.service.PointService;
import com.stamp.vo.GuardianVO;
import com.stamp.vo.MemberVO;

@Controller
@RequestMapping(value = "/join")
public class JoinController {
	Logger logger = Logger.getLogger(JoinController.class);

	@Autowired
	private JoinService joinService;
	
	@Autowired
	private PointService pointService;
	
	
	// 아이디/비밀번호 찾기 눌렀을때
	@RequestMapping(value = "/findIdPassword", method = RequestMethod.GET)
	public String findIdPassword(HttpSession session) {
		logger.info("findIdPassword 호출 성공");
		return "/join/findIdPassword";
	}
	// 아이디/비밀번호 찾기 찾기버튼 눌렀을시
	@RequestMapping(value = "/findIdPassword", method = RequestMethod.POST)
	public String findIdPasswordPro(@ModelAttribute MemberVO mvo,  RedirectAttributes rttr, Model model) throws IllegalStateException, IOException {
		logger.info("findIdPassword(POST) 호출 성공");
		//아이디 찾기 일때
		if (mvo.getM_id() == null || mvo.getM_id() == "") {
			List<String> findIds = joinService.idFind(mvo.getM_email());
			String findIdString = "";
			for (int i = 0; i < findIds.size(); i++) {
				findIdString += findIds.get(i) + "<br />";
			}

			String emailSubject = "그루터기 아이디 찾기 안내문";
			String emailBody = "[그루터기 아이디 찾기 안내문 내용]<br />아이디 목록<br />" + findIdString;
			EmailSendUtil.emailSend(mvo.getM_email(), emailSubject, emailBody);
			rttr.addFlashAttribute("msg", "찾은 아이디 들은 입력하신 이메일 " + mvo.getM_email() + "로 보냈습니다.");
			return "redirect:/join/findIdPassword.do";
		}
		//비밀번호 찾기 일때 이메일로 인증번호 전송하고 찾기페이지 이동
		else {
			//아이디와 이메일 일치하지 않을때
			if (joinService.pwFindIdEmailChk(mvo) != 1) {
				rttr.addFlashAttribute("msg", "아이디와 이메일이 일치하지 않습니다.");
				return "redirect:/join/findIdPassword.do";
				
			}else {
				GuardianVO gvo = new GuardianVO();
				gvo.setG_email(mvo.getM_email());
				gvo.setG_key(joinService.outputKey(5));
				gvo.setM_id(mvo.getM_id());
				if (joinService.pwFindAgreeInsert(gvo) == 1) {
					String emailSubject = "그루터기 비밀번호 찾기 안내문";
					String emailBody = "[그루터기 비밀번호 찾기 안내문 내용]<br />인증키<br />" + gvo.getG_key();
					EmailSendUtil.emailSend(mvo.getM_email(), emailSubject, emailBody);
					rttr.addFlashAttribute("msg", "인증키를 이메일 " + mvo.getM_email() + "로 보냈습니다.");
					
					model.addAttribute("findId_email", gvo.getG_email());
					return "/join/findPassword";
				}else {
					rttr.addFlashAttribute("msg", "알수없는 에러 관리자에게 문의하세요.");
					return "redirect:/join/findIdPassword.do";
				}
			}
		}
	}	
	// 비밀번호 찾기 인증 작업
	@RequestMapping(value = "/confirmPw", method = RequestMethod.POST)
	public String confirmPw(@ModelAttribute GuardianVO gvo, Model model, RedirectAttributes rttr) throws IllegalStateException, IOException {
		logger.info("confirmPw(POST) 호출 성공");
		gvo.setM_id(joinService.pwFindGetId(gvo));
		if (joinService.pwFindCertify(gvo) == 1) {
			model.addAttribute("findPwChangeId", gvo.getM_id());
			return "/join/changePassword";
		} else {
			rttr.addFlashAttribute("msg", "인증번호가 틀렸습니다.");
			model.addAttribute("findId_email", gvo.getG_email());
			return "/join/findPassword";
		}
	}	
	// 분실 비밀번호 변경 최종단계	
	@RequestMapping(value = "/pwFindChange", method = RequestMethod.POST)
	public String pwFindChange(@ModelAttribute MemberVO mvo, HttpServletRequest request, RedirectAttributes rttr)
			throws IllegalStateException, IOException {
		logger.info("pwFindChange(POST) 호출 성공");
		int result = 0;
		result = joinService.pwFindChange(mvo);
		if (result == 1) {
			request.setAttribute("msg", "비밀번호 변경이 완료 되었습니다.");
			return "/sub/close";
		}

		request.setAttribute("msg", "알수없는 오류가 생겼습니다 다시 시도해 주세요.");
		return "/sub/close";
	}
	
		
	// 회원가입 눌렀을때
	@RequestMapping(value = "/joinAge", method = RequestMethod.GET)
	public String joinAge(HttpSession session) {
		logger.info("joinAge 호출 성공");
		return "/join/joinAge";
	}
	
	// 어린이회원 가입 눌렀을때
	@RequestMapping(value = "/joinPaAgree", method = RequestMethod.GET)
	public String joinPaAgree(HttpSession session) {
		logger.info("joinPaAgree 호출 성공");
		return "/join/joinPaAgree";
	}

	// 일반 회원 가입 눌렀을때
	@RequestMapping(value = "/joinAgree", method = RequestMethod.GET)
	public String joinAgree(HttpSession session) {
		logger.info("joinAgree(Get) 호출 성공");
		return "/join/joinAgree";
	}

	// 어린이 회원 약관 동의시
	@RequestMapping(value = "/joinPaAgree", method = RequestMethod.POST)
	public String joinPaAgree(@ModelAttribute GuardianVO gvo, RedirectAttributes rttr) throws IllegalStateException, IOException {
		logger.info("joinPaAgree(POST) 호출 성공");
		// 인증키 생성
		gvo.setG_key(joinService.outputKey(5));
		int result = 0;
		result = joinService.guardianAttestation(gvo);
		String emailSubject = "그루터기 어린이 회원가입 보호자 동의 안내문";
		String emailBody = "[법정대리인 인증 약관 내용]<br />보호자 동의 하시겠습니까?<br />인증키<br />" + gvo.getG_key();
		EmailSendUtil.emailSend(gvo.getG_email(), emailSubject, emailBody);
		String url = "";
		if (result == 1) {
			url = "joinPaGuard";
			rttr.addFlashAttribute("guardian", gvo);
		}
		
		return "redirect:/join/" + url + ".do";
	}
	// 일반 회원 가입 눌렀을때
	@RequestMapping(value = "/joinPaGuard", method = RequestMethod.GET)
	public String joinPaGuard(HttpSession session, Model model, RedirectAttributes rttr) {
		logger.info("joinPaGuard(Get) 호출 성공");
		return "/join/joinPaGuard";
	}
	// 어린이 회원 법정대리인 인증 작업
	@RequestMapping(value = "/joinPaCertify", method = RequestMethod.POST)
	public String joinPaCertify(HttpSession session, @ModelAttribute GuardianVO gvo, Model model, RedirectAttributes rttr) throws IllegalStateException, IOException {
		logger.info("joinPaCertify(POST) 호출 성공");
		String url = "";
		if (joinService.guardianCertify(gvo) == 1) {
			url = "joinPaForm";
			session.setAttribute("guardian", gvo);
		} else {
			url = "joinPaAgree";
		}
		return "redirect:/join/" + url + ".do";
	}
	// 어린이 회원 가입폼
	@RequestMapping(value = "/joinPaForm", method = RequestMethod.GET)
	public String joinPaForm(HttpSession session, @ModelAttribute GuardianVO gvo, Model model, RedirectAttributes rttr) {
		logger.info("joinPaForm(Get) 호출 성공");
		return "/join/joinPaForm";
	}
	// 일반 회원 약관 동의시
	@RequestMapping(value = "/joinForm", method = RequestMethod.GET)
	public String joinForm(@ModelAttribute GuardianVO gvo, HttpServletRequest request)
			throws IllegalStateException, IOException {
		logger.info("joinForm(GET) 호출 성공");
		return "/join/joinForm";
	}
	// 아이디 중복 확인 (받은 아이디의 숫자만큼 리턴 (예 있음1 없음0))
	@RequestMapping(value = "/memberChk/{m_id}.do", method = RequestMethod.GET)
	public ResponseEntity<Integer> memberChk(@PathVariable("m_id") String m_id) {
		ResponseEntity<Integer> entity = null;
		entity = new ResponseEntity<Integer>(joinService.memberChk(m_id), HttpStatus.OK);
		//joinService.memberChk(m_id)
		return entity;
	}
	// 일반 회원 가입
	@RequestMapping(value = "/joinPro", method = RequestMethod.POST)
	public String joinPro(@ModelAttribute MemberVO mvo, HttpServletRequest request)
			throws IllegalStateException, IOException {
		logger.info("joinPro(POST) 호출 성공");
		int result = 0;
		String url = "";
		String m_profile = FileUploadUtil.fileUpload(mvo.getFile(), request);
		mvo.setM_profile(m_profile);
		result = joinService.joinMember(mvo);
		if (result == 1) {
			//화원가입 축하 포인트 지급
			result = pointService.insertJoinPoint(mvo.getM_id());
			if (result == 1) {
				url = "redirect:/main/main.do";
			}
		}
		return url;
	}

	// 어린이 회원 가입
	@RequestMapping(value = "/joinPaPro", method = RequestMethod.POST)
	public String joinPaPro(HttpSession session, @ModelAttribute MemberVO mvo, @ModelAttribute GuardianVO gvo, HttpServletRequest request)
			throws IllegalStateException, IOException {
		logger.info("joinPro(POST) 호출 성공");
		int result1 = 0;
		int result2 = 0;
		String url = "";
		String m_profile = FileUploadUtil.fileUpload(mvo.getFile(), request);
		mvo.setM_profile(m_profile);
		result1 = joinService.joinMember(mvo);
		result2 = joinService.saveGuardian(gvo);
		if (result1 == 1 && result2 == 1) {
			//화원가입 축하 포인트 지급
			result1 = pointService.insertJoinPoint(mvo.getM_id());
			if (result1 == 1) {
				url = "redirect:/main/main.do";
			}
		}
		session.removeAttribute("guardian");
		return url;
	}

}

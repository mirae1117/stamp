package com.stamp.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stamp.common.NoticePaging;
import com.stamp.common.Util;
import com.stamp.service.AdminNoticeService;
import com.stamp.vo.NoticeVO;

@Controller
@RequestMapping(value="/admin/notice")
public class AdminNoticeController {
	Logger logger = Logger.getLogger(AdminNoticeController.class);
	
	@Autowired
	private AdminNoticeService noticeService;
	
	//글 목록 구현
	@RequestMapping(value="/noticeList")
	public String noticeList(@ModelAttribute NoticeVO nvo, Model model) {
		logger.info("noticeList 호출 성공");
		
		//페이지 세팅
		NoticePaging.set(nvo);
		
		//전체 레코드 수 구하기
		int total = noticeService.noticeListCnt(nvo);
		logger.info("total = " + total);
		
		int count = total - (Util.nvl(nvo.getPage())-1) * Util.nvl(nvo.getPageSize());
		logger.info("count = " + count);
		
		List<NoticeVO> noticeList = noticeService.noticeList(nvo);
		model.addAttribute("noticeList", noticeList);
		model.addAttribute("count", count);
		model.addAttribute("data", nvo);
		model.addAttribute("total", total);
		
		return "admin/notice/noticeList";
	}
	
	//글 쓰기 폼 출력
	@RequestMapping(value="/writeForm", method=RequestMethod.GET)
	public String writeForm() {
		logger.info("writeForm 호출 성공");
		
		return "admin/notice/writeForm";
	}
	
	//글 쓰기 구현
	@RequestMapping(value="/noticeInsert", method=RequestMethod.POST)
	public String noticeInsert(@ModelAttribute NoticeVO nvo) {
		logger.info("noticeInsert 호출 성공");
		
		int result = 0;
		String url = "";
		
		System.out.println("ID : " + nvo.getM_id());
		result = noticeService.noticeInsert(nvo);
		if(result == 1) {
			url = "/admin/notice/noticeList.do";
		}
		
		return "redirect:" + url;
	}
	
	//글 상세 보기 구현
	@RequestMapping(value="/noticeDetail", method=RequestMethod.GET)
	public String noticeDetail(@ModelAttribute NoticeVO nvo, Model model) {
		logger.info("noticeDetail 호출 성공");
		
		logger.info("nt_no = " + nvo.getNt_no());
		
		noticeService.countUpdate(nvo.getNt_no());
		NoticeVO detail = new NoticeVO();
		detail = noticeService.noticeDetail(nvo);
		
		if(detail != null && (!detail.equals(""))) {
			detail.setNt_content(detail.getNt_content().toString().replaceAll("\n", "<br>"));
		}
		
		model.addAttribute("detail", detail);
		
		return "admin/notice/noticeDetail";
	}
	
	//글 수정 폼 구현
	@RequestMapping(value="/updateForm", method=RequestMethod.POST)
	public String udpateForm(@ModelAttribute NoticeVO nvo, Model model) {
		logger.info("updateForm 호출 성공");
		
		logger.info("nt_no = " + nvo.getNt_no());
		
		NoticeVO updateData = new NoticeVO();
		updateData = noticeService.noticeDetail(nvo);
		
		model.addAttribute("updateData", updateData);
		return "admin/notice/updateForm";
	}
	
	//글 수정 구현
	@RequestMapping(value="noticeUpdate", method=RequestMethod.POST)
	public String noticeUpdate(@ModelAttribute NoticeVO nvo) {
		logger.info("noticeUpdate 호출 성공");
		
		int result = 0;
		String url = "";
		
		result = noticeService.noticeUpdate(nvo);
		
		if(result == 1) {
			url="/admin/notice/noticeList.do";	//수정 후 목록이로 이동
		}
		return "redirect:" + url;
	}
	
	//글 삭제 구현
	@RequestMapping(value="noticeDelete")
	public String noticeDelete(@ModelAttribute NoticeVO nvo) {
		logger.info("noticeDelete 호출 성공");
		
		int result = 0;
		String url = "";
		
		result = noticeService.noticeDelete(nvo.getNt_no());
		if(result == 1) {
			url="/admin/notice/noticeList.do";
		}
		return "redirect:" + url;
	}
}

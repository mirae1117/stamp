package com.stamp.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stamp.service.QuestionService;
import com.stamp.service.ReportService;
import com.stamp.vo.QuestionVO;
import com.stamp.vo.ReportVO;

@Controller
@RequestMapping(value = "/report")
public class ReportController {

	Logger logger = Logger.getLogger(ReportController.class);

	@Autowired
	private QuestionService questionService;

	@Autowired
	private ReportService reportService;

	@RequestMapping(value = "/reportForm", method = RequestMethod.GET)
	public String reportForm(@ModelAttribute QuestionVO qvo, Model model, HttpSession session) {
		logger.info("reportForm 호출");
		
		QuestionVO reportData = new QuestionVO();
		reportData = questionService.boardDetail(qvo);
		model.addAttribute("reportData", reportData);
		session.getAttribute("log_id");
		return "qboard/reportForm";
	}

	@RequestMapping(value = "/reportInsert", method = RequestMethod.POST)
	public String reportInsert(@ModelAttribute QuestionVO qvo, @ModelAttribute ReportVO rvo, HttpSession session)
			throws IllegalStateException, IOException {
		logger.info("reportInsert 호출");

		rvo.setRe_sort(1);
		String re_suspect=rvo.getRe_suspect();
		int reportCount = 0;
		logger.info("re_suspect : " + re_suspect);
		reportService.reportInsert(rvo);
		
		//리포트 카운트
		reportCount = reportService.countReport(rvo);
		if(reportCount >=3){
			logger.info("reportUpdate 호출  "+rvo.getRe_suspect());
			reportService.reportUpdate(rvo.getRe_suspect());
			
		}
		
		logger.info("re_suspect : " + rvo.getRe_suspect());
		logger.info("reportCount : " + reportCount);
		
		return "/sub/close";
	}
	
	
	
	
	
}

package com.stamp.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.stamp.vo.PointVO;
import com.stamp.vo.ReplyVO;
import com.stamp.service.FileUploadUtil;
import com.stamp.service.PointService;
import com.stamp.service.ReplyService;

@RestController
@RequestMapping(value = "/reply")
public class ReplyController {
	Logger logger = Logger.getLogger(ReplyController.class);

	@Autowired
	private ReplyService replyService;

	@Autowired
	private PointService pointService;
	// 댓글불러오기
	@RequestMapping(value = "/all/{q_no}.do", method = RequestMethod.GET)
	public ResponseEntity<List<ReplyVO>> list(@PathVariable("q_no") int q_no) {
		ResponseEntity<List<ReplyVO>> entity = null;
		System.out.println("댓글 q_no : " + q_no);
		try {
			entity = new ResponseEntity<List<ReplyVO>>(replyService.replyList(q_no), HttpStatus.OK);

		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<List<ReplyVO>>(HttpStatus.BAD_REQUEST);
		}
		return entity;
	}

	// 댓글업로드
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody void upload(MultipartHttpServletRequest request, HttpServletResponse response,
			@ModelAttribute ReplyVO rvo) throws IOException {
		logger.info("upload 호출");
		if (rvo.getFile() != null) {
			String rep_file;
			rep_file = FileUploadUtil.fileUpload(rvo.getFile(), request);
			rvo.setRep_file(rep_file);
		}
		replyService.replyInsert(rvo);

	}

	// 댓글채택
	@RequestMapping(value = "/select{rep_no}.do", method = RequestMethod.POST)
	public ResponseEntity<String> replySelect(@PathVariable("rep_no") int rep_no, HttpServletRequest request) {
		logger.info("replySelect 호출 성공 : " + rep_no);
		ResponseEntity<String> entity = null;
		int port = request.getServerPort();
		String domain = "http://" + request.getServerName();
		if (port != 80 && port != 0) {
			domain += ":" + port;
		}
		domain += "/";
		try {
			replyService.replySelect(rep_no);
			PointVO pvo = new PointVO();
			ReplyVO rvo = replyService.getReply(rep_no);
			pvo.setM_id(rvo.getM_id());
			pvo.setP_detail(domain + "qboard/boardDetail.do?q_no=" + rvo.getQ_no()+"");
			int result = pointService.insertReplyPoint(pvo);
			if (result == 1) {
				entity = new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
			}else {
				entity = new ResponseEntity<String>("empty", HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}

	//댓글 수정
	@RequestMapping(value = "/update.do", method = RequestMethod.POST)
	public void replyUpdate(HttpSession session, RedirectAttributes rttr, Model model, HttpServletRequest request,
			@ModelAttribute ReplyVO rvo)  throws IOException {
		logger.info("replyUpdate 호출 성공");
		System.out.println("댓글 번호 : " + rvo.getRep_no());
		if (rvo.getFile() != null) {
			String rep_file;
			rep_file = FileUploadUtil.fileUpload(rvo.getFile(), request);
			rvo.setRep_file(rep_file);
		}
		replyService.replyUpdate(rvo);
	}
	// 댓글삭제
	@RequestMapping(value = "/delete{rep_no}.do", method = RequestMethod.POST)
	public ResponseEntity<String> replyDelete(@PathVariable("rep_no") int rep_no) {
		logger.info("replyDelete 호출 성공" + rep_no);
		ResponseEntity<String> entity = null;
		try {
			replyService.replyDelete(rep_no);
			entity = new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}

}

package com.stamp.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/main")
public class MainController {
	Logger logger = Logger.getLogger(MainController.class);
	
	// 메인을 눌렀을때
	@RequestMapping(value = "/main", method = RequestMethod.GET)
	public String mainpage(HttpSession session) {
		logger.info("main 호출 성공");
		return "mainpage/main";
	}
	
	@RequestMapping(value = "/companyIntro", method = RequestMethod.GET)
	public String companyIntro(HttpSession session) {
		logger.info("companyIntro 호출 성공");
		return "/intro/companyIntro";
	}
	
	@RequestMapping(value = "/companyMap", method = RequestMethod.GET)
	public String companyMap(HttpSession session) {
		logger.info("companyIntro 호출 성공");
		return "/intro/companyMap";
	}
	@RequestMapping(value = "/notice", method = RequestMethod.GET)
	public String notice(HttpSession session) {
		logger.info("notice 호출 성공");
		return "/notice/noticeList";
	}

}

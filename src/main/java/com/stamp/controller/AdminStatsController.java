package com.stamp.controller;

import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.stamp.service.AdminManageService;
import com.stamp.service.PointService;
import com.stamp.service.QuestionService;
import com.stamp.vo.MemberVO;
import com.stamp.vo.PointVO;
import com.stamp.vo.QuestionVO;

@RestController
public class AdminStatsController {

	@Autowired
	private AdminManageService manageService;

	@Autowired
	private QuestionService questionService;

	@Autowired
	private PointService pointService;

	@RequestMapping(value = "admin/stats/memberStats.do")
	public ModelAndView memberStats() {
		return new ModelAndView("admin/stats/memberStats");
	}

	@RequestMapping(value = "/stats/memberJoinChart.do")
	public JSONObject memberJoinChart() {
		List<MemberVO> items = manageService.memberJoinChart();
		// 리스트 데이터를 json 형식으로 만들어서 리턴
		JSONObject data = new JSONObject();
		// 컬럼 객체
		JSONObject col1 = new JSONObject();
		JSONObject col2 = new JSONObject();
		JSONArray title = new JSONArray();
		col1.put("label", "월별");
		col1.put("type", "string");
		col2.put("label", "가입자수");
		col2.put("type", "number");
		title.add(col1);
		title.add(col2);
		data.put("cols", title);

		JSONArray body = new JSONArray();
		MemberVO mvo = new MemberVO();
		for (int i = 0; i < items.size(); i++) {
			JSONObject name = new JSONObject();
			name.put("v", items.get(i).getM_state1() + "월");
			JSONObject val = new JSONObject();
			val.put("v", items.get(i).getM_state2());
			JSONArray row = new JSONArray();
			row.add(name);
			row.add(val);
			JSONObject cell = new JSONObject();
			cell.put("c", row);
			body.add(cell);
		}
		data.put("rows", body);
		return data;
	}

	@RequestMapping(value = "/stats/memberGenderChart.do")
	public JSONObject memberGenderChart() {
		List<MemberVO> items = manageService.memberGenderChart();
		// 리스트 데이터를 json 형식으로 만들어서 리턴
		JSONObject data = new JSONObject();
		// 컬럼 객체
		JSONObject col1 = new JSONObject();
		JSONObject col2 = new JSONObject();
		JSONArray title = new JSONArray();
		col1.put("label", "남녀별");
		col1.put("type", "string");
		col2.put("label", "가입자수");
		col2.put("type", "number");
		title.add(col1);
		title.add(col2);
		data.put("cols", title);

		JSONArray body = new JSONArray();
		MemberVO mvo = new MemberVO();
		for (int i = 0; i < items.size(); i++) {
			JSONObject name = new JSONObject();
			name.put("v", items.get(i).getM_sex());
			JSONObject val = new JSONObject();
			val.put("v", items.get(i).getM_state2());
			JSONArray row = new JSONArray();
			row.add(name);
			row.add(val);
			JSONObject cell = new JSONObject();
			cell.put("c", row);
			body.add(cell);
		}
		data.put("rows", body);
		return data;
	}

	@RequestMapping(value = "/stats/memberGradeChart.do")
	public JSONObject memberGradeChart() {
		List<MemberVO> items = manageService.memberGradeChart();
		// 리스트 데이터를 json 형식으로 만들어서 리턴
		JSONObject data = new JSONObject();
		// 컬럼 객체
		JSONObject col1 = new JSONObject();
		JSONObject col2 = new JSONObject();
		JSONArray title = new JSONArray();
		col1.put("label", "학년별");
		col1.put("type", "string");
		col2.put("label", "가입자수");
		col2.put("type", "number");
		title.add(col1);
		title.add(col2);
		data.put("cols", title);

		JSONArray body = new JSONArray();
		MemberVO mvo = new MemberVO();
		for (int i = 0; i < items.size(); i++) {
			JSONObject name = new JSONObject();
			switch (items.get(i).getM_grade()) {
			case 1:
				name.put("v", "초등학교 1학년");
				break;
			case 2:
				name.put("v", "초등학교 2학년");
				break;
			case 3:
				name.put("v", "초등학교 3학년");
				break;
			case 4:
				name.put("v", "초등학교 4학년");
				break;
			case 5:
				name.put("v", "초등학교 5학년");
				break;
			case 6:
				name.put("v", "초등학교 6학년");
				break;
			case 7:
				name.put("v", "중학교 1학년");
				break;
			case 8:
				name.put("v", "중학교 2학년");
				break;
			case 9:
				name.put("v", "중학교 3학년");
				break;
			case 10:
				name.put("v", "고등학교 1학년");
				break;
			case 11:
				name.put("v", "고등학교 2학년");
				break;
			case 12:
				name.put("v", "고등학교 3학년");
				break;
			case 13:
				name.put("v", "대학생");
				break;
			case 14:
				name.put("v", "일반");
				break;

			default:
				name.put("v", "기타");
				break;
			}
			JSONObject val = new JSONObject();
			val.put("v", items.get(i).getM_state2());
			JSONArray row = new JSONArray();
			row.add(name);
			row.add(val);
			JSONObject cell = new JSONObject();
			cell.put("c", row);
			body.add(cell);
		}
		data.put("rows", body);
		return data;
	}

	@RequestMapping(value = "/stats/questionChart.do")
	public JSONObject questionChart() {
		List<QuestionVO> items = questionService.questionChart();
		// 리스트 데이터를 json 형식으로 만들어서 리턴
		JSONObject data = new JSONObject();
		// 컬럼 객체
		JSONObject col1 = new JSONObject();
		JSONObject col2 = new JSONObject();
		JSONArray title = new JSONArray();
		col1.put("label", "월별");
		col1.put("type", "string");
		col2.put("label", "게시글수");
		col2.put("type", "number");
		title.add(col1);
		title.add(col2);
		data.put("cols", title);

		JSONArray body = new JSONArray();
		QuestionVO qvo = new QuestionVO();
		for (int i = 0; i < items.size(); i++) {
			JSONObject name = new JSONObject();
			name.put("v", items.get(i).getQ_state() + "월");
			JSONObject val = new JSONObject();
			val.put("v", items.get(i).getQ_count());
			JSONArray row = new JSONArray();
			row.add(name);
			row.add(val);
			JSONObject cell = new JSONObject();
			cell.put("c", row);
			body.add(cell);
		}
		data.put("rows", body);
		return data;
	}

	@RequestMapping(value = "/stats/questionSubjectChart.do")
	public JSONObject questionSubjectChart() {
		List<QuestionVO> items = questionService.questionSubjectChart();
		// 리스트 데이터를 json 형식으로 만들어서 리턴
		JSONObject data = new JSONObject();
		// 컬럼 객체
		JSONObject col1 = new JSONObject();
		JSONObject col2 = new JSONObject();
		JSONArray title = new JSONArray();
		col1.put("label", "과목별");
		col1.put("type", "string");
		col2.put("label", "게시글수");
		col2.put("type", "number");
		title.add(col1);
		title.add(col2);
		data.put("cols", title);

		JSONArray body = new JSONArray();
		QuestionVO qvo = new QuestionVO();
		for (int i = 0; i < items.size(); i++) {
			JSONObject name = new JSONObject();
			switch (items.get(i).getQ_subject()) {
			case 1:
				name.put("v", "수학");
				break;
			case 2:
				name.put("v", "과학");
				break;
			case 3:
				name.put("v", "영어");
				break;
			case 4:
				name.put("v", "국어");
				break;
			case 5:
				name.put("v", "한문");
				break;
			case 6:
				name.put("v", "역사");
				break;
			case 7:
				name.put("v", "경제");
				break;
			case 8:
				name.put("v", "사회");
				break;
			case 9:
				name.put("v", "국가자격시험");
				break;
			default:
				name.put("v", "기타");
				break;
			}
			JSONObject val = new JSONObject();
			val.put("v", items.get(i).getQ_count());
			JSONArray row = new JSONArray();
			row.add(name);
			row.add(val);
			JSONObject cell = new JSONObject();
			cell.put("c", row);
			body.add(cell);
		}
		data.put("rows", body);
		return data;
	}

	@RequestMapping(value = "/stats/pointChart.do")
	public JSONObject pointChart() {
		List<PointVO> items = pointService.pointChart();
		// 리스트 데이터를 json 형식으로 만들어서 리턴
		JSONObject data = new JSONObject();
		// 컬럼 객체
		JSONObject col1 = new JSONObject();
		JSONObject col2 = new JSONObject();
		JSONArray title = new JSONArray();
		col1.put("label", "월별");
		col1.put("type", "string");
		col2.put("label", "포인트 충전 금액");
		col2.put("type", "number");
		title.add(col1);
		title.add(col2);
		data.put("cols", title);

		JSONArray body = new JSONArray();
		PointVO pvo = new PointVO();
		for (int i = 0; i < items.size(); i++) {
			JSONObject name = new JSONObject();
			name.put("v", items.get(i).getP_no() + "월");
			JSONObject val = new JSONObject();
			val.put("v", items.get(i).getP_point());
			JSONArray row = new JSONArray();
			row.add(name);
			row.add(val);
			JSONObject cell = new JSONObject();
			cell.put("c", row);
			body.add(cell);
		}
		data.put("rows", body);
		return data;
	}
}

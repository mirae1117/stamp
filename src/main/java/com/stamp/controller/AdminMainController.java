package com.stamp.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stamp.service.AdminManageService;
import com.stamp.service.AdminNoticeService;
import com.stamp.service.PointService;
import com.stamp.service.QuestionService;
import com.stamp.vo.MemberVO;
import com.stamp.vo.NoticeVO;
import com.stamp.vo.PointVO;
import com.stamp.vo.QuestionVO;

@Controller
@RequestMapping(value = "/admin")
public class AdminMainController {
	Logger logger = Logger.getLogger(AdminMainController.class);
	
	@Autowired
	private AdminNoticeService noticeService;
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private AdminManageService manageService;
	
	@Autowired
	private PointService pointService;
	
	// 메인을 눌렀을때
	@RequestMapping(value = "/adminPage", method = RequestMethod.GET)
	public String mainpage(HttpSession session, @ModelAttribute NoticeVO nvo, 
			@ModelAttribute QuestionVO qvo, @ModelAttribute MemberVO mvo, 
			@ModelAttribute PointVO pvo, Model model) {
		logger.info("main 호출 성공");
		
		//탭팬 게시판
		List<NoticeVO> mainNoticeList = noticeService.mainNoticeList(nvo);
		model.addAttribute("mainNoticeList", mainNoticeList);
		
		List<QuestionVO> mainQuestionList = questionService.mainQuestionList(qvo);
		model.addAttribute("mainQuestionList", mainQuestionList);
		
		//총 회원수
		int memberCount = manageService.memberCount(mvo);
		model.addAttribute("memberCount", memberCount);
		
		//총 게시글 수
		int questionCount = questionService.questionCount(qvo);
		model.addAttribute("questionCount", questionCount);
		
		//오늘 방문자 수
		int visitantCount = pointService.visitantCount(pvo);
		model.addAttribute("visitantCount", visitantCount);
		
		return "admin/adminPage";
	}

}

package com.stamp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.stamp.service.MessageService;
import com.stamp.service.MyPageService;
import com.stamp.vo.MessageVO;

@Controller
@RequestMapping(value = "/message")
public class MessageController {
	Logger logger = Logger.getLogger(MessageController.class);

	@Autowired
	private MessageService messageService;

	@Autowired
	private MyPageService myPageService;

	// 받은, 보낸편지함 눌렀을때
	@RequestMapping(value = "/messageBox/{boxType}", method = RequestMethod.GET)
	public String messageBox(HttpSession session, Model model, @PathVariable("boxType") String boxType,
			RedirectAttributes rttr) {
		logger.info("messageBox 호출 성공");

		// 로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}

		String m_id = session.getAttribute("log_id").toString();
		if (boxType.equals("sendBox")) {
			model.addAttribute("messageList", messageService.sendMessageList(m_id));
			model.addAttribute("boxType", boxType);
		}
		if (boxType.equals("getBox")) {
			model.addAttribute("messageList", messageService.getMessageList(m_id));
			model.addAttribute("boxType", boxType);
		}

		return "/message/messageBox";
	}

	// 메세지 상세보기
	@RequestMapping(value = "/messageDetail", method = RequestMethod.GET)
	public String messageDetail(HttpSession session, @ModelAttribute MessageVO msgvo, Model model,
			RedirectAttributes rttr) {
		logger.info("messageDetail 호출 성공");

		// 로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}

		logger.info("Msg_no = " + msgvo.getMsg_no());
		String m_id = session.getAttribute("log_id").toString();
		msgvo.setM_sender(m_id);
		MessageVO detail = new MessageVO();
		detail = messageService.messageDetail(msgvo);
		if (detail != null && (!detail.equals(""))) {
			detail.setMsg_content(detail.getMsg_content().toString().replaceAll("\n", "<br>"));
		}
		if (detail.getM_receiver().equals(m_id)) {
			if (messageService.messageChk(detail.getMsg_no()) == 1) {
				detail.setMsg_receivetime("지금 확인");
			}
		}
		model.addAttribute("detail", detail);

		return "message/messageDetail";
	}

	// 메세지 보내기창 구현
	@RequestMapping(value = "/messageWrite/{receiver}", method = RequestMethod.GET)
	public String messageWrite(HttpSession session, Model model, @PathVariable("receiver") String receiver,
			HttpServletRequest request) {
		logger.info("messageBox 호출 성공");
		//로그인중이 아닐시에 창끄기
		if (session.getAttribute("log_id") == null) {
			request.setAttribute("msg", "로그인을 해주세요");
			return "/sub/close";
		}
		model.addAttribute("receiver", receiver);
		model.addAttribute("receiver_name", myPageService.memberInfo(receiver).getM_name());
		return "/message/messageWrite";
	}

	// 메세지 보내기
	@RequestMapping(value = "/messageInsert", method = RequestMethod.POST)
	public String messageInsert(HttpSession session, @ModelAttribute MessageVO msgvo, HttpServletRequest request) {
		logger.info("messageInsert 호출성공");
		//로그인중이 아닐시에 창끄기
		if (session.getAttribute("log_id") == null) {
			request.setAttribute("msg", "로그인을 해주세요");
			return "/sub/close";
		}
		int result = 0;
		result = messageService.messageInsert(msgvo);
		if (result == 1) {
			return "/sub/close";
		}
		return "/sub/close";
	}

	// 메세지 삭제하기
	@RequestMapping(value = "/messageDelete", method = RequestMethod.POST)
	public String messageDelete(HttpSession session, @ModelAttribute MessageVO msgvo, HttpServletRequest request,
			RedirectAttributes rttr) {
		logger.info("messageDelete 호출성공");
		// 로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}
		String m_id = session.getAttribute("log_id").toString();
		int result = 0;
		String boxType = request.getParameter("boxType");
		String url = "";
		String m_receiver = msgvo.getM_receiver();
		String m_sender = msgvo.getM_sender();
		// 상태번호 0:정상 9:운영자삭제 8:양쪽삭제 7:보낸사람만 삭제 6:받은사람만 삭제
		if (boxType.equals("getBox")) {
			if (m_id.equals(m_receiver)) {
				result = messageService.messageDelete6(msgvo);
				System.out.println("boxType : " + boxType + "\nresult : " + result);
			}
			System.out.println("받은편지함");
		} else if (boxType.equals("sendBox")) {
			if (m_id.equals(m_sender)) {
				result = messageService.messageDelete7(msgvo);
				System.out.println("boxType : " + boxType + "\nresult : " + result);
			}
			System.out.println("보낸편지함");
		}
		if (result == 1) {
			url = "/message/messageBox/" + boxType + ".do";
		}
		System.out.println("result" + result);
		return "redirect:" + url;
	}

}

package com.stamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stamp.common.QuestionPaging;
import com.stamp.common.Util;
import com.stamp.service.QuestionService;
import com.stamp.vo.QuestionVO;

@Controller
@RequestMapping(value = "/admin/question")
public class AdminQuestionController {

	@Autowired
	private QuestionService questionService;
	
	@RequestMapping(value = "/questionList")
	public String manageList(@ModelAttribute QuestionVO qvo, Model model) {

		// 페이지 세팅
		QuestionPaging.set(qvo);

		// 전체 레코드 수 구하기
		int total = questionService.questionListCnt(qvo);

		int count = total - (Util.nvl(qvo.getPage()) - 1) * Util.nvl(qvo.getPageSize());
		
		List<QuestionVO> questionList = questionService.questionList(qvo);
		model.addAttribute("questionList", questionList);
		model.addAttribute("count", count);
		model.addAttribute("data", qvo);
		model.addAttribute("total", total);

		return "admin/question/questionList";
	}
	
	@RequestMapping(value = "/questionDetail", method = RequestMethod.GET)
	public String questionDetail(@ModelAttribute QuestionVO qvo, Model model) {
		
		QuestionVO detail = new QuestionVO();
		detail = questionService.boardDetail(qvo);
		
		model.addAttribute("detail", detail);
		
		return "admin/question/questionDetail";
	}
}

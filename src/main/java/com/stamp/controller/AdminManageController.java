package com.stamp.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stamp.common.ManagePaging;
import com.stamp.common.Util;
import com.stamp.service.AdminManageService;
import com.stamp.vo.MemberVO;

@Controller
@RequestMapping(value = "/admin/manage")
public class AdminManageController {
	Logger logger = Logger.getLogger(AdminManageController.class);

	@Autowired
	private AdminManageService manageService;

	// 회원 목록 구현
	@RequestMapping(value = "/manageList")
	public String manageList(@ModelAttribute MemberVO mvo, Model model) {
		logger.info("manageList 호출 성공");
		
		// 검색에 대한 데이터 확인   
		logger.info("search = "+mvo.getSearch());   
		logger.info("keyword = "+mvo.getKeyword()); 

		// 페이지 세팅
		ManagePaging.set(mvo);

		// 전체 레코드 수 구하기
		int total = manageService.manageListCnt(mvo);
		logger.info("total = " + total);

		int count = total - (Util.nvl(mvo.getPage()) - 1) * Util.nvl(mvo.getPageSize());
		logger.info("count = " + count);

		List<MemberVO> manageList = manageService.manageList(mvo);
		model.addAttribute("manageList", manageList);
		model.addAttribute("count", count);
		model.addAttribute("data", mvo);
		model.addAttribute("total", total);

		return "admin/manage/manageList";
	}

	// 회원 상세 보기
	@RequestMapping(value = "/manageDetail", method = RequestMethod.GET)
	public String manageDetail(@ModelAttribute MemberVO mvo, Model model) {
		logger.info("manageDetail 호출 성공");

		logger.info("m_id = " + mvo.getM_id());

		MemberVO detail = new MemberVO();
		detail = manageService.manageDetail(mvo);

		model.addAttribute("detail", detail);

		return "admin/manage/manageDetail";
	}
}

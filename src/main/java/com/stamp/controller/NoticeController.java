package com.stamp.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.stamp.common.NoticePaging;
import com.stamp.common.Util;
import com.stamp.service.NoticeService;
import com.stamp.vo.NoticeVO;
@Controller
@RequestMapping(value="/notice")
public class NoticeController {
	Logger logger=Logger.getLogger(NoticeController.class);
	
@Autowired
private NoticeService noticeService;

//글 목록 구현
	@RequestMapping(value="/noticeList")
	public String noticeList(@ModelAttribute NoticeVO nvo, Model model) {
		logger.info("noticeList 호출 성공");
		
		//페이지 세팅
		NoticePaging.set(nvo);
		
		//전체 레코드 수 구하기
		int total = noticeService.noticeListCnt(nvo);
		logger.info("total = " + total);
		
		int count = total - (Util.nvl(nvo.getPage())-1) * Util.nvl(nvo.getPageSize());
		logger.info("count = " + count);
		
		List<NoticeVO> noticeList = noticeService.noticeList(nvo);
		model.addAttribute("noticeList", noticeList);
		model.addAttribute("count", count);
		model.addAttribute("data", nvo);
		model.addAttribute("total", total);
		
		return "notice/noticeList";
	}

//글 상세 보기 구현
	@RequestMapping(value="/noticeDetail", method=RequestMethod.GET)
	public String noticeDetail(@ModelAttribute NoticeVO nvo, Model model) {
		logger.info("noticeDetail 호출 성공");
		
		logger.info("nt_no = " + nvo.getNt_no());
		
		noticeService.countUpdate(nvo.getNt_no());
		NoticeVO detail = new NoticeVO();
		detail = noticeService.noticeDetail(nvo);
		
		if(detail != null && (!detail.equals(""))) {
			detail.setNt_content(detail.getNt_content().toString().replaceAll("\n", "<br>"));
		}
		
		
		model.addAttribute("detail", detail);
		
		return "notice/noticeDetail";
	}
}

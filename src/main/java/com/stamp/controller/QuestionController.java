package com.stamp.controller;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.stamp.service.FileUploadUtil;
import com.stamp.service.PointService;
import com.stamp.service.QuestionService;
import com.stamp.vo.PointVO;
import com.stamp.vo.QuestionVO;

@Controller
@RequestMapping(value = "/qboard")
public class QuestionController {
	Logger logger = Logger.getLogger(QuestionController.class);

	@Autowired
	private QuestionService questionService;

	@Autowired
	private PointService pointService;
	
	@RequestMapping(value = "/boardList", method = RequestMethod.GET)
	public String boardList(@ModelAttribute QuestionVO qvo, Model model, HttpServletRequest request) {
		logger.info("boardList 호출");
		List<QuestionVO> boardList = questionService.boardList(qvo);

		int grade_search = qvo.getQ_grade();
		int subject_search = qvo.getQ_subject();
		model.addAttribute("boardList", boardList);
		if (grade_search == 123) {
			grade_search = -1;
		}

		logger.info("q_grade : " + grade_search);
		logger.info("q_subject: " + subject_search);
		return "qboard/boardList";
	}

	@RequestMapping(value = "/scrollDown", method = RequestMethod.POST)
	public @ResponseBody List<QuestionVO> scrollDown(@RequestBody QuestionVO qvo) {
		logger.info("무한스크롤 시작");
		
		return questionService.scrollDown(qvo);

	}

	@RequestMapping(value = "/writeForm")
	public String writeForm(HttpSession session) {
		logger.info("writeForm 호출");

		session.setAttribute("CSRF_TOKEN", UUID.randomUUID().toString());
		session.getAttribute("log_id");

		logger.info("CSRF_TOKEN : " + UUID.randomUUID().toString());
		return "qboard/writeForm";
	}

	@RequestMapping(value = "/boardInsert", method = RequestMethod.POST)
	public String boardInsert(@ModelAttribute QuestionVO qvo, HttpServletRequest request, HttpSession session,
			RedirectAttributes rttr) throws IllegalStateException, IOException {
		logger.info("boardInsert 호출");
		logger.info("fileName : " + qvo.getQ_file());
		logger.info("b_title : " + qvo.getQ_title());

		int port = request.getServerPort();
		String domain = "http://" + request.getServerName();
		if (port != 80 && port != 0) {
			domain += ":" + port;
		}
		domain += "/";
		qvo.setQ_count(0);
		qvo.setQ_state(1);
		qvo.setM_id((String) session.getAttribute("log_id"));
		int result = 0;
		if (pointService.memberPoints(qvo.getM_id()) < 10) {
			rttr.addFlashAttribute("msg", "포인트가 부족하여 글의 등록이 불가능 합니다.");
			return "redirect:/qboard/boardList.do";
		}
		String q_file = FileUploadUtil.fileUpload(qvo.getFile(), request);
		qvo.setQ_file(q_file);
		result = questionService.boardInsert(qvo);
		logger.info("insert no : " + qvo.getQ_no());
		if (result == 1) {
			PointVO pvo = new PointVO();
			pvo.setM_id(qvo.getM_id());
			pvo.setP_detail(domain + "qboard/boardDetail.do?q_no=" + qvo.getQ_no()+"");
			pointService.insertQuestionPoint(pvo);
			rttr.addFlashAttribute("msg", "글이 등록됐습니다.");
			request.setAttribute("reload", "reload");
			return "redirect:/qboard/boardList.do";
		}
		rttr.addFlashAttribute("msg", "오류발생 다시 시도를 해주세요.");
		return "redirect:/qboard/boardList.do";
	}

	//
	@RequestMapping(value = "/boardDetail", method = RequestMethod.GET)
	public String boardDetail(@ModelAttribute QuestionVO qvo, Model model, HttpSession session) {
		logger.info("boardDetail 호출");
		logger.info("q_no = " + qvo.getQ_no());
		Object log_id = "";
		log_id = session.getAttribute("log_id");
		if (log_id == null) {
			log_id = "";
		}
		QuestionVO detail = new QuestionVO();
		detail = questionService.boardDetail(qvo);
		// int count = detail.getQ_count();
		if (!log_id.equals(detail.getM_id())) {

			questionService.countUpdate(qvo.getQ_no());
		}

		if (detail != null && (!detail.equals(""))) {
			detail.setQ_content(detail.getQ_content().toString().replaceAll("\n", "<br>"));
		}

		model.addAttribute("detail", detail);
		return "qboard/boardDetail";
	}

	//
	@RequestMapping(value = "/updateForm", method = RequestMethod.POST)
	public String updateForm(@ModelAttribute QuestionVO qvo, Model model) {
		logger.info("updateForm 호출");
		logger.info("q_no = " + qvo.getQ_no());
		QuestionVO updateData = new QuestionVO();
		updateData = questionService.boardDetail(qvo);
		model.addAttribute("updateData", updateData);
		return "qboard/updateForm";
	}

	@RequestMapping(value = "/boardUpdate", method = RequestMethod.POST)
	public String boardUpdate(@ModelAttribute QuestionVO qvo, HttpServletRequest request)
			throws IllegalStateException, IOException {
		logger.info("boardUpdate 호출");
		int result = 0;
		String url = "";
		String q_file = "";
		if (!qvo.getFile().isEmpty()) {
			logger.info("===========q_file = " + qvo.getQ_file());
			FileUploadUtil.fileDelete(qvo.getQ_file(), request);
			q_file = FileUploadUtil.fileUpload(qvo.getFile(), request);
			qvo.setQ_file(q_file);
		} else {

			qvo.setQ_file("");
		}
		logger.info("==========Q_file = " + qvo.getQ_file());
		result = questionService.boardUpdate(qvo);
		if (result == 1) {
			url = "/qboard/boardDetail.do?q_no=" + qvo.getQ_no();
		}
		return "redirect:" + url;
	}

	@RequestMapping(value = "/boardDelete", method = RequestMethod.POST)
	public String boardDelete(@ModelAttribute QuestionVO qvo) throws IOException {
		logger.info("boardDelete 호출");

		int result = 0;
		String url = "";

		result = questionService.boardDelete(qvo.getQ_no());
		if (result == 1) {
			url = "/qboard/boardList.do";
		}
		return "redirect:" + url;
	}

}

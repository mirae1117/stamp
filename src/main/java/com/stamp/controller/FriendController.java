package com.stamp.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stamp.service.FriendService;
import com.stamp.vo.FriendVO;
import com.stamp.vo.MemberVO;

/**************************************************************
*	REST(Representational State Transfer)의 약어로 하나의 URI는 하나의
* 고유한 리소스를 대표하도록 설계된다는 개념이다. 최근에는    서버에 접근하는 기기의
* 종류가 다양해지면서 다양한 기기에서 공통으로 데이터를 처리할 수 있는 규칙을 만들려고
* 하는데 이러한 시도가  REST방식이다.
* REST방식은 특정 URL는 반드시 그에 상응하는 데이터 자체라는 것을 의미하는 방식이다.
* 예를 들어 'board/125'은 게시물 중에서 125번이라는 고유한 의미를 가지도록 설계하고,
* 이에 대한 처리는 GET, POST 방식과 같이 추가적인 정보를 통해서 결정한다.
**************************************************************/

/**************************************************************
 * 참고 : @RestController (@Controller + @ResponesBody) 기존의 특정한 JSP와 같은 뷰를 만들어 내는
 * 것이 목적이 아닌 REST 방식의 데이터 처리를 위해서 사용하는(데이터 자체를 반환) 어노테이션이다.
 *************************************************************/

@RestController
@RequestMapping(value = "/friend")
public class FriendController {
	Logger logger = Logger.getLogger(FriendController.class);

	@Autowired
	private FriendService friendService;

	/**************************************************************
	 * 친구 검색 구현하기
	 * 
	 **************************************************************/
	@RequestMapping(value = "/search/{f_keyw}.do", method = RequestMethod.GET)
	public ResponseEntity<List<MemberVO>> list(@PathVariable("f_keyw") String f_keyw, HttpSession session) {
		MemberVO condition = new MemberVO();
		String m_id = (String) session.getAttribute("log_id");
		condition.setM_id(m_id);
		condition.setM_pw(f_keyw);
		ResponseEntity<List<MemberVO>> entity = null;
		try {
			entity = new ResponseEntity<List<MemberVO>>(friendService.memberSelect(condition), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<List<MemberVO>>(HttpStatus.BAD_REQUEST);
		}
		return entity;
	}

	/**************************************************************
	 * 친구 신청 구현하기
	 * 
	 * @return String 참고 : @RequestBody
	 **************************************************************/
	@RequestMapping(value = "/callFriend")
	public ResponseEntity<String> callFriend(@RequestBody FriendVO fvo) {
		logger.info("callFriend 호출 성공");
		ResponseEntity<String> entity = null;
		int result;
		try {
			result = friendService.callFriend(fvo);
			if (result == 1) {
				entity = new ResponseEntity<String>("msg1", HttpStatus.OK);
			} else if (result < 0) {
				entity = new ResponseEntity<String>("msg2", HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}

	/**************************************************************
	 * 신청한 친구 목록 구현하기
	 * 
	 **************************************************************/
	@RequestMapping(value = "/serveCall", method = RequestMethod.GET)
	public ResponseEntity<List<MemberVO>> serveCall(HttpSession session) {
		String m_id = (String) session.getAttribute("log_id");
		ResponseEntity<List<MemberVO>> entity = null;
		try {
			entity = new ResponseEntity<List<MemberVO>>(friendService.serveCall(m_id), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<List<MemberVO>>(HttpStatus.BAD_REQUEST);
		}
		return entity;
	}

	/**************************************************************
	 * 신청 받은 친구 목록 구현하기
	 * 
	 **************************************************************/
	@RequestMapping(value = "/receiveCall", method = RequestMethod.GET)
	public ResponseEntity<List<MemberVO>> receiveCall(HttpSession session) {
		String m_id = (String) session.getAttribute("log_id");
		ResponseEntity<List<MemberVO>> entity = null;
		try {
			entity = new ResponseEntity<List<MemberVO>>(friendService.receiveCall(m_id), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<List<MemberVO>>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}

	/**************************************************************
	 * 친구 신청 거부, 친구 신청 취소 구현하기
	 * 
	 * @return String 참고 : @RequestBody
	 **************************************************************/
	@RequestMapping(value = "/unCallFriend")
	public ResponseEntity<String> unCallFriend(@RequestBody FriendVO fvo) {
		logger.info("unCallFriend 호출 성공");
		ResponseEntity<String> entity = null;
		int result;
		try {
			result = friendService.unCallFriend(fvo);
			if (result == 1) {
				entity = new ResponseEntity<String>("msg1", HttpStatus.OK);
			} else {
				entity = new ResponseEntity<String>("msg2", HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}

	/**************************************************************
	 * 친구 신청 수락 구현하기
	 * 
	 * @return String 참고 : @RequestBody
	 **************************************************************/
	@RequestMapping(value = "/consentCallFriend")
	public ResponseEntity<String> consentCallFriend(@RequestBody FriendVO fvo) {
		logger.info("consentCallFriend 호출 성공");
		ResponseEntity<String> entity = null;
		int result;
		try {
			result = friendService.consentCallFriend(fvo);
			if (result == 1) {
				entity = new ResponseEntity<String>("msg1", HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}

	/**************************************************************
	 * 신청 받은 친구 목록 구현하기
	 * 
	 **************************************************************/
	@RequestMapping(value = "/listFriend", method = RequestMethod.GET)
	public ResponseEntity<List<MemberVO>> listFriend(HttpSession session) {
		String m_id = (String) session.getAttribute("log_id");
		ResponseEntity<List<MemberVO>> entity = null;
		try {
			entity = new ResponseEntity<List<MemberVO>>(friendService.listFriend(m_id), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<List<MemberVO>>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}
	/**************************************************************
	 * 친구 신청 거부, 친구 신청 취소 구현하기
	 * 
	 * @return String 참고 : @RequestBody
	 **************************************************************/
	@RequestMapping(value = "/deleteFriend")
	public ResponseEntity<String> deleteFriend(@RequestBody FriendVO fvo) {
		logger.info("unCallFriend 호출 성공");
		ResponseEntity<String> entity = null;
		int result;
		try {
			result = friendService.deleteFriend(fvo);
			if (result == 2) {
				entity = new ResponseEntity<String>("msg1", HttpStatus.OK);
			} else {
				System.out.println("result = " + result);
				entity = new ResponseEntity<String>("msg2", HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
}

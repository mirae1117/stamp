package com.stamp.controller;

import java.io.IOException;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.stamp.service.LoginService;
import com.stamp.service.MessageService;
import com.stamp.service.MyPageService;
import com.stamp.service.PointService;
import com.stamp.vo.LoginVO;

@Controller
@RequestMapping(value = "/login")
public class LoginController {
	Logger logger = Logger.getLogger(LoginController.class);

	@Autowired
	private LoginService loginService;

	@Autowired
	private PointService pointService;
	
	@Autowired
	private MyPageService myPageService;
	
	@Autowired
	private MessageService messageService;
	
	// 로그인 눌렀을때
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(HttpSession session) {
		logger.info("login 호출 성공");
		return "login/login";
	}

	// 로그인 세션 없을때
	@RequestMapping(value = "/loginForm", method = RequestMethod.GET)
	public String loginForm(HttpSession session) {
		logger.info("loginForm 호출 성공");
		return "login/loginForm";
	}

	// 로그인 세션 있을때
	@RequestMapping(value = "/loginPro", method = RequestMethod.GET)
	public String loginPro(HttpSession session) {
		logger.info("loginPro 호출 성공");
		session.setAttribute("message_count", messageService.getMessageCount(session.getAttribute("log_id").toString()));
		session.setAttribute("memberPoints", pointService.memberPoints(session.getAttribute("log_id").toString()));
		return "login/loginPro";
	}

	// 로그인 누를때
	@RequestMapping(value = "/loginPro", method = RequestMethod.POST)
	public String joinPro(@ModelAttribute LoginVO lvo, HttpSession session, RedirectAttributes rttr) throws IllegalStateException, IOException {
		logger.info("loginPro(POST) 호출 성공");
		int result = 0;
		String url = "redirect:/main/main.do";

		if (lvo.getLog_id() == "" || lvo.getLog_id() == null) {
			result = -4;
		} else {
			if (lvo.getLog_pw() == "" || lvo.getLog_pw() == null) {
				result = -5;
			} else {
				result = loginService.loginMember(lvo);
			}
		}

		if (result >= 10 && result < 20) {
			session.setAttribute("log_id", lvo.getLog_id());
			session.setAttribute("log_name", myPageService.memberInfo(lvo.getLog_id()).getM_name());
			session.setAttribute("log_state", "일반회원");
			pointService.insertAttendPoint(lvo.getLog_id());
		} else if (result >= 20 && result < 30) {
			session.setAttribute("log_id", lvo.getLog_id());
			session.setAttribute("log_name", myPageService.memberInfo(lvo.getLog_id()).getM_name());
			session.setAttribute("log_state", "관리자");
			pointService.insertAttendPoint(lvo.getLog_id());
		} else {
			String msg;
			switch (result) {
			case -1:
				msg = "사용불가능 한 아이디 입니다.(정지, 탈퇴 등)";
				break;
			case -2:
				msg = "비밀번호가 틀렸습니다.";
				break;
			case -3:
				msg = "해당 아이디가 없습니다.";
				break;
			case -4:
				msg = "아이디를 입력해 주세요.";
				break;
			case -5:
				msg = "비밀번호를 입력해 주세요.";
				break;
			default:
				msg = "알수없는 에러로 로그인을 실패 하였습니다.";
				break;
			}
			rttr.addFlashAttribute("msg", msg);
		}
		return url;
	}

	// 로그아웃 누를때
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(HttpSession session) {
		logger.info("logout 호출 성공");
		session.invalidate();
		return "redirect:/main/main.do";
	}
}

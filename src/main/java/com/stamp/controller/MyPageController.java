package com.stamp.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.stamp.service.FileUploadUtil;
import com.stamp.service.FriendService;
import com.stamp.service.LoginService;
import com.stamp.service.MyPageService;
import com.stamp.service.PointService;
import com.stamp.vo.FriendVO;
import com.stamp.vo.LoginVO;
import com.stamp.vo.MemberVO;
import com.stamp.vo.PointVO;

@Controller
@RequestMapping(value = "/mypage")
public class MyPageController {
	Logger logger = Logger.getLogger(MyPageController.class);

	@Autowired
	private MyPageService myPageService;
	
	@Autowired
	private PointService pointService;
	
	@Autowired
	private LoginService loginService;

	@Autowired
	private FriendService friendService;
	
	// 마이페이지 눌렀을때
	@RequestMapping(value = "/info", method = RequestMethod.GET)
	public String myInfo(HttpSession session, Model model, RedirectAttributes rttr) {
		logger.info("myInfo 호출 성공");
		
		//로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}
		
		String m_id = session.getAttribute("log_id").toString();
		model.addAttribute("userInfo", myPageService.memberInfo(m_id));
		model.addAttribute("lastAttendDay", pointService.lastAttendDay(m_id));
		
		return "/mypage/info";
	}	
	// 정보 수정 페이지 눌렀을떄
	@RequestMapping(value = "/infoUpdate", method = RequestMethod.GET)
	public String myUpdate(HttpSession session, Model model, RedirectAttributes rttr) {
		logger.info("infoUpdate 호출 성공");
		
		//로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}
		
		String m_id = session.getAttribute("log_id").toString();
		return "/mypage/myInfoUpdateAgree";
	}	
	// 정보 수정 확인 페이지에서 로그인 했을시
	@RequestMapping(value = "/infoUpdateForm", method = RequestMethod.POST)
	public String myUpdateAgree(@ModelAttribute LoginVO lvo, HttpSession session, Model model, RedirectAttributes rttr) {
		logger.info("infoUpdateForm(post) 호출 성공");
		
		//로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}
		int loginInt = loginService.loginMember(lvo);
		if (loginInt< 10 || loginInt >= 30) {
			rttr.addFlashAttribute("msg", "비밀번호가 틀립니다");
			return "redirect:/mypage/infoUpdate.do";
		} else {
			String m_id = session.getAttribute("log_id").toString();
			model.addAttribute("userInfo", myPageService.memberInfo(m_id));
			return "redirect:/mypage/infoUpdateForm.do";
		}
	}
	
	// 정보 수정 확인 페이지에서 로그인 했을시2
	@RequestMapping(value = "/infoUpdateForm", method = RequestMethod.GET)
	public String infoUpdateForm(HttpSession session, Model model, RedirectAttributes rttr) {
		logger.info("infoUpdateForm 호출 성공");
		
		//로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}
		
		String m_id = session.getAttribute("log_id").toString();
		model.addAttribute("userInfo", myPageService.memberInfo(m_id));
		return "/mypage/myInfoUpdate";
	}	
	// 포인트 페이지 눌렀을떄
	@RequestMapping(value = "/points", method = RequestMethod.GET)
	public String myPoint(HttpSession session, Model model, RedirectAttributes rttr) {
		logger.info("points 호출 성공");
		
		//로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}
		
		return "/mypage/points";
	}	
	
	/**************************************************************
	 * 신청 받은 친구 목록 구현하기
	 * 
	 **************************************************************/
	@RequestMapping(value = "/listPoint", method = RequestMethod.GET)
	public ResponseEntity<List<PointVO>> listFriend(HttpSession session) {
		String m_id = (String) session.getAttribute("log_id");
		ResponseEntity<List<PointVO>> entity = null;
		try {
			entity = new ResponseEntity<List<PointVO>>(pointService.listPoint(m_id), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<List<PointVO>>(HttpStatus.BAD_REQUEST);
		}

		return entity;
	}
	// 정보 수정 완료
	@RequestMapping(value = "/infoUpdatePro", method = RequestMethod.POST)
	public String myUpdatePro(@ModelAttribute MemberVO mvo,HttpSession session, RedirectAttributes rttr, Model model, HttpServletRequest request)
			throws IllegalStateException, IOException {
		logger.info("infoUpdate(POST) 호출 성공");
		int result = 0;
		String url = "";
		String m_profile = FileUploadUtil.fileUpload(mvo.getFile(), request);
		mvo.setM_profile(m_profile);
		result = myPageService.memberUpdate(mvo);
		if (result == 1) {
			model.addAttribute("userInfo", myPageService.memberInfo(mvo.getM_id()));
			url = "redirect:/mypage/info.do";
		}
		return url;
	}	
	// 탈퇴 눌렀을떄
	@RequestMapping(value = "/dropMember", method = RequestMethod.GET)
	public String dropMember(HttpSession session, RedirectAttributes rttr, Model model) {
		logger.info("dropMember 호출 성공");
		
		//로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}
		return "/mypage/dropMemberAgree";
	}	
	// 탈퇴 확인 페이지에서 로그인 했을시
	@RequestMapping(value = "/dropMember", method = RequestMethod.POST)
	public String dropMember(@ModelAttribute LoginVO lvo, HttpSession session, Model model, RedirectAttributes rttr) {
		logger.info("dropMember(post) 호출 성공");
		
		//로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}
		int loginInt = loginService.loginMember(lvo);
		System.out.println("loginInt : " + loginInt);
		if (loginInt< 10 || loginInt >= 30) {
			rttr.addFlashAttribute("msg", "비밀번호가 틀립니다");
			return "redirect:/mypage/dropMember.do";
		} else {
			String m_id = session.getAttribute("log_id").toString();
			myPageService.memberDelete(m_id);
			session.invalidate();
			return "redirect:/main/main.do";
		}
	}
	
	
	
	
	// 친구 정보 눌렀을때
	@RequestMapping(value = "/friends", method = RequestMethod.GET)
	public String myFriends(HttpSession session, RedirectAttributes rttr, Model model) {
		logger.info("myFriends 호출 성공");
		
		//로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}
		
		return "/mypage/friends";
	}	
	// 친구 검색 눌렀을때
	@RequestMapping(value = "/findFriend", method = RequestMethod.GET)
	public String findfriend(HttpSession session, RedirectAttributes rttr, Model model) {
		logger.info("findfriend 호출 성공");
		
		//로그인중이 아닐시에 메인페이지로 리다이렉트
		if (session.getAttribute("log_id") == null) {
			rttr.addFlashAttribute("msg", "로그인을 해주세요");
			return "redirect:/main/main.do";
		}
		
		return "/mypage/findFriend";
	}
	// 포인트 충전 눌렀을때
	@RequestMapping(value = "/chargePoint", method = RequestMethod.GET)
	public String chargePoint(HttpSession session, HttpServletRequest request, Model model) {
		logger.info("chargePoint 호출 성공");

		//로그인중이 아닐시에 창끄기
		if (session.getAttribute("log_id") == null) {
			request.setAttribute("msg", "로그인을 해주세요");
			return "/sub/close";
		}
		model.addAttribute("log_id", session.getAttribute("log_id"));
		return "/mypage/chargePoint";
	}
	// 포인트 충전 완료
	@RequestMapping(value = "/chargePoint", method = RequestMethod.POST)
	public ResponseEntity<String> chargePointPro(@RequestBody PointVO pvo)
			throws IllegalStateException, IOException {
		logger.info("chargePoint(POST) 호출 성공");
		ResponseEntity<String> entity = null;
		int result = 0;
		
		try {
			result = pointService.chargePoint(pvo);
			if (result == 1) {
				entity = new ResponseEntity<String>("msg1", HttpStatus.OK);
			} else {
				entity = new ResponseEntity<String>("msg2", HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	// 다른유저 정보 눌렀을때
	@RequestMapping(value = "/otherInfo/{m_id}.do", method = RequestMethod.GET)
	public String otherInfo(HttpSession session, Model model, @PathVariable("m_id") String m_id, HttpServletRequest request) {
		logger.info("otherInfo 호출 성공");

		//로그인중이 아닐시에 창끄기
		if (session.getAttribute("log_id") == null) {
			request.setAttribute("msg", "로그인을 해주세요");
			return "/sub/close";
		}
		MemberVO userInfo = myPageService.memberInfo(m_id);
		if (userInfo.getM_private() != 0) {
			model.addAttribute("userInfo", userInfo);
			model.addAttribute("lastAttendDay", pointService.lastAttendDay(m_id));
		}else{
			MemberVO privateMvo = new MemberVO();
			privateMvo.setM_id(m_id);
			privateMvo.setM_name(userInfo.getM_name());
			model.addAttribute("userInfo", privateMvo);
		}

		FriendVO fvo = new FriendVO();
		fvo.setM_receiver(m_id);
		fvo.setM_sender(session.getAttribute("log_id").toString());
		//내가요청 1 친구중 2 상대가요청 3 아무상태도아니거나 블랙리스트 0
		model.addAttribute("friendState", friendService.callFriendBeforeChk(fvo));
		return "/mypage/otherInfo";
	}	
	
	
	
}

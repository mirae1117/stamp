package com.stamp.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stamp.vo.PointVO;

@Repository
public class PointDaoImpl implements PointDao {
	@Autowired
	private SqlSession session;

	@Override
	public int insertJoinPoint(String m_id) {
		return session.insert("insertJoinPoint", m_id);
	}

	@Override
	public void insertAttendPoint(String log_id) {
		session.insert("insertAttendPoint", log_id);
	}
	@Override
	public int attendChk(String log_id) {
		return session.selectOne("attendChk", log_id);
	}

	@Override
	public String lastAttendDay(String m_id) {
		return session.selectOne("lastAttendDay", m_id);
	}

	@Override
	public int memberPoints(String m_id) {
		return session.selectOne("memberPoints", m_id);
	}

	@Override
	public List<PointVO> listPoint(String m_id) {
		return session.selectList("memberPointbreakdown", m_id);
	}

	@Override
	public int insertQuestionPoint(PointVO pvo) {
		return session.insert("insertQuestionPoint", pvo);
	}

	@Override
	public int chargePoint(PointVO pvo) {
		return session.insert("insertChargePoint", pvo);
	}

	@Override
	public int visitantCount(PointVO pvo) {
		return session.selectOne("visitantCount", pvo);
	}

	@Override
	public List<PointVO> pointChart() {
		return session.selectList("pointChart");
	}

	@Override
	public int insertReplyPoint(PointVO pvo) {
		return session.insert("insertReplyPoint", pvo);
	}

}

package com.stamp.dao;

import java.util.List;

import com.stamp.vo.ReplyVO;

public interface ReplyDao {

	public int replyInsert(ReplyVO rvo);

	public List<ReplyVO> replyList(int q_no);

	//public List<ReplyVO> replyList(ReplyVO rvo);
	
	public int replyUpdate(ReplyVO rvo);

	public int replyDelete(int rep_no);

	public void replySelect(int rep_no);

	public ReplyVO getReply(int rep_no);


	//public ReplyVO replyBest(int q_no);

	//public String replyDetail(int rep_no);
}

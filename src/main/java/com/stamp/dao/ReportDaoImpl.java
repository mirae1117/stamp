package com.stamp.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stamp.vo.QuestionVO;
import com.stamp.vo.ReplyVO;
import com.stamp.vo.ReportVO;

@Repository
public class ReportDaoImpl implements ReportDao {

	@Autowired
	private SqlSession session;

	@Override
	public int reportInsert(ReportVO rvo) {
		// TODO Auto-generated method stub
		return session.insert("reportInsert", rvo);
	}

	@Override
	public int reportCount(ReportVO rvo) {
		// TODO Auto-generated method stub
		return session.selectOne("reportCount", rvo);
	}

	@Override
	public void reportUpdate(String no) {
		// TODO Auto-generated method stub
		QuestionVO qvo = new QuestionVO();
		ReplyVO rvo = new ReplyVO();
		String result = no.substring(0, 1);
		System.out.println("글종류 : "+result);
		String go = no.substring(2);
		System.out.println("글번호 : "+go);
		if (result.equals("1")) {
			qvo.setQ_no(Integer.parseInt(go));
			System.out.println(go);
			session.update("questionReportUpdate", qvo);
		}else{
			rvo.setRep_no(Integer.parseInt(go));
			session.update("replyReportUpdate",rvo);
		}
	}

}

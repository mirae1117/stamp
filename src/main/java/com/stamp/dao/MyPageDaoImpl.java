package com.stamp.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stamp.vo.MemberVO;

@Repository
public class MyPageDaoImpl implements MyPageDao {

	@Autowired
	private SqlSession session;
	
	//마이페이지 개인정보
	@Override
	public MemberVO memberInfo(String m_id) {
		return session.selectOne("memberInfo", m_id);
	}

	@Override
	public int memberUpdate(MemberVO mvo) {
		return session.update("memberUpdate", mvo);
	}

	@Override
	public int memberDelete(String m_id) {
		return session.update("memberDelete", m_id);
	}

}

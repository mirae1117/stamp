package com.stamp.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stamp.vo.ReplyVO;

@Repository
public class ReplyDaoImpl implements ReplyDao {
	@Autowired
	private SqlSession session;

	// 댓글입력
	@Override
	public int replyInsert(ReplyVO rvo) {
		// TODO Auto-generated method stub
		return session.insert("replyInsert", rvo);
	}

	// 댓글리스트
	@Override
	public List<ReplyVO> replyList(int q_no) {
		// TODO Auto-generated method stub
		ReplyVO rvo = new ReplyVO();
		rvo.setQ_no(q_no);
		return session.selectList("replyList", rvo);
	}

	// 댓글수정
	@Override
	public int replyUpdate(ReplyVO rvo) {
		return session.update("replyUpdate", rvo);
	}

	// 댓글삭제
	@Override
	public int replyDelete(int rep_no) {
		// TODO Auto-generated method stub
		ReplyVO rvo = new ReplyVO();
		rvo.setRep_no(rep_no);
		System.out.println("java.lang.Integer error???  : " + rvo.getRep_no());
		return session.update("replyDelete", rvo);
	}

	// 댓글채택
	@Override
	public void replySelect(int rep_no) {
		// TODO Auto-generated method stub
		ReplyVO rvo = new ReplyVO();
		rvo.setRep_no(rep_no);
		session.update("replySelect", rvo);
	}

	@Override
	public ReplyVO getReply(int rep_no) {
		return session.selectOne("getReply", rep_no);
	}

}

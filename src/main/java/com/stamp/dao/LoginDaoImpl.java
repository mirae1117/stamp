package com.stamp.dao;


import org.apache.ibatis.session.SqlSession;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stamp.vo.LoginVO;

@Repository
public class LoginDaoImpl implements LoginDao {

	@Autowired
	private SqlSession session;

	//로그인
	@Override
	public int memberLogin(LoginVO lvo) {
		try {
			if (BCrypt.checkpw(lvo.getLog_pw(), session.selectOne("memberLogin", lvo.getLog_id()))) {
				int state1 = session.selectOne("memberLoginState", lvo.getLog_id());
				if (state1 >= 10 && state1 < 30) {
					//상태번호 반환
					return state1;
				} else if(state1 > 80){
					//사용 불가능한 아이디
					return -1;
				} else {
					//반영 안됀 상태번호 -4 반환
					return -4;
				}
			} else {
				//비밀번호 틀림
				return -2;
			}
		} catch (NullPointerException e) {
			//ID 없을경우
			return -3;
		}
	}

}

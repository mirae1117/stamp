package com.stamp.dao;

import java.util.List;

import com.stamp.vo.FriendVO;
import com.stamp.vo.MemberVO;

public interface FriendDao {
	public List<MemberVO> memberSelect(MemberVO mvo);
	public int callFriend(FriendVO fvo);
	public int callFriendBeforeChk(FriendVO fvo);
	public List<MemberVO> serveCall(String m_id);
	public List<MemberVO> receiveCall(String m_id);
	public int unCallFriend(FriendVO fvo);
	public int consentCallFriend(FriendVO fvo);
	public List<MemberVO> listFriend(String m_id);
	public int deleteFriend(FriendVO fvo);
}

package com.stamp.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stamp.vo.GuardianVO;
import com.stamp.vo.MemberVO;

@Repository
public class JoinDaoImpl implements JoinDao {
	
	@Autowired
	private SqlSession session;

	//법정대리인 인증번호 발급
	@Override
	public int guardianAttestation(GuardianVO gvo) {
		return session.insert("guardianAttestation",gvo);
	}
	//법정대리인 말소 인증번호 제거
	@Override
	public void guardianErasure(GuardianVO gvo) {
		session.delete("guardianErasure", gvo);		
	}
	//법정대리인 인증번호 확인
	@Override
	public int guardianCertify(GuardianVO gvo) {
		return session.selectOne("guardianCertify",gvo);
	}
	//회원 가입
	@Override
	public int memberInsert(MemberVO mvo) {
		return session.insert("mbmberInsert", mvo);
	}
	//어린이 회원 가입시 법정대리인 저장
	@Override
	public int saveGuardian(GuardianVO gvo) {
		return session.update("saveGuardian", gvo);
	}
	@Override
	public int memberChk(String m_id) {
		return session.selectOne("memberChk", m_id);
	}
	//이메일로 아이디 찾기
	@Override
	public List<String> idFind(String m_email) {
		return session.selectList("idFind", m_email);
	}
	//이메일과 아이디가 일치하나 검사
	@Override
	public int pwFindIdEmailChk(MemberVO mvo) {
		return session.selectOne("pwFindIdEmailChk", mvo);
	}
	//비밀번호찾기 인증키 발송
	@Override
	public int pwFindAgreeInsert(GuardianVO gvo) {
		pwFindAgreeDelete(gvo.getG_email());
		return session.insert("pwFindAgreeInsert", gvo);
	}
	//재발급전 인증안한 인증키, 인증후 인증키 삭제
	@Override
	public void pwFindAgreeDelete(String g_email) {
		session.delete("pwFindAgreeDelete", g_email);
	}
	//비밀번호 찾기 인증 작업
	@Override
	public int pwFindCertify(GuardianVO gvo) {
		int result = session.selectOne("pwFindCertify", gvo);
		if (result == 1) {
			pwFindAgreeDelete(gvo.getG_email());
			return 1;
		}
		return 0;
	}
	//비밀번호 찾기시 인증키와 이메일로 아이디 다시 불러오기
	@Override
	public String pwFindGetId(GuardianVO gvo) {
		return session.selectOne("pwFindGetId", gvo);
	}
	// 분실 비밀번호 변경 최종단계	
	@Override
	public int pwFindChange(MemberVO mvo) {
		return session.update("pwFindChange", mvo);
	}

}

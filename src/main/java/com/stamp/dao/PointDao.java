package com.stamp.dao;

import java.util.List;

import com.stamp.vo.PointVO;

public interface PointDao {

	public int insertJoinPoint(String m_id);

	public void insertAttendPoint(String log_id);

	public int attendChk(String log_id);

	public String lastAttendDay(String m_id);

	public int memberPoints(String m_id);

	public List<PointVO> listPoint(String m_id);

	public int insertQuestionPoint(PointVO pvo);

	public int chargePoint(PointVO pvo);

	public int visitantCount(PointVO pvo);
	
	public List<PointVO> pointChart();

	public int insertReplyPoint(PointVO pvo);

}

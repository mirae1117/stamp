package com.stamp.dao;

import com.stamp.vo.ReportVO;

public interface ReportDao {

	public int reportInsert(ReportVO rvo);

	public int reportCount(ReportVO rvo);

	public void reportUpdate(String no);
}

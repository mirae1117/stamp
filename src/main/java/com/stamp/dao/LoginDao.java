package com.stamp.dao;

import com.stamp.vo.LoginVO;

public interface LoginDao {

	public int memberLogin(LoginVO lvo);

}

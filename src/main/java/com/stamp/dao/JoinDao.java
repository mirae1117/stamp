package com.stamp.dao;

import java.util.List;

import com.stamp.vo.GuardianVO;
import com.stamp.vo.MemberVO;

public interface JoinDao {
	//법정대리인 인증번호 발급
	public int guardianAttestation(GuardianVO gvo);
	//법정대리인 말소 인증번호 제거
	public void guardianErasure(GuardianVO gvo);
	//법정대리인 번호 체크
	public int guardianCertify(GuardianVO gvo);
	//회원가입
	public int memberInsert(MemberVO mvo);
	//어린이 회원 가입시 법정대리인 저장
	public int saveGuardian(GuardianVO gvo);
	//중복체크
	public int memberChk(String m_id);
	public List<String> idFind(String m_email);
	public int pwFindIdEmailChk(MemberVO mvo);
	public int pwFindAgreeInsert(GuardianVO gvo);
	//법정대리인 말소 인증번호 제거
	public void pwFindAgreeDelete(String g_email);
	public int pwFindCertify(GuardianVO gvo);
	public String pwFindGetId(GuardianVO gvo);
	public int pwFindChange(MemberVO mvo);
}
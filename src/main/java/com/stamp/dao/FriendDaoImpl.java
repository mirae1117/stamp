package com.stamp.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stamp.vo.FriendVO;
import com.stamp.vo.MemberVO;

@Repository
public class FriendDaoImpl implements FriendDao {
	@Autowired
	private SqlSession session;

	// 친구 찾기 목록 출력
	@Override
	public List<MemberVO> memberSelect(MemberVO mvo) {
		return session.selectList("memberSelect", mvo);
	}

	// 친구 상태 출력 
	@Override
	public int callFriendBeforeChk(FriendVO fvo) {
		int result = session.selectOne("callFriendBeforeChk", fvo);
		if (result == 1) {
			System.out.println("result : " + result);
			int reCallFriendChk = session.selectOne("reCallFriendChk", fvo);
			System.out.println("reCallFriendChk : " + reCallFriendChk);
			if (reCallFriendChk == 0) {
				result = 3;
			}
		}
		return result;
	}
	
	// 친구 신청하기
	@Override
	public int callFriend(FriendVO fvo) {
		// 0이면 현재 관계없음 1이상이면 친구혹은 블랙리스트 관계 혹은 이미 신청중
		switch ((Integer) session.selectOne("callFriendBeforeChk", fvo)) {
		case 0:
			return session.insert("callFriend", fvo);	
		case 1:
			return -2;
		default:
			return -1;
		}
	}

	// 친구 신청한 목록
	@Override
	public List<MemberVO> serveCall(String m_id) {
		return session.selectList("serveCallFriend", m_id);
	}

	// 친구 신청받은 목록
	@Override
	public List<MemberVO> receiveCall(String m_id) {
		return session.selectList("receiveCallFriend", m_id);
	}

	// 친구 신청 거부, 취소
	@Override
	public int unCallFriend(FriendVO fvo) {
		return session.delete("unCallFriend", fvo);
	}

	// 친구 신청 수락
	@Override
	public int consentCallFriend(FriendVO fvo) {
		if (session.update("consentCallUpdateFriend", fvo) == 1) {
			return session.insert("consentCallInsertFriend", fvo);
		}
		return 0;
	}

	// 친구 목록
	@Override
	public List<MemberVO> listFriend(String m_id) {
		return session.selectList("listFriend", m_id);
	}

	// 친구 삭제
	@Override
	public int deleteFriend(FriendVO fvo) {
		return session.delete("deleteFriend", fvo);
	}


}

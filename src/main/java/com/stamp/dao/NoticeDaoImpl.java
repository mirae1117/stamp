package com.stamp.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stamp.vo.NoticeVO;
@Repository
public class NoticeDaoImpl implements NoticeDao {

	@Autowired
	private SqlSession session;
	
	//글목록 구현
	@Override
	public List<NoticeVO> noticeList(NoticeVO nvo) {
		// TODO Auto-generated method stub
		return session.selectList("noticeList",nvo);
	}

	//전체 레코드 건수 구현
	@Override
	public int noticeListCnt(NoticeVO nvo) {
		// TODO Auto-generated method stub
		return (Integer)session.selectOne("noticeListCnt",nvo);
	}

	//글 입력 구현/
	@Override
	public int noticeInsert(NoticeVO nvo) {
		// TODO Auto-generated method stub
		return session.insert("noticeInsert", nvo);
	}

	//글 상세 구현
	@Override
	public NoticeVO noticeDetail(NoticeVO nvo) {
		// TODO Auto-generated method stub
		return (NoticeVO)session.selectOne("noticeDetail", nvo);
	}

	//글 수정 구현
	@Override
	public int noticeUpdate(NoticeVO nvo) {
		// TODO Auto-generated method stub
		return session.update("noticeUpdate", nvo);
	}

	//글 삭제 구현
	@Override
	public int noticeDelete(int nt_no) {
		// TODO Auto-generated method stub
		return session.delete("noticeDelete",nt_no);
	}

	@Override
	public int countUpdate(int nt_no) {
		// TODO Auto-generated method stub
		return session.update("noticeCountUpdate", nt_no);
	}

}

package com.stamp.dao;

import java.util.List;

import com.stamp.vo.MemberVO;

public interface AdminManageDao {

	public List<MemberVO> manageList(MemberVO mvo);
	public MemberVO manageDetail(MemberVO mvo);
	public int manageListCnt(MemberVO mvo);
	public List<MemberVO> memberJoinChart();
	public List<MemberVO> memberGenderChart();
	public List<MemberVO> memberGradeChart();
	public int memberCount(MemberVO mvo);
}

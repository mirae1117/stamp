package com.stamp.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.stamp.vo.NoticeVO;

@Repository
public class AdminNoticeDaoImpl implements AdminNoticeDao {

	@Inject
	private SqlSession sqlSession;
	
	private static final String NAMESPACE = "com.stamp.dao.NoticeMapper";
	
	//글 목록 구현
	@Override
	public List<NoticeVO> noticeList(NoticeVO nvo) {
		return sqlSession.selectList(NAMESPACE + ".noticeList", nvo);
	}

	//글 등록 구현
	@Override
	public int noticeInsert(NoticeVO nvo) {
		return sqlSession.insert(NAMESPACE + ".noticeInsert", nvo);
	}

	//글 상세 구현
	@Override
	public NoticeVO noticeDetail(NoticeVO nvo) {
		return sqlSession.selectOne(NAMESPACE + ".noticeDetail", nvo);
	}

	//글 수정 구현
	@Override
	public int noticeUpdate(NoticeVO nvo) {
		return sqlSession.update(NAMESPACE + ".noticeUpdate", nvo);
	}

	//글 삭제 구현
	@Override
	public int noticeDelete(int nt_no) {
		return sqlSession.delete(NAMESPACE + ".noticeDelete", nt_no);
	}

	//전체 레코드 건수 구현
	@Override
	public int noticeListCnt(NoticeVO nvo) {
		return sqlSession.selectOne(NAMESPACE + ".noticeListCnt", nvo);
	}
	
	@Override
	public List<NoticeVO> mainNoticeList(NoticeVO nvo) {
		return sqlSession.selectList(NAMESPACE + ".mainNoticeList", nvo);
	}

	@Override
	public int countUpdate(int nt_no) {
		// TODO Auto-generated method stub
		return sqlSession.update(NAMESPACE+".noticeCountUpdate", nt_no);
	}
}

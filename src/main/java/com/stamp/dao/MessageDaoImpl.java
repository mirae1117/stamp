package com.stamp.dao;


import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stamp.vo.MessageVO;


@Repository
public class MessageDaoImpl implements MessageDao {
	@Autowired
	private SqlSession session;

	@Override
	public List<MessageVO> sendMessageList(String m_id) {
		return session.selectList("sendMessageList", m_id);
	}

	@Override
	public List<MessageVO> getMessageList(String m_id) {
		return session.selectList("getMessageList", m_id);
	}

	@Override
	public MessageVO messageDetail(MessageVO msgvo) {
		return session.selectOne("messageDetail", msgvo);
	}

	@Override
	public int messageChk(int msg_no) {
		return session.update("messageChk", msg_no);
	}

	@Override
	public int messageInsert(MessageVO msgvo) {
		return session.insert("messageInsert", msgvo);
	}

	@Override
	public int messageState(MessageVO msgvo) {
		return session.selectOne("messageState", msgvo);
	}

	@Override
	public int messageDelete8(MessageVO msgvo) {
		return session.update("messageDelete8", msgvo);
	}

	@Override
	public int messageDelete7(MessageVO msgvo) {
		return session.update("messageDelete7", msgvo);
	}
	@Override
	public int messageDelete6(MessageVO msgvo) {
		return session.update("messageDelete6", msgvo);
	}

	@Override
	public int getMessageCount(String log_id) {
		return session.selectOne("getMessageCount", log_id);
	}

}

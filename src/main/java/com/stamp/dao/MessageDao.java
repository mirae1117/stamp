package com.stamp.dao;

import java.util.List;

import com.stamp.vo.MessageVO;

public interface MessageDao {

	public List<MessageVO> sendMessageList(String m_id);

	public List<MessageVO> getMessageList(String m_id);

	public MessageVO messageDetail(MessageVO msgvo);

	public int messageChk(int msg_no);

	public int messageInsert(MessageVO msgvo);

	public int messageState(MessageVO msgvo);

	public int messageDelete8(MessageVO msgvo);

	public int messageDelete7(MessageVO msgvo);

	public int messageDelete6(MessageVO msgvo);

	public int getMessageCount(String log_id);
}

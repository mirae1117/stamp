package com.stamp.dao;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.stamp.vo.MemberVO;

@Repository
public class AdminManageDaoImpl implements AdminManageDao{

	@Inject
	private SqlSession sqlSession;
	
	private static final String NAMESPACE = "com.stamp.dao.ManageMapper";
	
	@Override
	public List<MemberVO> manageList(MemberVO mvo) {
		return sqlSession.selectList(NAMESPACE + ".manageList", mvo);
	}

	@Override
	public MemberVO manageDetail(MemberVO mvo) {
		return sqlSession.selectOne(NAMESPACE + ".manageDetail", mvo);
	}

	@Override
	public int manageListCnt(MemberVO mvo) {
		return sqlSession.selectOne(NAMESPACE + ".manageListCnt", mvo);
	}

	@Override
	public List<MemberVO> memberJoinChart() {
		return sqlSession.selectList(NAMESPACE + ".memberJoinChart");
	}

	@Override
	public List<MemberVO> memberGenderChart() {
		return sqlSession.selectList(NAMESPACE + ".memberGenderChart");
	}

	@Override
	public List<MemberVO> memberGradeChart() {
		return sqlSession.selectList(NAMESPACE + ".memberGradeChart");
	}

	@Override
	public int memberCount(MemberVO mvo) {
		return sqlSession.selectOne(NAMESPACE + ".memberCount", mvo);
	}

}

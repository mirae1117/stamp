package com.stamp.dao;

import com.stamp.vo.MemberVO;;

public interface MyPageDao {

	public MemberVO memberInfo(String m_id);
	public int memberUpdate(MemberVO mvo);
	public int memberDelete(String m_id);
}

package com.stamp.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.stamp.vo.QuestionVO;

@Repository
public class QuestionDaoImpl implements QuestionDao {
	@Autowired
	private SqlSession session;

	@Override
	public int boardInsert(QuestionVO qvo) {
		// TODO Auto-generated method stub

		return session.insert("boardInsert", qvo);
	}

	@Override
	public List<QuestionVO> boardList(QuestionVO qvo) {
		// TODO Auto-generated method stub
		return session.selectList("boardList", qvo);
	}

	@Override
	public QuestionVO boardDetail(QuestionVO qvo) {
		// TODO Auto-generated method stub
		return (QuestionVO) session.selectOne("questionDetail", qvo);
	}

	@Override
	public int boardUpdate(QuestionVO qvo) {
		// TODO Auto-generated method stub
		return session.update("boardUpdate", qvo);
	}

	@Override
	public int boardDelete(int q_no) {
		// TODO Auto-generated method stub
		return session.update("boardDelete", q_no);
	}

	@Override
	public int countUpdate(int q_no) {
		// TODO Auto-generated method stub
		return session.update("countUpdate", q_no);
	}

	@Override
	public List<QuestionVO> scrollDown(QuestionVO qvo) {
		// TODO Auto-generated method stub
		return session.selectList("scrollDown", qvo);
	}

	@Override
	public List<QuestionVO> mainQuestionList(QuestionVO qvo) {
		return session.selectList("mainQuestionList", qvo);
	}

	@Override
	public List<QuestionVO> questionList(QuestionVO qvo) {
		return session.selectList("questionList", qvo);
	}

	@Override
	public int questionListCnt(QuestionVO qvo) {
		return session.selectOne("questionListCnt", qvo);
	}

	@Override
	public int questionCount(QuestionVO qvo) {
		return session.selectOne("questionCount", qvo);
	}

	@Override
	public List<QuestionVO> questionSubjectChart() {
		return session.selectList("questionSubjectChart");
	}

	@Override
	public List<QuestionVO> questionChart() {
		return session.selectList("questionChart");
	}

}

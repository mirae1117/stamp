package com.stamp.vo;

import org.springframework.web.multipart.MultipartFile;

public class ReplyVO {
	private int rep_no;
	private String rep_content;
	private String m_id;
	private String m_name;
	private int q_no;
	private MultipartFile file;
	private String rep_file;
	private String rep_time;
	private int rep_state;

	public ReplyVO() {
		this(0, null, null, null, 0, null, null, null, 2);
	}

	public ReplyVO(int rep_no, String rep_content, String m_id, String m_name, int q_no, MultipartFile file, String rep_file,
			String rep_time, int rep_state) {
		super();
		this.rep_no = rep_no;
		this.rep_content = rep_content;
		this.m_id = m_id;
		this.m_name = m_name;
		this.q_no = q_no;
		this.file = file;
		this.rep_file = rep_file;
		this.rep_time = rep_time;
		this.rep_state = rep_state;
	}

	public int getRep_no() {
		return rep_no;
	}

	public void setRep_no(int rep_no) {
		this.rep_no = rep_no;
	}

	public String getRep_content() {
		return rep_content;
	}

	public void setRep_content(String rep_content) {
		this.rep_content = rep_content;
	}

	public String getM_id() {
		return m_id;
	}

	public void setM_id(String m_id) {
		this.m_id = m_id;
	}

	public String getM_name() {
		return m_name;
	}

	public void setM_name(String m_name) {
		this.m_name = m_name;
	}
	public int getQ_no() {
		return q_no;
	}

	public void setQ_no(int q_no) {
		this.q_no = q_no;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getRep_file() {
		return rep_file;
	}

	public void setRep_file(String rep_file) {
		this.rep_file = rep_file;
	}

	public String getRep_time() {
		return rep_time;
	}

	public void setRep_time(String rep_time) {
		this.rep_time = rep_time;
	}

	public int getRep_state() {
		return rep_state;
	}

	public void setRep_state(int rep_state) {
		this.rep_state = rep_state;
	}

}

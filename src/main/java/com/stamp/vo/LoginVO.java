package com.stamp.vo;

public class LoginVO {

	private String log_id;
	private String log_pw;
	private int log_state1;

	public LoginVO(String log_id, String log_pw, int log_state1) {
		this.log_id = log_id;
		this.log_pw = log_pw;
		this.log_state1 = log_state1;
	}

	public LoginVO() {
		this("", "", 0);
	}

	public String getLog_id() {
		return log_id;
	}

	public void setLog_id(String log_id) {
		this.log_id = log_id;
	}

	public String getLog_pw() {
		return log_pw;
	}

	public void setLog_pw(String log_pw) {
		this.log_pw = log_pw;
	}

	@Override
	public String toString() {
		return "LoginVO [log_id=" + log_id + ", log_pw=" + log_pw + ", log_state1=" + log_state1 + "]";
	}

	public int getLog_state1() {
		return log_state1;
	}

	public void setLog_state1(int log_state1) {
		this.log_state1 = log_state1;
	}
}

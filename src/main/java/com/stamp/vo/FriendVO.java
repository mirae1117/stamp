package com.stamp.vo;

public class FriendVO {
	private int f_no;
	private String f_time;
	private int f_state;
	private String m_receiver;
	private String m_sender;

	public FriendVO(int f_no, String f_time, int f_state, String m_receiver, String m_sender) {
		super();
		this.f_no = f_no;
		this.f_time = f_time;
		this.f_state = f_state;
		this.m_receiver = m_receiver;
		this.m_sender = m_sender;
	}

	public FriendVO() {
		this(0, null, 0, "", "");
	}

	public int getF_no() {
		return f_no;
	}

	public void setF_no(int f_no) {
		this.f_no = f_no;
	}

	public String getF_time() {
		return f_time;
	}

	public void setF_time(String f_time) {
		this.f_time = f_time;
	}

	public int getF_state() {
		return f_state;
	}

	public void setF_state(int f_state) {
		this.f_state = f_state;
	}

	public String getM_receiver() {
		return m_receiver;
	}

	public void setM_receiver(String m_receiver) {
		this.m_receiver = m_receiver;
	}

	public String getM_sender() {
		return m_sender;
	}

	public void setM_sender(String m_sender) {
		this.m_sender = m_sender;
	}

}

package com.stamp.vo;

public class PointVO {
	private int p_no;
	private String m_id;
	private int p_point;
	private int p_state;
	private String p_detail;
	private String p_time;

	public PointVO(int p_no, String m_id, int p_point, int p_state, String p_detail, String p_time) {
		this.p_no = p_no;
		this.m_id = m_id;
		this.p_point = p_point;
		this.p_state = p_state;
		this.p_detail = p_detail;
		this.p_time = p_time;
	}

	public PointVO() {
		this(0, "", 0, 0, "", null);
	}

	public int getP_no() {
		return p_no;
	}

	public void setP_no(int p_no) {
		this.p_no = p_no;
	}

	public String getM_id() {
		return m_id;
	}

	public void setM_id(String m_id) {
		this.m_id = m_id;
	}

	public int getP_point() {
		return p_point;
	}

	public void setP_point(int p_point) {
		this.p_point = p_point;
	}

	public int getP_state() {
		return p_state;
	}

	public void setP_state(int p_state) {
		this.p_state = p_state;
	}

	public String getP_detail() {
		return p_detail;
	}

	public void setP_detail(String p_detail) {
		this.p_detail = p_detail;
	}

	public String getP_time() {
		return p_time;
	}

	public void setP_time(String p_time) {
		this.p_time = p_time;
	}

}

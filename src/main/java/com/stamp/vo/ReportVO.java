package com.stamp.vo;

public class ReportVO {
	private int re_no;
	private String re_title;
	private String re_content;
	private String M_id;
	private String re_state;
	private int re_sort;
	private String re_suspect;
	private String re_time;

	public ReportVO() {
		super();
	}

	public ReportVO(int re_no, String re_title, String re_content, String m_id, String re_state, int re_sort,
			String re_suspect, String re_time) {
		super();
		this.re_no = re_no;
		this.re_title = re_title;
		this.re_content = re_content;
		M_id = m_id;
		this.re_state = re_state;
		this.re_sort = re_sort;
		this.re_suspect = re_suspect;
		this.re_time = re_time;
	}

	public int getRe_no() {
		return re_no;
	}

	public void setRe_no(int re_no) {
		this.re_no = re_no;
	}

	public String getRe_title() {
		return re_title;
	}

	public void setRe_title(String re_title) {
		this.re_title = re_title;
	}

	public String getRe_content() {
		return re_content;
	}

	public void setRe_content(String re_content) {
		this.re_content = re_content;
	}

	public String getM_id() {
		return M_id;
	}

	public void setM_id(String m_id) {
		M_id = m_id;
	}

	public String getRe_state() {
		return re_state;
	}

	public void setRe_state(String re_state) {
		this.re_state = re_state;
	}

	public int getRe_sort() {
		return re_sort;
	}

	public void setRe_sort(int re_sort) {
		this.re_sort = re_sort;
	}

	public String getRe_suspect() {
		return re_suspect;
	}

	public void setRe_suspect(String re_suspect) {
		this.re_suspect = re_suspect;
	}

	public String getRe_time() {
		return re_time;
	}

	public void setRe_date(String re_time) {
		this.re_time = re_time;
	}

}
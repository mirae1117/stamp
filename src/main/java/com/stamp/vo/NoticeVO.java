package com.stamp.vo;

import org.springframework.web.multipart.MultipartFile;

import com.stamp.common.PagingVO;

public class NoticeVO extends PagingVO {
	private int nt_no;       //공지 일련번호
	private String m_id;      //작성자 아이디
	private String nt_title;  //공지사항 제목
	private String nt_content;  //공지사항 내용
	private int nt_count;  //조회수
	private String nt_time;  //올린시간
	
	//파일 업로드를 위한 속성
		private MultipartFile file;//첨부 파일
		private String n_file=""; //실제 서버에 저장한 파일 명
		private String c_menu="";

	public NoticeVO(int nt_no, String m_id, String nt_title, String nt_content, int nt_count, String nt_time) {
		this.nt_no = nt_no;
		this.m_id = m_id;
		this.nt_title = nt_title;
		this.nt_content = nt_content;
		this.nt_count = nt_count;
		this.nt_time = nt_time;
	}

	public NoticeVO() {
		this(0, "", "", "", 0, null);
	}

	public int getNt_no() {
		return nt_no;
	}

	public void setNt_no(int nt_no) {
		this.nt_no = nt_no;
	}

	public String getM_id() {
		return m_id;
	}

	public void setM_id(String m_id) {
		this.m_id = m_id;
	}

	public String getNt_title() {
		return nt_title;
	}

	public void setNt_title(String nt_title) {
		this.nt_title = nt_title;
	}

	public String getNt_content() {
		return nt_content;
	}

	public void setNt_content(String nt_content) {
		this.nt_content = nt_content;
	}

	public int getNt_count() {
		return nt_count;
	}

	public void setNt_count(int nt_count) {
		this.nt_count = nt_count;
	}

	public String getNt_time() {
		return nt_time;
	}

	public void setNt_time(String nt_time) {
		this.nt_time = nt_time;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getN_file() {
		return n_file;
	}

	public void setN_file(String n_file) {
		this.n_file = n_file;
	}

	public String getC_menu() {
		return c_menu;
	}

	public void setC_menu(String c_menu) {
		this.c_menu = c_menu;
	}

	
}

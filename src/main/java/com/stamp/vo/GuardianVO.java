package com.stamp.vo;

public class GuardianVO {
	private int g_no;
	private String m_id;
	private String g_key;
	private String g_email;
	private int g_state;

	public GuardianVO(int g_no, String m_id, String g_key, String g_email, int g_state) {
		this.g_no = g_no;
		this.m_id = m_id;
		this.g_key = g_key;
		this.g_email = g_email;
		this.g_state = g_state;
	}

	public GuardianVO() {
		this(0, "", "", "", 0);
	}

	public int getG_no() {
		return g_no;
	}

	public void setG_no(int g_no) {
		this.g_no = g_no;
	}

	public String getM_id() {
		return m_id;
	}

	public void setM_id(String m_id) {
		this.m_id = m_id;
	}

	public String getG_key() {
		return g_key;
	}

	public void setG_key(String g_key) {
		this.g_key = g_key;
	}

	public String getG_email() {
		return g_email;
	}

	public void setG_email(String g_email) {
		this.g_email = g_email;
	}

	public int getG_state() {
		return g_state;
	}

	public void setG_state(int g_state) {
		this.g_state = g_state;
	}

}

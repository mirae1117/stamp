package com.stamp.vo;

import org.springframework.web.multipart.MultipartFile;

import com.stamp.common.PagingVO;

public class MemberVO extends PagingVO{
	private String m_id;
	private String m_name;
	private String m_pw;
	private String m_email;
	private int m_grade;
	private String m_region;
	private String m_sex;
	private String m_pr;
	private MultipartFile file; // 프로필 첨부 파일
	private String m_profile; // 프로필 첨부 파일명
	private int m_state1;
	private int m_state2;
	private int m_private;
	private String m_birth;
	private String m_joindate;
	
	//조건검색시 사용할 필드  
	private String search = "";  
	private String keyword = ""; 
	
	//열제목 클릭시 정렬을 위한 필드  
	private String order_by ;  
	private String order_sc ; 

	
	public MemberVO() {
		this("", "", "", "", 0, "", "", "", "", 0, 0, 0, null, null);
	}

	public MemberVO(String m_id, String m_name, String m_pw, String m_email, int m_grade, String m_region, String m_sex,
			String m_pr, String m_profile, int m_state1, int m_state2, int m_private,
			String m_birth, String m_joindate) {
		this.m_id = m_id;
		this.m_name = m_name;
		this.m_pw = m_pw;
		this.m_email = m_email;
		this.m_grade = m_grade;
		this.m_region = m_region;
		this.m_sex = m_sex;
		this.m_pr = m_pr;
		this.m_profile = m_profile;
		this.m_state1 = m_state1;
		this.m_state2 = m_state2;
		this.m_private = m_private;
		this.m_birth = m_birth;
		this.m_joindate = m_joindate;
	}

	public String getM_id() {
		return m_id;
	}

	public void setM_id(String m_id) {
		this.m_id = m_id;
	}

	public String getM_name() {
		return m_name;
	}

	public void setM_name(String m_name) {
		this.m_name = m_name;
	}

	public String getM_pw() {
		return m_pw;
	}

	public void setM_pw(String m_pw) {
		this.m_pw = m_pw;
	}

	public String getM_email() {
		return m_email;
	}

	public void setM_email(String m_email) {
		this.m_email = m_email;
	}

	public int getM_grade() {
		return m_grade;
	}

	public void setM_grade(int m_grade) {
		this.m_grade = m_grade;
	}

	public String getM_region() {
		return m_region;
	}

	public void setM_region(String m_region) {
		this.m_region = m_region;
	}

	public String getM_sex() {
		return m_sex;
	}

	public void setM_sex(String m_sex) {
		this.m_sex = m_sex;
	}

	public String getM_pr() {
		return m_pr;
	}

	public void setM_pr(String m_pr) {
		this.m_pr = m_pr;
	}

	public String getM_profile() {
		return m_profile;
	}

	public void setM_profile(String m_profile) {
		this.m_profile = m_profile;
	}

	public int getM_state1() {
		return m_state1;
	}

	public void setM_state1(int m_state1) {
		this.m_state1 = m_state1;
	}

	public int getM_state2() {
		return m_state2;
	}

	public void setM_state2(int m_state2) {
		this.m_state2 = m_state2;
	}

	public int getM_private() {
		return m_private;
	}

	public void setM_private(int m_private) {
		this.m_private = m_private;
	}

	public String getM_joindate() {
		return m_joindate;
	}

	public void setM_joindate(String m_joindate) {
		this.m_joindate = m_joindate;
	}

	public String getM_birth() {
		return m_birth;
	}

	public void setM_birth(String m_birth) {
		this.m_birth = m_birth;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getOrder_by() {
		return order_by;
	}

	public void setOrder_by(String order_by) {
		this.order_by = order_by;
	}

	public String getOrder_sc() {
		return order_sc;
	}

	public void setOrder_sc(String order_sc) {
		this.order_sc = order_sc;
	}
	

}

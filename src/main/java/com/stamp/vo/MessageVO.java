package com.stamp.vo;

public class MessageVO {
	private int msg_no;
	private String msg_title;
	private String msg_content;
	private String msg_sendtime;
	private String msg_receivetime;
	private String m_sender;
	private String m_senderName;
	private String m_receiver;
	private String m_receiverName;
	private int msg_state;
	public MessageVO(int msg_no, String msg_title, String msg_content, String msg_sendtime, String msg_receivetime,
			String m_sender, String m_senderName, String m_receiver, String m_receiverName, int msg_state) {
		this.msg_no = msg_no;
		this.msg_title = msg_title;
		this.msg_content = msg_content;
		this.msg_sendtime = msg_sendtime;
		this.msg_receivetime = msg_receivetime;
		this.m_sender = m_sender;
		this.m_senderName = m_senderName;
		this.m_receiver = m_receiver;
		this.m_receiverName = m_receiverName;
		this.msg_state = msg_state;
	}
	public String getM_senderName() {
		return m_senderName;
	}
	public void setM_senderName(String m_senderName) {
		this.m_senderName = m_senderName;
	}
	public String getM_receiverName() {
		return m_receiverName;
	}
	public void setM_receiverName(String m_receiverName) {
		this.m_receiverName = m_receiverName;
	}
	public MessageVO() {
		this(0,"","",null,null,"","","","",0);
	}
	public int getMsg_no() {
		return msg_no;
	}
	public void setMsg_no(int msg_no) {
		this.msg_no = msg_no;
	}
	public String getMsg_title() {
		return msg_title;
	}
	public void setMsg_title(String msg_title) {
		this.msg_title = msg_title;
	}
	public String getMsg_content() {
		return msg_content;
	}
	public void setMsg_content(String msg_content) {
		this.msg_content = msg_content;
	}
	public String getMsg_sendtime() {
		return msg_sendtime;
	}
	public void setMsg_sendtime(String msg_sendtime) {
		this.msg_sendtime = msg_sendtime;
	}
	public String getMsg_receivetime() {
		return msg_receivetime;
	}
	public void setMsg_receivetime(String msg_receivetime) {
		this.msg_receivetime = msg_receivetime;
	}
	public String getM_sender() {
		return m_sender;
	}
	public void setM_sender(String m_sender) {
		this.m_sender = m_sender;
	}
	public String getM_receiver() {
		return m_receiver;
	}
	public void setM_receiver(String m_receiver) {
		this.m_receiver = m_receiver;
	}
	public int getMsg_state() {
		return msg_state;
	}
	public void setMsg_state(int msg_state) {
		this.msg_state = msg_state;
	}
	
	
}

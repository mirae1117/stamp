package com.stamp.vo;

import org.springframework.web.multipart.MultipartFile;

import com.stamp.common.PagingVO;

public class QuestionVO extends PagingVO {
	private int q_no;
	private String q_title;
	private String q_content;
	private String m_id;
	private String m_name;
	private int q_subject;
	private int q_grade;
	private int q_count;
	private MultipartFile file;
	private String q_file;
	private int q_state;
	private String q_time;

	// 조건검색시 사용할 필드
	private String search = "";
	private String keyword = "";

	// 열제목 클릭시 정렬을 위한 필드
	private String order_by;
	private String order_sc;

	public QuestionVO() {
		this(0, "", "", "","", 0, -1, 0, null, "", 0, "");
	}

	public QuestionVO(int q_no, String q_title, String m_name, String q_content, String m_id, int q_subject, int q_grade, int q_count,
			MultipartFile file, String q_file, int q_state, String q_time) {
		super();
		this.q_no = q_no;
		this.q_title = q_title;
		this.q_content = q_content;
		this.m_id = m_id;
		this.m_name = m_name;
		this.q_subject = q_subject;
		this.q_grade = q_grade;
		this.q_count = q_count;
		this.file = file;
		this.q_file = q_file;
		this.q_state = q_state;
		this.q_time = q_time;
	}

	public int getQ_no() {
		return q_no;
	}

	public void setQ_no(int q_no) {
		this.q_no = q_no;
	}

	public String getQ_title() {
		return q_title;
	}

	public void setQ_title(String q_title) {
		this.q_title = q_title;
	}

	public String getQ_content() {
		return q_content;
	}

	public void setQ_content(String q_content) {
		this.q_content = q_content;
	}

	public String getM_id() {
		return m_id;
	}

	public void setM_id(String m_id) {
		this.m_id = m_id;
	}

	public String getM_name() {
		return m_name;
	}

	public void setM_name(String m_name) {
		this.m_name = m_name;
	}
	public int getQ_subject() {
		return q_subject;
	}

	public void setQ_subject(int q_subject) {
		this.q_subject = q_subject;
	}

	public int getQ_grade() {
		return q_grade;
	}

	public void setQ_grade(int q_grade) {
		this.q_grade = q_grade;
	}

	public int getQ_count() {
		return q_count;
	}

	public void setQ_count(int q_count) {
		this.q_count = q_count;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getQ_file() {
		return q_file;
	}

	public void setQ_file(String q_file) {
		this.q_file = q_file;
	}

	public int getQ_state() {
		return q_state;
	}

	public void setQ_state(int q_state) {
		this.q_state = q_state;
	}

	public String getQ_time() {
		return q_time;
	}

	public void setQ_time(String q_time) {
		this.q_time = q_time;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getOrder_by() {
		return order_by;
	}

	public void setOrder_by(String order_by) {
		this.order_by = order_by;
	}

	public String getOrder_sc() {
		return order_sc;
	}

	public void setOrder_sc(String order_sc) {
		this.order_sc = order_sc;
	}

}
package com.stamp.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.MessageDao;
import com.stamp.vo.MessageVO;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {

	Logger logger = Logger.getLogger(MessageServiceImpl.class);

	@Autowired
	private MessageDao messageDao;

	@Override
	public List<MessageVO> sendMessageList(String m_id) {
		return messageDao.sendMessageList(m_id);
	}

	@Override
	public List<MessageVO> getMessageList(String m_id) {
		return messageDao.getMessageList(m_id);
	}

	@Override
	public MessageVO messageDetail(MessageVO msgvo) {
		return messageDao.messageDetail(msgvo);
	}

	@Override
	public int messageChk(int msg_no) {
		return messageDao.messageChk(msg_no);
	}

	@Override
	public int messageInsert(MessageVO msgvo) {
		return messageDao.messageInsert(msgvo);
	}

	//쪽지삭제
	//상태번호 0:정상 9:운영자삭제 8:양쪽삭제 7:보낸사람만 삭제 6:받은사람만 삭제
	
	@Override
	public int messageDelete8(MessageVO msgvo) {
		return messageDao.messageDelete8(msgvo);
	}

	@Override
	public int messageDelete7(MessageVO msgvo) {
		if (messageDao.messageDetail(msgvo).getMsg_receivetime() == null || messageDao.messageDetail(msgvo).getMsg_receivetime() == "") {
			return messageDelete8(msgvo);
		}
		else if (messageDao.messageState(msgvo) == 6) {
			return messageDelete8(msgvo);
		}
		return messageDao.messageDelete7(msgvo);
	}

	@Override
	public int messageDelete6(MessageVO msgvo) {
		if (messageDao.messageState(msgvo) == 7) {
			return messageDelete8(msgvo);
		}
		return messageDao.messageDelete6(msgvo);
	}

	@Override
	public int getMessageCount(String log_id) {
		return messageDao.getMessageCount(log_id);
	}


}

package com.stamp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.FriendDao;
import com.stamp.vo.FriendVO;
import com.stamp.vo.MemberVO;
@Service
@Transactional
public class FriendServiceImpl implements FriendService {

	@Autowired
	private FriendDao friendDao;
	
	@Override
	public List<MemberVO> memberSelect(MemberVO mvo) {
		return friendDao.memberSelect(mvo);
	}

	@Override
	public int callFriendBeforeChk(FriendVO fvo) {
		return friendDao.callFriendBeforeChk(fvo);
	}
	@Override
	public int callFriend(FriendVO fvo) {
		return friendDao.callFriend(fvo);
	}

	@Override
	public List<MemberVO> serveCall(String m_id) {
		return friendDao.serveCall(m_id);
	}

	@Override
	public List<MemberVO> receiveCall(String m_id) {
		return friendDao.receiveCall(m_id);
	}

	@Override
	public int unCallFriend(FriendVO fvo) {
		return friendDao.unCallFriend(fvo);
	}

	@Override
	public int consentCallFriend(FriendVO fvo) {
		return friendDao.consentCallFriend(fvo);
	}

	@Override
	public List<MemberVO> listFriend(String m_id) {
		return friendDao.listFriend(m_id);
	}

	@Override
	public int deleteFriend(FriendVO fvo) {
		return friendDao.deleteFriend(fvo);
	}


}

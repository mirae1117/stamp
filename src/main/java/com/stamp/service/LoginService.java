package com.stamp.service;

import com.stamp.vo.LoginVO;

public interface LoginService {

	public int loginMember(LoginVO lvo);

}

package com.stamp.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.PointDao;
import com.stamp.vo.PointVO;

@Service
@Transactional
public class PointServiceImpl implements PointService {

	Logger logger = Logger.getLogger(PointServiceImpl.class);

	@Autowired
	private PointDao pointDao;

	@Override
	public int insertJoinPoint(String m_id) {
		return pointDao.insertJoinPoint(m_id);
	}

	@Override
	public void insertAttendPoint(String log_id) {
		if (pointDao.attendChk(log_id) == 0) {
			pointDao.insertAttendPoint(log_id);
		} 
	}

	@Override
	public String lastAttendDay(String m_id) {
		return pointDao.lastAttendDay(m_id);
	}

	@Override
	public int memberPoints(String m_id) {
		return pointDao.memberPoints(m_id);
	}

	@Override
	public List<PointVO> listPoint(String m_id) {
		return pointDao.listPoint(m_id);
	}

	@Override
	public int insertQuestionPoint(PointVO pvo) {
		return 	pointDao.insertQuestionPoint(pvo);
	}

	@Override
	public int chargePoint(PointVO pvo) {
		if (pvo.getP_detail().length()>20) {
			pvo.setP_detail("충전 내용 : "+ pvo.getP_detail().substring(0, 20));
		}else{
		pvo.setP_detail("충전 내용 : "+ pvo.getP_detail());
		}
		return 	pointDao.chargePoint(pvo);
	}

	@Override
	public int visitantCount(PointVO pvo) {
		return pointDao.visitantCount(pvo);
	}

	@Override
	public List<PointVO> pointChart() {
		return pointDao.pointChart();
	}

	@Override
	public int insertReplyPoint(PointVO pvo) {
		return pointDao.insertReplyPoint(pvo);
	}

}

package com.stamp.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.vo.MemberVO;

@Service
@Transactional
public interface MyPageService {
	public MemberVO memberInfo(String m_id);
	public int memberUpdate(MemberVO mvo);
	public int memberDelete(String m_id);
}

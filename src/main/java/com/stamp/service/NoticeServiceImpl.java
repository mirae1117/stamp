package com.stamp.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.NoticeDao;
import com.stamp.vo.NoticeVO;

@Service
@Transactional
public class NoticeServiceImpl implements NoticeService {

	Logger logger=Logger.getLogger(NoticeServiceImpl.class);
	
	@Autowired
	private NoticeDao noticeDao;
	
	
	//글목록구현ㅇ
	@Override
	public List<NoticeVO> noticeList(NoticeVO nvo) {
		// TODO Auto-generated method stub
		List<NoticeVO>myList=null;
		myList=noticeDao.noticeList(nvo);
		return myList;
	}

	//전체 레코드 구현
	@Override
	public int noticeListCnt(NoticeVO nvo) {
		// TODO Auto-generated method stub
		return noticeDao.noticeListCnt(nvo);
	}

	//글입력 구현
	@Override
	public int noticeInsert(NoticeVO nvo) {
		// TODO Auto-generated method stub
		int result=0;
		result =noticeDao.noticeInsert(nvo);
		return result;
	}

	//글 상세 구현
	@Override
	public NoticeVO noticeDetail(NoticeVO nvo) {
		// TODO Auto-generated method stub
		NoticeVO detail=null;
		detail =noticeDao.noticeDetail(nvo);
		return detail;
	}

	//글 수정 구현
	@Override
	public int noticeUpdate(NoticeVO nvo) {
		// TODO Auto-generated method stub
		int result=0;
		result=noticeDao.noticeUpdate(nvo);
		return result;
	}

	@Override
	public int noticeDelete(int nt_no) {
		// TODO Auto-generated method stub
		int result=0;
		result =noticeDao.noticeDelete(nt_no);
		return result;
	}

	@Override
	public int countUpdate(int nt_no) {
		// TODO Auto-generated method stub
		return noticeDao.countUpdate(nt_no);
	}


}

package com.stamp.service;



import java.util.List;

//import javax.mail.MessagingException;

//import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.JoinDao;
import com.stamp.vo.GuardianVO;
import com.stamp.vo.MemberVO;

@Service
@Transactional
public class JoinServiceImpl implements JoinService {

	Logger logger = Logger.getLogger(JoinServiceImpl.class);

	@Autowired
	private JoinDao joinDao;

	// 법정대리인 인증번호 생성, 값저장
	@Override
	public int guardianAttestation(GuardianVO gvo) {
		int result = 0;
		joinDao.guardianErasure(gvo);
		result = joinDao.guardianAttestation(gvo);
		return result;
	}

	@Override
	public int guardianCertify(GuardianVO gvo) {
		int result = 0;
		result = joinDao.guardianCertify(gvo);
		return result;
	}

	// 보호자 인증키 발급
	@Override
	public String outputKey(int num) {
		char[] outputChar = new char[num];
		String outputString = "";
		for (int i = 0; i < outputChar.length; i++) {
			int temp = (int) (Math.random() * 36);
			if (temp < 10) {
				outputChar[i] = (char) (temp + 48);
			} else {
				outputChar[i] = (char) (temp + 55);
			}
			outputString += outputChar[i];
		}
		return outputString;
	}

	//회원 가입
	@Override
	public int joinMember(MemberVO mvo) {
		int result=0;
		String bcPass = BCrypt.hashpw(mvo.getM_pw(), BCrypt.gensalt());
		mvo.setM_pw(bcPass);
		result =joinDao.memberInsert(mvo);
		return result;
	}

	//어린이 회원 가입시 법정대리인 저장
	@Override
	public int saveGuardian(GuardianVO gvo) {
		int result=0;
		result =joinDao.saveGuardian(gvo);
		return result;
	
	}

	//중복확인
	@Override
	public int memberChk(String m_id) {
		return joinDao.memberChk(m_id);
	}

	//id찾기
	@Override
	public List<String> idFind(String m_email) {
		return joinDao.idFind(m_email);
	}

	//아이디&이메일 일치 검사
	@Override
	public int pwFindIdEmailChk(MemberVO mvo) {
		return joinDao.pwFindIdEmailChk(mvo);
	}
	//비밀번호 찾기 인증키 보내기
	@Override
	public int pwFindAgreeInsert(GuardianVO gvo) {
		return joinDao.pwFindAgreeInsert(gvo);
	}

	//비밀번호 찾기 인증 작업
	@Override
	public int pwFindCertify(GuardianVO gvo) {
		return joinDao.pwFindCertify(gvo);
	}
	//비밀번호 찾기시 인증키와 이메일로 아이디 다시 불러오기
	@Override
	public String pwFindGetId(GuardianVO gvo) {
		return joinDao.pwFindGetId(gvo);
	}

	// 분실 비밀번호 변경 최종단계	
	@Override
	public int pwFindChange(MemberVO mvo) {
		int result=0;
		String bcPass = BCrypt.hashpw(mvo.getM_pw(), BCrypt.gensalt());
		mvo.setM_pw(bcPass);
		result =joinDao.pwFindChange(mvo);
		return result;
	}


}

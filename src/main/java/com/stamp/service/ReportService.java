package com.stamp.service;

import com.stamp.vo.ReportVO;

public interface ReportService {

	public int reportInsert(ReportVO rvo);

	public int countReport(ReportVO rvo);

	public void reportUpdate(String no);
}

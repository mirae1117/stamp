package com.stamp.service;

import java.util.List;

import com.stamp.vo.MemberVO;

public interface AdminManageService {

	public List<MemberVO> manageList(MemberVO mvo);
	public MemberVO manageDetail(MemberVO mvo);
	public int manageListCnt(MemberVO mvo);
	public List<MemberVO> memberJoinChart();
	public List<MemberVO> memberGenderChart();
	public List<MemberVO> memberGradeChart();
	public int memberCount(MemberVO mvo);
}

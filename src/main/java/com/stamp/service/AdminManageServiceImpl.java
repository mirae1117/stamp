package com.stamp.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.AdminManageDao;
import com.stamp.vo.MemberVO;

@Service
@Transactional
public class AdminManageServiceImpl implements AdminManageService {

	Logger logger = Logger.getLogger(AdminNoticeServiceImpl.class);
	
	@Autowired
	private AdminManageDao manageDao;
	
	@Override
	public List<MemberVO> manageList(MemberVO mvo) {
		List<MemberVO> myList = new ArrayList<MemberVO>();
		myList = manageDao.manageList(mvo);
		return myList;
	}

	@Override
	public MemberVO manageDetail(MemberVO mvo) {
		MemberVO detail = null;
		detail = manageDao.manageDetail(mvo);
		return detail;
	}

	@Override
	public int manageListCnt(MemberVO mvo) {
		return manageDao.manageListCnt(mvo);
	}

	@Override
	public List<MemberVO> memberJoinChart() {
		return manageDao.memberJoinChart();
	}

	@Override
	public List<MemberVO> memberGenderChart() {
		return manageDao.memberGenderChart();
	}

	@Override
	public List<MemberVO> memberGradeChart() {
		return manageDao.memberGradeChart();
	}

	@Override
	public int memberCount(MemberVO mvo) {
		return manageDao.memberCount(mvo);
	}

}

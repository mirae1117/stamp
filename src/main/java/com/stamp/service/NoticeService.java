package com.stamp.service;

import java.util.List;

import com.stamp.vo.NoticeVO;

public interface NoticeService {
//ㅇ
	public List<NoticeVO>noticeList(NoticeVO nvo);
	public int noticeListCnt(NoticeVO nvo);
	public int noticeInsert(NoticeVO nvo);
	public NoticeVO noticeDetail(NoticeVO nvo);
	public int noticeUpdate(NoticeVO nvo);
	public int noticeDelete(int nt_no);
	public int countUpdate(int nt_no);
}

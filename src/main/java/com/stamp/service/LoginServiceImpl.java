package com.stamp.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.LoginDao;
import com.stamp.vo.LoginVO;

@Service
@Transactional
public class LoginServiceImpl implements LoginService {

	Logger logger = Logger.getLogger(LoginServiceImpl.class);

	@Autowired
	private LoginDao loginDao;
	//로그인 버튼 클릭시
	@Override
	public int loginMember(LoginVO lvo) {
		int result=0;
		result =loginDao.memberLogin(lvo);
		return result;
	}
}

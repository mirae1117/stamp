package com.stamp.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.QuestionDao;
import com.stamp.vo.QuestionVO;

@Service
@Transactional
public class QuestionServiceImpl implements QuestionService {
	Logger logger = Logger.getLogger(QuestionServiceImpl.class);

	@Autowired
	private QuestionDao questionDao;

	@Override
	public int boardInsert(QuestionVO qvo) {
		int result = 0;
		result = questionDao.boardInsert(qvo);
		return result;
	}

	@Override
	public List<QuestionVO> boardList(QuestionVO qvo) {
		List<QuestionVO> myList = null;
		myList = questionDao.boardList(qvo);
		return myList;
	}

	@Override
	public QuestionVO boardDetail(QuestionVO qvo) {
		// TODO Auto-generated method stub
		QuestionVO detail = null;
		detail = questionDao.boardDetail(qvo);
		return detail;
	}

	@Override
	public int boardUpdate(QuestionVO qvo) {
		// TODO Auto-generated method stub
		int result = 0;
		result = questionDao.boardUpdate(qvo);
		return result;
	}

	@Override
	public int boardDelete(int q_no) {
		// TODO Auto-generated method stub
		int result = 0;
		result = questionDao.boardDelete(q_no);
		return result;
	}

	@Override
	public int countUpdate(int q_no) {
		// TODO Auto-generated method stub
		int result = 0;
		result = questionDao.countUpdate(q_no);
		return result;
	}

	@Override
	public List<QuestionVO> scrollDown(QuestionVO qvo) {
		// TODO Auto-generated method stub
		List<QuestionVO> list = null;
		list = questionDao.scrollDown(qvo);
		return list;
	}

	@Override
	public List<QuestionVO> mainQuestionList(QuestionVO qvo) {
		return questionDao.mainQuestionList(qvo);
	}

	@Override
	public List<QuestionVO> questionList(QuestionVO qvo) {
		List<QuestionVO> questionList = null;
		questionList = questionDao.questionList(qvo);
		return questionList;
	}

	@Override
	public int questionListCnt(QuestionVO qvo) {
		return questionDao.questionListCnt(qvo);
	}

	@Override
	public int questionCount(QuestionVO qvo) {
		return questionDao.questionCount(qvo);
	}

	@Override
	public List<QuestionVO> questionSubjectChart() {
		return questionDao.questionSubjectChart();
	}

	@Override
	public List<QuestionVO> questionChart() {
		return questionDao.questionChart();
	}

}

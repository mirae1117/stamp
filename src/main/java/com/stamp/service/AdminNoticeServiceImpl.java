package com.stamp.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.AdminNoticeDao;
import com.stamp.vo.NoticeVO;

@Service
@Transactional
public class AdminNoticeServiceImpl implements AdminNoticeService {
	Logger logger = Logger.getLogger(AdminNoticeServiceImpl.class);
	
	@Autowired
	private AdminNoticeDao noticeDao;
	
	//글 목록 구현
	@Override
	public List<NoticeVO> noticeList(NoticeVO nvo) {
		List<NoticeVO> myList = new ArrayList<NoticeVO>();
		myList = noticeDao.noticeList(nvo);
		return myList;
	}

	//글 등록 구현
	@Override
	public int noticeInsert(NoticeVO nvo) {
		int result = 0;
		result = noticeDao.noticeInsert(nvo);
		return result;
	}

	//글 상세 구현
	@Override
	public NoticeVO noticeDetail(NoticeVO nvo) {
		NoticeVO detail = null;
		detail = noticeDao.noticeDetail(nvo);
		return detail;
	}

	//글 수정 구현
	@Override
	public int noticeUpdate(NoticeVO nvo) {
		int result = 0;
		result = noticeDao.noticeUpdate(nvo);
		return result;
	}
	
	//글 삭제 구현
	@Override
	public int noticeDelete(int nt_no) {
		int retult = 0;
		retult = noticeDao.noticeDelete(nt_no);
		return retult;
	}

	//전체 레코드 건수 구현
	@Override
	public int noticeListCnt(NoticeVO nvo) {
		return noticeDao.noticeListCnt(nvo);
	}
	
	@Override
	public List<NoticeVO> mainNoticeList(NoticeVO nvo) {
		return noticeDao.mainNoticeList(nvo);
	}
	@Override
	public int countUpdate(int nt_no) {
		// TODO Auto-generated method stub
		return noticeDao.countUpdate(nt_no);
	}

}

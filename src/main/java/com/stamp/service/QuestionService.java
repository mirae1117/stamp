package com.stamp.service;

import java.util.List;

import com.stamp.vo.QuestionVO;

public interface QuestionService {

	public List<QuestionVO> boardList(QuestionVO qvo);

	public int boardInsert(QuestionVO qvo);

	public QuestionVO boardDetail(QuestionVO qvo);

	public int boardUpdate(QuestionVO qvo);

	public int boardDelete(int q_no);

	public int countUpdate(int q_no);

	public List<QuestionVO> scrollDown(QuestionVO qvo);

	//관리자 페이지
	public List<QuestionVO> mainQuestionList(QuestionVO qvo);
	
	public List<QuestionVO> questionList(QuestionVO qvo);
	
	public int questionListCnt(QuestionVO qvo);
	
	public int questionCount(QuestionVO qvo);
	
	public List<QuestionVO> questionSubjectChart();
	
	public List<QuestionVO> questionChart();
	
}

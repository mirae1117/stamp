package com.stamp.service;

import org.apache.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.MyPageDao;
import com.stamp.vo.MemberVO;

@Service
@Transactional
public class MyPageServiceImpl implements MyPageService {

	Logger logger = Logger.getLogger(LoginServiceImpl.class);
	
	@Autowired
	private MyPageDao myPageDao;

	@Override
	public MemberVO memberInfo(String m_id) {
		return myPageDao.memberInfo(m_id);
	}

	@Override
	public int memberUpdate(MemberVO mvo) {
		int result=0;
		String bcPass = BCrypt.hashpw(mvo.getM_pw(), BCrypt.gensalt());
		mvo.setM_pw(bcPass);
		result =myPageDao.memberUpdate(mvo);
		return result;
	}

	@Override
	public int memberDelete(String m_id) {
		return myPageDao.memberDelete(m_id);
	}
}

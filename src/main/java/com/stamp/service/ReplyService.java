package com.stamp.service;

import java.util.List;

import com.stamp.vo.ReplyVO;

public interface ReplyService {
	public int replyInsert(ReplyVO rvo);

	public List<ReplyVO> replyList(int q_no);
	
	public int replyUpdate(ReplyVO rvo);

	public int replyDelete(int rep_no);

	public void replySelect(int rep_no);

	public ReplyVO getReply(int rep_no);

	//public ReplyVO replyBest(int q_no);

	//public String replyDetail(int rep_no);
	
}

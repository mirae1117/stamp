package com.stamp.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.ReplyDao;
import com.stamp.vo.ReplyVO;

@Service
@Transactional
public class ReplyServiceImpl implements ReplyService {
	Logger logger = Logger.getLogger(ReplyServiceImpl.class);

	@Autowired
	private ReplyDao replyDao;

	@Override
	public int replyInsert(ReplyVO rvo) {
		// TODO Auto-generated method stub
		int result = 0;
		result = replyDao.replyInsert(rvo);
		return result;
	}

	@Override
	public List<ReplyVO> replyList(int q_no) {
		List<ReplyVO> myList = null;
		myList = replyDao.replyList(q_no);
		return myList;
	}

	@Override
	public int replyDelete(int rep_no) {
		// TODO Auto-generated method stub
		// replyDao.replyDelete(rep_no);
		return replyDao.replyDelete(rep_no);
	}

	@Override
	public int replyUpdate(ReplyVO rvo) {
		// TODO Auto-generated method stub
		return replyDao.replyUpdate(rvo);
	}

	@Override
	public void replySelect(int rep_no) {
		// TODO Auto-generated method stub
		replyDao.replySelect(rep_no);
	}

	@Override
	public ReplyVO getReply(int rep_no) {
		return replyDao.getReply(rep_no);
	}



}

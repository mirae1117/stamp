package com.stamp.service;

import java.util.List;

import com.stamp.vo.FriendVO;
import com.stamp.vo.MemberVO;

public interface FriendService {
	public List<MemberVO> memberSelect(MemberVO mvo);
	public int callFriendBeforeChk(FriendVO fvo);
	public int callFriend(FriendVO fvo);
	public List<MemberVO> serveCall(String m_id);
	public List<MemberVO> receiveCall(String m_id);
	public int unCallFriend(FriendVO fvo);
	public int consentCallFriend(FriendVO fvo);
	public List<MemberVO> listFriend(String m_id);
	public int deleteFriend(FriendVO fvo);
}

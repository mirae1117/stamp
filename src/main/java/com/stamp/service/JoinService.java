package com.stamp.service;

import java.util.List;

import com.stamp.vo.GuardianVO;
import com.stamp.vo.MemberVO;

public interface JoinService {
	public int guardianAttestation(GuardianVO gvo);
	public String outputKey(int num);
	public int guardianCertify(GuardianVO gvo);
	public int joinMember(MemberVO mvo);
	public int saveGuardian(GuardianVO gvo);
	public int memberChk(String m_id);
	public List<String> idFind(String m_email);
	public int pwFindIdEmailChk(MemberVO mvo);
	public int pwFindAgreeInsert(GuardianVO gvo);
	public int pwFindCertify(GuardianVO gvo);
	public String pwFindGetId(GuardianVO gvo);
	public int pwFindChange(MemberVO mvo);
}

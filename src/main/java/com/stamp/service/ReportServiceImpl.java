package com.stamp.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.stamp.dao.ReportDao;
import com.stamp.vo.ReportVO;

@Service
@Transactional
public class ReportServiceImpl implements ReportService {
	Logger logger = Logger.getLogger(ReportServiceImpl.class);

	@Autowired
	private ReportDao reportDao;

	@Override
	public int reportInsert(ReportVO rvo) {
		int result = 0;
		result = reportDao.reportInsert(rvo);
		return result;
	}

	@Override
	public int countReport(ReportVO rvo) {
		// TODO Auto-generated method stub
		int result = 0;
		result = reportDao.reportCount(rvo);
		return result;
	}

	@Override
	public void reportUpdate(String no) {
		// TODO Auto-generated method stub
		
		reportDao.reportUpdate(no);
	}
	

}

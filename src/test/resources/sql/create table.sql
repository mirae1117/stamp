/*
drop table Guardian;
drop table Report;
drop table Point;
drop table Message;
drop table friend;
drop table Reply;
drop table question;
drop table Notice;
drop table MEMBER;
drop SEQUENCE Guardian_SEQ;
drop SEQUENCE Notice_SEQ;
drop SEQUENCE Question_SEQ;
drop SEQUENCE Reply_SEQ;
drop SEQUENCE Friend_SEQ;
drop SEQUENCE Message_SEQ;
drop SEQUENCE Point_SEQ;
drop SEQUENCE Report_SEQ;
*/

create table Member(
m_id varchar2(50) not null primary key,
m_name varchar2(50) not null,
m_pw varchar2(200) not null,
m_email varchar2(100) not null,
m_grade number(2) not null,
m_region varchar2(500) not null,
m_sex varchar2(10) not null,
m_pr varchar2(200) null,
m_profile varchar2(500) null,
m_state1 number(3) not null,
m_state2 number(2) not null,
m_private number(1) not null,
m_birth date not null,
m_joindate date default sysdate not null
);
create table Notice(
nt_no number not null primary key,
m_id varchar2(50) not null,
nt_title varchar2(100) not null,
nt_content varchar2(3000) not null,
nt_file varchar2(3000) null,
nt_count number default 0,
nt_time date default sysdate not null,
CONSTRAINT Notice_m_id FOREIGN KEY (m_id)REFERENCES  MEMBER(M_ID)
);
create table Question(
q_no number not null primary key,
q_title varchar2(200) not null,
q_content varchar2(3000) not null,
M_id varchar2(50) not null,
q_subject number(3) not null,
q_grade number(3) not null,
q_count number not null,
q_file varchar2(3000) null,
q_state number(2) not null,
q_time date default sysdate not null,
CONSTRAINT question_m_id FOREIGN KEY (m_id)REFERENCES  MEMBER(M_ID)
);
create table Reply(
rep_no number not null primary key,
rep_content varchar2(3000) not null,
m_id varchar2(50) not null,
q_no number not null,
rep_file varchar2(3000) null,
rep_time date default sysdate not null,
rep_state number(2) not null,
CONSTRAINT reply_m_id FOREIGN KEY (m_id)REFERENCES  MEMBER(M_ID)
);
create table Friend(
f_no number not null primary key,
f_time date default sysdate not null,
f_state number(2) not null,
m_receiver varchar2(50) not null,
m_sender varchar2(50) not null,
CONSTRAINT friend_m_receiver FOREIGN KEY (m_receiver)REFERENCES  MEMBER(M_ID),
CONSTRAINT friend_m_sender FOREIGN KEY (m_sender)REFERENCES  MEMBER(M_ID)
);
create table Message(
msg_no number not null primary key,
msg_title varchar2(200) not null,
msg_content varchar2(500) not null,
msg_sendtime date default sysdate not null,
msg_receivetime date null,
m_sender varchar2(50) not null,
m_receiver varchar2(50) not null,
msg_state number(1) default 0 not null,
CONSTRAINT message_m_receiver FOREIGN KEY (m_receiver)REFERENCES  MEMBER(M_ID),
CONSTRAINT message_m_sender FOREIGN KEY (m_sender)REFERENCES  MEMBER(M_ID)
);
create table Point(
p_no number not null primary key,
m_id varchar2(50) not null,
p_point number not null,
p_state number(2) not null,
p_detail varchar2(100) not null,
p_time date default sysdate not null,
CONSTRAINT point_m_id FOREIGN KEY (m_id)REFERENCES  MEMBER(M_ID)
);
create table Report(
re_no number not null primary key,
re_title varchar2(200) not null,
re_content varchar2(1000) not null,
M_id varchar2(50) not null,
re_state varchar2(2) not null,
re_sort number(2) not null,
re_suspect varchar2(13) not null,
re_date date default sysdate not null,
CONSTRAINT report_m_id FOREIGN KEY (m_id)REFERENCES  MEMBER(M_ID)
);
create table Guardian(
G_no number not null primary key,
M_id varchar2(50) null,
G_key varchar2(200) null,
G_email varchar2(100) not null,
G_state number(1) not null,
CONSTRAINT guardian_m_id FOREIGN KEY (m_id)REFERENCES  MEMBER(M_ID)
);

CREATE SEQUENCE Guardian_SEQ
START WITH 1
INCREMENT BY 1
NOCYCLE;
CREATE SEQUENCE Notice_SEQ
START WITH 1
INCREMENT BY 1
NOCYCLE;
CREATE SEQUENCE Question_SEQ
START WITH 1
INCREMENT BY 1
NOCYCLE;
CREATE SEQUENCE Reply_SEQ
START WITH 1
INCREMENT BY 1
NOCYCLE;
CREATE SEQUENCE Friend_SEQ
START WITH 1
INCREMENT BY 1
NOCYCLE;
CREATE SEQUENCE Message_SEQ
START WITH 1
INCREMENT BY 1
NOCYCLE;
CREATE SEQUENCE Point_SEQ
START WITH 1
INCREMENT BY 1
NOCYCLE;
CREATE SEQUENCE Report_SEQ
START WITH 1
INCREMENT BY 1
NOCYCLE;
